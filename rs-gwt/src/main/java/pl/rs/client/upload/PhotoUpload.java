package pl.rs.client.upload;

import java.util.Date;

import pl.rs.client.event.ChangeIconEvent;
import pl.rs.client.event.ChangeIconEvent.ChangeIconHandler;
import pl.rs.client.event.ChangeIconEvent.HasChangeIcontHandlers;
import pl.rs.shared.dto.PhotoDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent;
import com.sencha.gxt.widget.core.client.event.SubmitCompleteEvent.SubmitCompleteHandler;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.FormPanel.Encoding;
import com.sencha.gxt.widget.core.client.form.FormPanel.Method;
import com.sencha.gxt.widget.core.client.info.Info;

public class PhotoUpload extends Composite implements HasChangeIcontHandlers {

	private FramedPanel framedPanel;
	protected String fileName;
	protected CheckBox isIcon = new CheckBox();

	public PhotoUpload() {
		isIcon.setBoxLabel("Ustaw jako ikonę");
		isIcon.setValue(false);
		initWidget(createFramedPanel());
	}

	private FramedPanel createFramedPanel() {
		if (framedPanel == null) {
			framedPanel = new FramedPanel();
			framedPanel.setHeadingText("Dodaj zdjęcie");
			framedPanel.setButtonAlign(BoxLayoutPack.CENTER);

			FormPanel form = createFormPanel();
			framedPanel.add(form);

			VerticalLayoutContainer p = new VerticalLayoutContainer();
			form.add(p);

			FileUploadField file = createFileUploadField();

			p.add(new FieldLabel(file, "Plik"), new VerticalLayoutData(-18, -1));
			p.add(isIcon);

			framedPanel.addButton(createSubmitButton(form));
			framedPanel.addButton(createResetButton(form, file));
		}
		return framedPanel;
	}

	private FormPanel createFormPanel() {
		FormPanel form = new FormPanel();
		form.setAction(GWT.getHostPageBaseURL() + "fileUploadServlet");
		form.setEncoding(Encoding.MULTIPART);
		form.setMethod(Method.POST);
		return form;
	}

	private FileUploadField createFileUploadField() {
		final FileUploadField file = new FileUploadField();
		file.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				setFileName(file);
				Info.display("Plik zmieniony", "Wybrałeś: " + file.getValue());
			}
		});
		file.setName("uploadedfile");
		file.setAllowBlank(false);
		return file;
	}

	private TextButton createResetButton(final FormPanel formPanel, final FileUploadField fileUploadField) {
		TextButton resetButton = new TextButton("Wyczyść");
		resetButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				formPanel.reset();
				fileUploadField.reset();
			}
		});
		return resetButton;
	}

	private TextButton createSubmitButton(final FormPanel formPanel) {
		TextButton submitButton = new TextButton("Wyślij");
		submitButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				if (!formPanel.isValid()) {
					return;
				}
				String savedfileName = createSavedFileName();
				formPanel.setAction(GWT.getHostPageBaseURL() + "fileUploadServlet?fileName=" + savedfileName);
				formPanel.submit();
				PhotoDto photoDto = new PhotoDto(savedfileName);
				final ChangeIconEvent changeIconEvent = new ChangeIconEvent();
				if (isIcon.getValue()) {
					changeIconEvent.setIcon(photoDto);
				} else {
					changeIconEvent.setPhoto(photoDto);
				}
				formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
					@Override
					public void onSubmitComplete(SubmitCompleteEvent event) {
						fireEvent(changeIconEvent);
					}
				});
			}
		});
		return submitButton;
	}

	public String createSavedFileName() {
		String[] name = fileName.split("\\.");
		name[0] += new Date().getTime();
		return name[0] + "." + name[1];
	}

	private void setFileName(FileUploadField file) {
		String path = file.getValue();
		int lastBackSlashIndex = path.lastIndexOf("\\");
		fileName = path.substring(lastBackSlashIndex + 1);
	}

	@Override
	public HandlerRegistration addChangeIconHandler(ChangeIconHandler handler) {
		return addHandler(handler, ChangeIconEvent.getType());
	}
}