package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("roomService")
public interface RoomService extends RemoteService {

	RoomDto save(RoomDto roomDto);

	void delete(RoomDto roomDto);

	RoomDto findOne(Long roomId);

	List<RoomDto> findAll();

	PagingLoadResult<RoomDto> findAllPagin(PagingLoadConfig config);

	PagingLoadResult<RoomDto> findAllPaginSearch(RoomSearchDto roomSearchDto, PagingLoadConfig config);

	PagingLoadResult<RoomDto> findAllPaginSearchAdmin(RoomSearchDto roomSearchDto, PagingLoadConfig config);
}