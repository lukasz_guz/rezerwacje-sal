package pl.rs.client.service;

import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("reservationService")
public interface ReservationService extends RemoteService {

	ReservationDto save(ReservationDto reservationDto);

	void delete(ReservationDto reservationDto);

	ReservationDto findOne(Long id);

	PagingLoadResult<ReservationDto> findAllPagination(PagingLoadConfig config);

	PagingLoadResult<ReservationDto> findAllPaginationSearch(ReservationSearchDto reservationSearchDto, PagingLoadConfig config);
}