package pl.rs.client.service;

import pl.rs.shared.dto.RoomSettingsDto;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoomSettingsServiceAsync {

	void findOne(Long id, AsyncCallback<RoomSettingsDto> callback);

	void save(RoomSettingsDto roomSettingsDto, AsyncCallback<RoomSettingsDto> callback);
}