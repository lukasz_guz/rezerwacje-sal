package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface RoomServiceAsync {

	void findAll(AsyncCallback<List<RoomDto>> callback);

	void findOne(Long roomId, AsyncCallback<RoomDto> callback);

	void save(RoomDto roomDto, AsyncCallback<RoomDto> callback);

	void findAllPagin(PagingLoadConfig config, AsyncCallback<PagingLoadResult<RoomDto>> callback);

	void delete(RoomDto roomDto, AsyncCallback<Void> callback);

	void findAllPaginSearch(RoomSearchDto roomSearchDto, PagingLoadConfig config, AsyncCallback<PagingLoadResult<RoomDto>> callback);

	void findAllPaginSearchAdmin(RoomSearchDto roomSearchDto, PagingLoadConfig config, AsyncCallback<PagingLoadResult<RoomDto>> callback);
}