package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface RoomElementServiceAsync {

	void findAll(AsyncCallback<List<RoomElementDto>> callback);

	void findOne(Long roomElementId, AsyncCallback<RoomElementDto> callback);

	void save(RoomElementDto roomElementDto, AsyncCallback<RoomElementDto> callback);

	void findAllPagination(PagingLoadConfig config, AsyncCallback<PagingLoadResult<RoomElementDto>> callback);

	void delete(RoomElementDto roomElementDto, AsyncCallback<Void> callback);
}