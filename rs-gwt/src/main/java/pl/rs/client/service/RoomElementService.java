package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("roomElementService")
public interface RoomElementService extends RemoteService {

	RoomElementDto save(RoomElementDto roomElementDto);

	void delete(RoomElementDto roomElementDto);

	RoomElementDto findOne(Long roomElementId);

	PagingLoadResult<RoomElementDto> findAllPagination(PagingLoadConfig config);

	List<RoomElementDto> findAll();
}