package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.ChartData;
import pl.rs.shared.dto.Searchable;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ReportServiceAsync {

	void getData(AsyncCallback<List<ChartData>> callback);

	void getData(Searchable searchable, AsyncCallback<List<ChartData>> callback);
}