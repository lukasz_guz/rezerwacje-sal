package pl.rs.client.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@SuppressWarnings("serial")
public class FileUploadServlet extends HttpServlet {

	@Value("${photo.dir.path}")
	private String photoDirPath;
	private static final Logger log = LoggerFactory.getLogger(FileUploadServlet.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		FileItem uploadItem = getFileItem(req);
		if (uploadItem == null) {
			resp.getWriter().write("NO-SCRIPT-DATA");
			return;
		}
		resp.getWriter().write(new String(uploadItem.get()));
	}

	private FileItem getFileItem(HttpServletRequest req) {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> items = upload.parseRequest(req);
			Iterator<FileItem> it = items.iterator();
			while (it.hasNext()) {
				FileItem item = it.next();
				String fileName = req.getParameter("fileName");
				log.debug("Zapisywanie pliku: " + fileName);
				if (!item.isFormField() && "uploadFormElement".equals(item.getFieldName())) {
					return item;
				}
				saveFile(item, fileName);
			}
		} catch (FileUploadException e) {
			return null;
		}
		return null;
	}

	private void saveFile(FileItem fileItem, String fileName) {
		String filePath = photoDirPath + File.separator + fileName;
		log.debug("Zapisywanie pliku w " + filePath);
		File file = new File(filePath);
		try (BufferedInputStream in = new BufferedInputStream(fileItem.getInputStream()); FileOutputStream out = new FileOutputStream(file);) {
			int i;
			while ((i = in.read()) != -1) {
				out.write(i);
			}
			log.debug("Plik zapisano");
		} catch (IOException e) {
			log.error("Nie można zapisać pliku", e);
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
		final WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		if (ctx == null) {
			throw new IllegalStateException("No Spring web application context found");
		}
		ctx.getAutowireCapableBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
	}
}