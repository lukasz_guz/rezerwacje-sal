package pl.rs.client.service;

import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface ReservationServiceAsync {

	void findOne(Long id, AsyncCallback<ReservationDto> callback);

	void save(ReservationDto reservationDto, AsyncCallback<ReservationDto> callback);

	void findAllPagination(PagingLoadConfig config, AsyncCallback<PagingLoadResult<ReservationDto>> callback);

	void findAllPaginationSearch(ReservationSearchDto reservationSearchDto, PagingLoadConfig config, AsyncCallback<PagingLoadResult<ReservationDto>> callback);

	void delete(ReservationDto reservationDto, AsyncCallback<Void> callback);
}