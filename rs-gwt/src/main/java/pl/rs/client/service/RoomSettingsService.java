package pl.rs.client.service;

import pl.rs.shared.dto.RoomSettingsDto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("roomSettingsService")
public interface RoomSettingsService extends RemoteService {

	RoomSettingsDto save(RoomSettingsDto roomSettingsDto);

	RoomSettingsDto findOne(Long id);
}