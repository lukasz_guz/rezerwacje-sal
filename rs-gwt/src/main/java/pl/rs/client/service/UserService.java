package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.UserDto;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("userService")
public interface UserService extends RemoteService {

	UserDto save(UserDto userDto, boolean isAdmin);

	UserDto findOne(Long id);

	UserDto getLoggedUser();

	UserDto findOneByLogin(String login);

	List<UserDto> findAll();

	PagingLoadResult<UserDto> findAllPagin(PagingLoadConfig config);

	boolean isAdmin();

	public void changePassword(Long userId, String password);

	void logout();
}