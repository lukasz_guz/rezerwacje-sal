package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.UserDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface UserServiceAsync {

	void findAll(AsyncCallback<List<UserDto>> callback);

	void findOne(Long id, AsyncCallback<UserDto> callback);

	void findOneByLogin(String login, AsyncCallback<UserDto> callback);

	void findAllPagin(PagingLoadConfig config, AsyncCallback<PagingLoadResult<UserDto>> callback);

	void changePassword(Long userId, String password, AsyncCallback<Void> callback);

	void isAdmin(AsyncCallback<Boolean> callback);

	void save(UserDto userDto, boolean isAdmin, AsyncCallback<UserDto> callback);

	void logout(AsyncCallback<Void> callback);

	void getLoggedUser(AsyncCallback<UserDto> callback);
}