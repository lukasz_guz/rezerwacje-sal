package pl.rs.client.service;

import java.util.List;

import pl.rs.shared.dto.ChartData;
import pl.rs.shared.dto.Searchable;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("reportService")
public interface ReportService extends RemoteService {
	List<ChartData> getData();

	List<ChartData> getData(Searchable searchable);
}