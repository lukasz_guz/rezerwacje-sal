package pl.rs.client.grid;

import pl.rs.shared.dto.Idable;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

/**
 * @author lukasz.guz
 *
 * @param <T>
 */
/**
 * @author lukasz.guz
 * 
 * @param <T>
 */
public abstract class AbstractGrid<T extends Idable> implements IsWidget {

	protected RpcProxy<PagingLoadConfig, PagingLoadResult<T>> proxy = initStandardProxy();
	protected ListStore<T> store = initListStore();
	protected PagingToolBar paginToolBar;
	protected Grid<T> grid;
	protected ToolBar toolBar = new ToolBar();

	public AbstractGrid() {
	}

	protected abstract RpcProxy<PagingLoadConfig, PagingLoadResult<T>> initStandardProxy();

	protected ListStore<T> initListStore() {
		ListStore<T> store = new ListStore<T>(new ModelKeyProvider<T>() {
			@Override
			public String getKey(T idable) {
				return "" + idable.getId();
			}
		});
		return store;
	}

	protected void initGrid() {
		PagingLoader<PagingLoadConfig, PagingLoadResult<T>> loader = createPaginLoader(proxy);
		paginToolBar = createPagingToolBar(loader);
		grid = createGrid(loader, store);
	}

	public PagingLoader<PagingLoadConfig, PagingLoadResult<T>> createPaginLoader(RpcProxy<PagingLoadConfig, PagingLoadResult<T>> proxy) {
		PagingLoader<PagingLoadConfig, PagingLoadResult<T>> loader = new PagingLoader<PagingLoadConfig, PagingLoadResult<T>>(proxy);
		loader.setRemoteSort(true);
		loader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, T, PagingLoadResult<T>>(store));
		return loader;
	}

	private PagingToolBar createPagingToolBar(PagingLoader<PagingLoadConfig, PagingLoadResult<T>> loader) {
		PagingToolBar toolBar = new PagingToolBar(25);
		toolBar.bind(loader);
		return toolBar;
	}

	private Grid<T> createGrid(final PagingLoader<PagingLoadConfig, PagingLoadResult<T>> loader, ListStore<T> store) {
		ColumnModel<T> cm = createColumnModel();
		Grid<T> grid = new Grid<T>(store, cm) {
			// Wymagane do pierwszego załadowania danych
			@Override
			protected void onAfterFirstAttach() {
				super.onAfterFirstAttach();
				loader.load();
			}
		};
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setLoader(loader);
		return grid;
	}

	protected abstract ColumnModel<T> createColumnModel();

	public void updateLoader(PagingLoader<PagingLoadConfig, PagingLoadResult<T>> loader) {
		grid.setLoader(loader);
		paginToolBar.bind(loader);
		grid.getLoader().load();
	}

	@Override
	public Widget asWidget() {
		VerticalLayoutContainer con = new VerticalLayoutContainer();
		con.setBorders(true);
		con.add(toolBar, new VerticalLayoutData(1, 28));
		con.add(grid, new VerticalLayoutData(1, 1));
		con.add(paginToolBar, new VerticalLayoutData(1, 28));
		return con;
	}

	public PagingToolBar getPaginToolBar() {
		return paginToolBar;
	}

	public Grid<T> getGrid() {
		return grid;
	}

	public ToolBar getToolBar() {
		return toolBar;
	}

}