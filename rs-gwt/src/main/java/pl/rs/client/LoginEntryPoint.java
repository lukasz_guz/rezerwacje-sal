package pl.rs.client;

import pl.rs.client.login.LoginWindow;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

public class LoginEntryPoint implements EntryPoint {

	public void onModuleLoad() {
		GWT.setUncaughtExceptionHandler(new RSUncaughtExceptionHandler());
		LoginWindow loginWindow = new LoginWindow();
		if (isError()) {
			loginWindow.setErrorMessage("Nieprawidłowy login lub hasło!");
		} else {
			loginWindow.setErrorMessage("");
		}
		RootPanel.get("login-panel").add(loginWindow);
	}

	private boolean isError() {
		String error = Window.Location.getParameter("error");
		return (error != null && error.equals("1"));
	}
}
