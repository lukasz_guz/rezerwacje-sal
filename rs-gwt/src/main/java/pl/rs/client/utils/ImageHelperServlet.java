package pl.rs.client.utils;

import pl.rs.service.ImageServlet;
import pl.rs.shared.dto.PhotoDto;

public final class ImageHelperServlet {

	public static String createPhotoPath(String photoName) {
		return "imageServlet?" + ImageServlet.PHOTO_NAME_PARAMETER + "=" + photoName;
	}

	public static String createPhotoPath(PhotoDto photoDto) {
		return "imageServlet?" + ImageServlet.PHOTO_NAME_PARAMETER + "=" + photoDto.getName();
	}
}
