package pl.rs.client.user.presenter.crud;

import pl.rs.shared.dto.UserDto;

/**
 * Interfejs prezentera dodawania nowego użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public interface AddUserPresenter extends UserPresenter {

	void save(UserDto userDto);
}