package pl.rs.client.user.ui.crud;

import pl.rs.client.validator.ConfirmPasswordValidator;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;

public class UserEditor extends Composite implements Editor<UserDto> {

	TextField login = new TextField();
	PasswordField password = new PasswordField();
	@Ignore
	PasswordField confirmPassword = new PasswordField();
	TextField lastName = new TextField();
	TextField firstName = new TextField();

	private SimpleContainer root = new SimpleContainer();

	public UserEditor() {
		preparePanel();
		initWidget(root);
	}

	private void preparePanel() {
		VerticalLayoutContainer basicData = new VerticalLayoutContainer();
		basicData.add(new FieldLabel(login, "Login"));
		basicData.add(new FieldLabel(password, "Hasło"));
		basicData.add(new FieldLabel(confirmPassword, "Powtórz hasło"));
		basicData.add(new FieldLabel(lastName, "Nazwisko"));
		basicData.add(new FieldLabel(firstName, "Imię"));
		root.add(basicData);
	}

	public void addValidators() {
		login.addValidator(new EmptyValidator<String>());
		login.addValidator(new MinLengthValidator(3));
		login.addValidator(new MaxLengthValidator(20));

		password.addValidator(new EmptyValidator<String>());
		password.addValidator(new MinLengthValidator(6));

		confirmPassword.addValidator(new ConfirmPasswordValidator(password));

		lastName.addValidator(new EmptyValidator<String>());
		lastName.addValidator(new MaxLengthValidator(40));

		firstName.addValidator(new EmptyValidator<String>());
		firstName.addValidator(new MaxLengthValidator(30));
	}
}