package pl.rs.client.user.view.list;

import pl.rs.client.view.View;

/**
 * Interfejs widoku listy użytkowników
 * 
 * @author lukasz.guz
 * 
 */
public interface UserListView extends View {

}