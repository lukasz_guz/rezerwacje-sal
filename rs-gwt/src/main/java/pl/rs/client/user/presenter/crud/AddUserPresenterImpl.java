package pl.rs.client.user.presenter.crud;

import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;
import pl.rs.client.user.view.crud.AddUserView;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class AddUserPresenterImpl implements AddUserPresenter {

	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);
	private AddUserView view;

	public AddUserPresenterImpl(AddUserView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void save(UserDto userDto) {
		userServiceAsync.save(userDto, false, new AsyncCallback<UserDto>() {
			@Override
			public void onSuccess(UserDto result) {
				view.setUserDto(result);
				view.setAddressDto(result.getAddress());
				view.addMessage("", "Zarejestrowano użytkownika");
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
}