package pl.rs.client.user.view.crud;


/**
 * Interfejs podglądu dodawania użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public interface AddUserView extends UserView {
}