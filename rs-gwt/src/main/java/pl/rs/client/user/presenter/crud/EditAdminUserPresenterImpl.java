package pl.rs.client.user.presenter.crud;

import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;
import pl.rs.client.user.view.crud.EditUserView;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class EditAdminUserPresenterImpl implements EditUserPresenter {

	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);
	private EditUserView view;
	private Long userId;

	public EditAdminUserPresenterImpl(Long userId, EditUserView view) {
		this.userId = userId;
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
		userServiceAsync.findOne(userId, new AsyncCallback<UserDto>() {

			@Override
			public void onSuccess(UserDto result) {
				view.setUserDto(result);
				view.setAddressDto(result.getAddress());
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można wczytać wskazanego użytkownika");
			}
		});
	}

	@Override
	public void save(UserDto userDto, boolean isAdmin) {
		userServiceAsync.save(userDto, isAdmin, new AsyncCallback<UserDto>() {
			@Override
			public void onSuccess(UserDto result) {
				view.setUserDto(result);
				view.setAddressDto(result.getAddress());
				view.addMessage("", "Zarejestrowano użytkownika");
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}
}