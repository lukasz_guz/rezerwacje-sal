package pl.rs.client.user.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.user.presenter.crud.EditUserPresenter;
import pl.rs.client.user.view.crud.EditUserView;

import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class EditAdminUserWindow extends UserWindow implements EditUserView {

	protected final TextButton changePassButton = createChangePassBtn();
	protected final CheckBox isAdmin = new CheckBox();

	private EditUserPresenter presenter;

	public EditAdminUserWindow() {
		setHeadingText("Edycja użytkownika");
		toolBar = createToolBar();
		userEditor.addValidators();
		addressEditor.addValidators();
		isAdmin.setBoxLabel("Administrator");
		if ("ROLE_ADMIN".equals(userDto.getRole())) {
			isAdmin.setValue(true);
		} else {
			isAdmin.setValue(false);
		}
		turnOffFields();
		init();
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.add(isAdmin);
		containerPanel.add(contentPanel);
		containerPanel.forceLayout();
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton saveButton = new TextButton("Zapisz");
		saveButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				userDto = userDriver.flush();
				addressDto = addressDriver.flush();
				userDto.setAddress(addressDto);
				if (userDriver.hasErrors() || addressDriver.hasErrors()) {
					return;
				}
				presenter.save(userDto, isAdmin.getValue());
			}
		});
		TextButton cancelButton = new TextButton("Anuluj");
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				EditAdminUserWindow.this.hide();
			}
		});
		toolBar.add(saveButton);
		toolBar.add(changePassButton);
		toolBar.add(cancelButton);
		return toolBar;
	}

	void turnOffFields() {
		userEditor.login.setReadOnly(true);
		userEditor.password.setEnabled(false);
		userEditor.password.getParent().setVisible(false);
		userEditor.confirmPassword.setEnabled(false);
		userEditor.confirmPassword.getParent().setVisible(false);
	}

	private TextButton createChangePassBtn() {
		TextButton changePassBtn = new TextButton("Zmiana hasła");
		changePassBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				new ChangePasswordAdminWindow(EditAdminUserWindow.this).show();
			}
		});
		return changePassBtn;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (EditUserPresenter) presenter;
	}
}