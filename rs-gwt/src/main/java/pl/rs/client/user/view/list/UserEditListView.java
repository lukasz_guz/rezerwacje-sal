package pl.rs.client.user.view.list;

/**
 * Interfejs widoku edytowanej listy użytkowników
 * 
 * @author lukasz.guz
 * 
 */
public interface UserEditListView extends UserListView {

}