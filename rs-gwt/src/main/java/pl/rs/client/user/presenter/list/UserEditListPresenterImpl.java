package pl.rs.client.user.presenter.list;

import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;
import pl.rs.client.user.view.list.UserEditListView;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class UserEditListPresenterImpl implements UserEditListPresenter {

	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);
	private UserEditListView view;

	public UserEditListPresenterImpl(UserEditListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<UserDto>> callback) {
		userServiceAsync.findAllPagin(loadConfig, callback);
	}
}