package pl.rs.client.user.presenter.list;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * Interfejs prezenetera listy podglądu użytkowników
 * 
 * @author lukasz.guz
 * 
 */
public interface UserListPresenter extends Presenter {

	void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<UserDto>> callback);
}