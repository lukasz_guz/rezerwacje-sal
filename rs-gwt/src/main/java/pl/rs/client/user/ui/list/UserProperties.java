package pl.rs.client.user.ui.list;

import pl.rs.shared.dto.UserDto;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface UserProperties extends PropertyAccess<UserDto> {

	ModelKeyProvider<UserDto> id();

	ValueProvider<UserDto, String> login();

	ValueProvider<UserDto, String> role();
}