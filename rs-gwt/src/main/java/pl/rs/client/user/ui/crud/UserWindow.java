package pl.rs.client.user.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.user.presenter.crud.UserPresenter;
import pl.rs.client.user.view.crud.UserView;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

/**
 * Okno podglądu użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public class UserWindow extends Window implements UserView {

	interface UserDriver extends SimpleBeanEditorDriver<UserDto, UserEditor> {
	}

	interface AddressDriver extends SimpleBeanEditorDriver<AddressDto, AddressEditor> {
	}

	protected final static String WIDTH = "500 px";
	protected final static String HEIGHT = "500 px";

	protected UserDriver userDriver = GWT.create(UserDriver.class);
	protected AddressDriver addressDriver = GWT.create(AddressDriver.class);

	@SuppressWarnings("unused")
	private UserPresenter presenter;
	protected UserDto userDto = new UserDto();
	protected AddressDto addressDto = new AddressDto();

	protected UserEditor userEditor = new UserEditor();
	protected AddressEditor addressEditor = new AddressEditor();
	protected VerticalLayoutContainer containerPanel;
	protected ToolBar toolBar = new ToolBar();

	public UserWindow() {
		setHeadingText("Dane użytkownika");
		init();
	}

	protected void init() {
		setSize(WIDTH, HEIGHT);
		setModal(true);
		setResizable(false);

		userDriver.initialize(userEditor);
		userDriver.edit(userDto);

		addressDriver.initialize(addressEditor);
		addressDriver.edit(addressDto);

		containerPanel = createPanel();
		containerPanel.forceLayout();
		add(containerPanel, new MarginData(10));
	}

	protected VerticalLayoutContainer createPanel() {
		final VerticalLayoutContainer registrationUserPanel = new VerticalLayoutContainer();
		registrationUserPanel.add(toolBar);

		ContentPanel userPanel = new ContentPanel();
		userPanel.setHeadingText("Dane użytkownika");
		userPanel.add(userEditor);
		registrationUserPanel.add(userPanel);

		ContentPanel addressPanel = new ContentPanel();
		addressPanel.setHeadingText("Adres");
		addressPanel.add(addressEditor);
		registrationUserPanel.add(addressPanel);

		return registrationUserPanel;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		userDriver.edit(userDto);
		addressDriver.edit(addressDto);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (UserPresenter) presenter;
	}

	@Override
	public UserDto getUserDto() {
		return userDto;
	}

	@Override
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}

	@Override
	public AddressDto getAddressDto() {
		return addressDto;
	}

	@Override
	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}
}