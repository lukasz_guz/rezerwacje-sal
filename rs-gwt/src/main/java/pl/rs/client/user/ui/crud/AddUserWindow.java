package pl.rs.client.user.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.user.presenter.crud.AddUserPresenter;
import pl.rs.client.user.view.crud.AddUserView;

import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class AddUserWindow extends UserWindow implements AddUserView {

	private AddUserPresenter presenter;

	public AddUserWindow() {
		toolBar = createToolBar();
		userEditor.addValidators();
		addressEditor.addValidators();
		init();
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton saveButton = new TextButton("Zapisz");
		saveButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				userDto = userDriver.flush();
				addressDto = addressDriver.flush();
				userDto.setAddress(addressDto);
				if (userDriver.hasErrors() || addressDriver.hasErrors()) {
					return;
				}
				presenter.save(userDto);
				updateView();
				AddUserWindow.this.hide();
			}
		});
		TextButton cancelButton = new TextButton("Anuluj");
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				AddUserWindow.this.hide();
			}
		});
		toolBar.add(saveButton);
		toolBar.add(cancelButton);
		return toolBar;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (AddUserPresenter) presenter;
	}
}