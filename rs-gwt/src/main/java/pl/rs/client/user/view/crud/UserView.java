package pl.rs.client.user.view.crud;

import pl.rs.client.view.View;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.UserDto;

/**
 * Interfejs widoku podglądu użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public interface UserView extends View {

	UserDto getUserDto();

	void setUserDto(UserDto userDto);

	AddressDto getAddressDto();

	void setAddressDto(AddressDto addressDto);
}