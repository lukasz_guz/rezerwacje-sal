package pl.rs.client.user.presenter.crud;

import pl.rs.client.presenter.Presenter;

/**
 * Interfejs prezentera podglądu użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public interface UserPresenter extends Presenter {

}