package pl.rs.client.user.ui.crud;

import pl.rs.client.async.EmptyAsyncCallback;
import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;
import pl.rs.client.validator.ConfirmPasswordValidator;

import com.google.gwt.core.client.GWT;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class ChangePasswordAdminWindow extends Window {

	private final static String WIDTH = "300 px";
	private final static String HEIGHT = "300 px";
	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);

	private PasswordField newPassword = new PasswordField();
	private PasswordField confirmPassword = new PasswordField();

	private UserWindow userRegistrationWindow;

	public ChangePasswordAdminWindow(UserWindow userRegistrationWindow) {
		this.userRegistrationWindow = userRegistrationWindow;
		setSize(WIDTH, HEIGHT);
		setHeadingText("Zmiana hasła");
		setModal(true);
		setResizable(false);
		addValidators();
		add(createPanel(), new MarginData(10));
	}

	private ContentPanel createPanel() {
		ContentPanel root = new ContentPanel();
		VerticalLayoutContainer panel = new VerticalLayoutContainer();
		panel.add(createToolBar());
		panel.add(new FieldLabel(newPassword, "Nowe hasło"));
		panel.add(new FieldLabel(confirmPassword, "Powtórz hasło"));
		root.add(panel);
		return root;
	}

	private void addValidators() {
		newPassword.addValidator(new EmptyValidator<String>());
		newPassword.addValidator(new MinLengthValidator(6));
		confirmPassword.addValidator(new ConfirmPasswordValidator(newPassword));
	}

	private ToolBar createToolBar() {
		ToolBar tb = new ToolBar();
		TextButton saveBtn = new TextButton("Zapisz");
		saveBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (newPassword.validate() && confirmPassword.validate()) {
					userServiceAsync.changePassword(userRegistrationWindow.userDto.getId(), newPassword.getValue(), new EmptyAsyncCallback<Void>());
					userRegistrationWindow.userDto.setPassword(newPassword.getValue());
					userRegistrationWindow.userDriver.edit(userRegistrationWindow.userDto);
					hide();
				}
			}
		});
		TextButton cancelBtn = new TextButton("Anuluj");
		cancelBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				hide();
			}
		});

		tb.add(saveBtn);
		tb.add(cancelBtn);
		return tb;
	}
}
