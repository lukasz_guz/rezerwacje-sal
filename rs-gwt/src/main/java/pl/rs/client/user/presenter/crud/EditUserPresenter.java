package pl.rs.client.user.presenter.crud;

import pl.rs.shared.dto.UserDto;

/**
 * Interfejs prezentera edycji użytkownika
 * 
 * @author lukasz.guz
 * 
 */
public interface EditUserPresenter extends UserPresenter {

	void save(UserDto userDto, boolean isAdmin);
}