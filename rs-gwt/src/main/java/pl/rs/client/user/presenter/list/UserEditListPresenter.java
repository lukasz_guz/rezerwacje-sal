package pl.rs.client.user.presenter.list;

/**
 * Interfejs prezenetera edytowanej listy użytkowników
 * 
 * @author lukasz.guz
 * 
 */
public interface UserEditListPresenter extends UserListPresenter {

}