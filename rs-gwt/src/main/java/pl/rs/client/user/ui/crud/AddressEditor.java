package pl.rs.client.user.ui.crud;

import pl.rs.shared.dto.AddressDto;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;

public class AddressEditor extends Composite implements Editor<AddressDto> {

	TextField city = new TextField();
	TextField apartamentNumber = new TextField();
	TextField houseNumber = new TextField();
	TextField street = new TextField();
	TextField postCode = new TextField();

	private SimpleContainer root = new SimpleContainer();

	public AddressEditor() {
		root.add(preparePanel());
		initWidget(root);
	}

	private VerticalLayoutContainer preparePanel() {
		VerticalLayoutContainer basicData = new VerticalLayoutContainer();
		basicData.add(new FieldLabel(city, "Miasto"));
		basicData.add(new FieldLabel(postCode, "Kod pocztowy"));
		basicData.add(new FieldLabel(street, "Ulica"));
		basicData.add(new FieldLabel(houseNumber, "Numer domu"));
		basicData.add(new FieldLabel(apartamentNumber, "Numer lokalu"));
		return basicData;
	}

	public void setEnabled(boolean enabled) {
		city.setEnabled(enabled);
		apartamentNumber.setEnabled(enabled);
		houseNumber.setEnabled(enabled);
		street.setEnabled(enabled);
		postCode.setEnabled(enabled);
	}

	public void setReadOnly(boolean readOnly) {
		city.setReadOnly(readOnly);
		apartamentNumber.setReadOnly(readOnly);
		houseNumber.setReadOnly(readOnly);
		street.setReadOnly(readOnly);
		postCode.setReadOnly(readOnly);
	}

	public void addValidators() {
		city.addValidator(new EmptyValidator<String>());
		city.addValidator(new MaxLengthValidator(40));

		postCode.addValidator(new EmptyValidator<String>());
		postCode.addValidator(new RegExValidator("\\d{2}-\\d{3}"));

		houseNumber.addValidator(new EmptyValidator<String>());
		houseNumber.addValidator(new MaxLengthValidator(10));
	}
}