package pl.rs.client.user.ui.list;

import java.util.ArrayList;
import java.util.List;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.user.presenter.crud.AddUserPresenterImpl;
import pl.rs.client.user.presenter.crud.EditAdminUserPresenterImpl;
import pl.rs.client.user.presenter.list.UserEditListPresenter;
import pl.rs.client.user.ui.crud.AddUserWindow;
import pl.rs.client.user.ui.crud.EditAdminUserWindow;
import pl.rs.client.user.view.list.UserEditListView;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent.RowDoubleClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class UserEditGrid extends AbstractGrid<UserDto> implements UserEditListView {

	private UserProperties props = GWT.create(UserProperties.class);
	private UserEditListPresenter presenter;

	public UserEditGrid() {
		initGrid();
		toolBar = createToolBar();
		grid.addRowDoubleClickHandler(new RowDoubleClickHandler() {
			@Override
			public void onRowDoubleClick(RowDoubleClickEvent event) {
				UserDto userDto = grid.getSelectionModel().getSelectedItem();
				EditAdminUserWindow editAdminUserWindow = new EditAdminUserWindow();
				new EditAdminUserPresenterImpl(userDto.getId(), editAdminUserWindow);
				editAdminUserWindow.show();
			}
		});
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton addUserBtn = new TextButton("Dodaj użytkownika");
		addUserBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				AddUserWindow addUserWindow = new AddUserWindow();
				new AddUserPresenterImpl(addUserWindow);
				addUserWindow.show();
			}
		});
		toolBar.add(addUserBtn);
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (UserEditListPresenter) presenter;
	}

	@Override
	protected RpcProxy<PagingLoadConfig, PagingLoadResult<UserDto>> initStandardProxy() {
		RpcProxy<PagingLoadConfig, PagingLoadResult<UserDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<UserDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<UserDto>> callback) {
				presenter.findAllPagination(loadConfig, callback);
			}
		};
		return proxy;
	}

	@Override
	protected ColumnModel<UserDto> createColumnModel() {
		ColumnConfig<UserDto, String> loginColumn = new ColumnConfig<UserDto, String>(props.login(), 150, "Login");
		ColumnConfig<UserDto, String> roleColumn = new ColumnConfig<UserDto, String>(props.role(), 150, "Rola");

		List<ColumnConfig<UserDto, ?>> columns = new ArrayList<ColumnConfig<UserDto, ?>>();
		columns.add(loginColumn);
		columns.add(roleColumn);
		return new ColumnModel<UserDto>(columns);
	}
}