package pl.rs.client.event;

import pl.rs.client.event.ChangeIconEvent.ChangeIconHandler;
import pl.rs.shared.dto.PhotoDto;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

/**
 * Fires after a item is selected.
 */
public class ChangeIconEvent extends GwtEvent<ChangeIconHandler> {

	/**
	 * Handler type.
	 */
	private static Type<ChangeIconHandler> TYPE;

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static Type<ChangeIconHandler> getType() {
		if (TYPE == null) {
			TYPE = new Type<ChangeIconHandler>();
		}
		return TYPE;
	}

	private Context context;
	private PhotoDto icon;
	private PhotoDto photo;

	public ChangeIconEvent() {

	}

	public ChangeIconEvent(Context context) {
		this.context = context;
	}

	/**
	 * Returns the cell context.
	 * 
	 * @return the cell context or null if event not cell based
	 */
	public Context getContext() {
		return context;
	}

	public PhotoDto getIcon() {
		return icon;
	}

	public void setIcon(PhotoDto icon) {
		this.icon = icon;
	}

	public PhotoDto getPhoto() {
		return photo;
	}

	public void setPhoto(PhotoDto photo) {
		this.photo = photo;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Type<ChangeIconHandler> getAssociatedType() {
		return (Type) TYPE;
	}

	@Override
	protected void dispatch(ChangeIconHandler handler) {
		handler.changeIcon(this);
	}

	/**
	 * Handler for {@link ChangeIconEvent} events.
	 */
	public interface ChangeIconHandler extends EventHandler {

		/**
		 * Called after a widget change icon in RoomElementDto.
		 * 
		 * @param event the {@link ChangeIconEvent} that was fired
		 */
		void changeIcon(ChangeIconEvent event);

	}

	/**
	 * A widget that implements this interface is a public source of {@link ChangeIconEvent} events.
	 */
	public interface HasChangeIcontHandlers extends HasHandlers {

		/**
		 * Adds a {@link ChangeIconEvent} handler for {@link ChangeIconEvent} events.
		 * 
		 * @param handler the handler
		 * @return the registration for the event
		 */
		HandlerRegistration addChangeIconHandler(ChangeIconHandler handler);
	}
}