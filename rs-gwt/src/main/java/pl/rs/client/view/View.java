package pl.rs.client.view;

import pl.rs.client.presenter.Presenter;

public interface View {

	void addMessage(String title, String message);

	void updateView();

	void setPresenter(Presenter presenter);
}