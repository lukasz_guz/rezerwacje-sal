package pl.rs.client.login;

import pl.rs.client.user.presenter.crud.AddUserPresenterImpl;
import pl.rs.client.user.ui.crud.AddUserWindow;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SubmitButton;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

public class LoginPanel implements IsWidget {

	@SuppressWarnings("unused")
	private Window window;
	private TextField login = new TextField();
	private PasswordField password = new PasswordField();
	private SubmitButton submitButton = new SubmitButton("Zaloguj");
	private Label errorLabel = new Label();

	private Button registerButton = new Button("Zarejestruj się");
	private VerticalLayoutContainer fieldPanel = new VerticalLayoutContainer();

	private KeyDownHandler keyDownHandler = new KeyDownHandler() {

		@Override
		public void onKeyDown(KeyDownEvent event) {
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				submitButton.click();
			}
		}
	};

	public LoginPanel() {
	}

	public LoginPanel(Window window) {
		this();
		this.window = window;
	}

	@Override
	public Widget asWidget() {
		login.setName("j_username");
		login.addKeyDownHandler(keyDownHandler);
		password.setName("j_password");
		password.addKeyDownHandler(keyDownHandler);
		fieldPanel.add(new FieldLabel(login, "Login"));
		fieldPanel.add(new FieldLabel(password, "Hasło"));
		fieldPanel.add(errorLabel);
		fieldPanel.add(submitButton);
		fieldPanel.add(registerButton);
		registerButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				AddUserWindow addUserWindow = new AddUserWindow();
				new AddUserPresenterImpl(addUserWindow);
				addUserWindow.show();
			}
		});
		return fieldPanel;
	}

	public void setErrorMessage(String message) {
		errorLabel.setText(message);
	}
}