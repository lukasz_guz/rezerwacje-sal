package pl.rs.client.login;

import com.sencha.gxt.widget.core.client.Window;

public class LoginWindow extends Window {

	private LoginPanel loginPanel;

	public LoginWindow() {
		setClosable(false);
		setDraggable(false);
		setResizable(false);
		loginPanel = new LoginPanel(this);
		this.add(loginPanel);
	}

	public void setErrorMessage(String message) {
		loginPanel.setErrorMessage(message);
	}
}