package pl.rs.client.presenter;

/**
 * Interfejs prezentera wykorzystywany we wzorcu MVP
 * 
 * @author lukasz.guz
 * 
 */
public interface Presenter {

	/**
	 * Podpięcie prezentera pod widok
	 */
	void bind();
}