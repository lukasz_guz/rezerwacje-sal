package pl.rs.client.reports.presenter;

import pl.rs.client.presenter.Presenter;

/**
 * Interfejs prezentera raportów
 * 
 * @author lukasz.guz
 * 
 */
public interface ReportPresenter extends Presenter {

	void getData();
}