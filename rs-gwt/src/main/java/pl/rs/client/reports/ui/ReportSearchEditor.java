package pl.rs.client.reports.ui;

import pl.rs.client.validator.PastEndDateValidator;
import pl.rs.shared.dto.ReportSearchDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;

public class ReportSearchEditor extends Composite implements Editor<ReportSearchDto> {

	// Reservation
	DateField beginDate = new DateField(new DateTimePropertyEditor(DateTimeFormat.getFormat("dd-MM-yyyy")));
	DateField endDate = new DateField(new DateTimePropertyEditor(DateTimeFormat.getFormat("dd-MM-yyyy")));

	// Room
	TextField nameRoom = new TextField();

	public ReportSearchEditor() {
		preparePanel();
		initWidget(preparePanel());
	}

	private SimpleContainer preparePanel() {
		HorizontalLayoutContainer basic = new HorizontalLayoutContainer();

		VerticalLayoutContainer reservationPanel = new VerticalLayoutContainer();
		reservationPanel.add((new FieldLabel(beginDate, "Od kiedy")));
		reservationPanel.add((new FieldLabel(endDate, "Do kiedy")));
		basic.add(reservationPanel, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		VerticalLayoutContainer roomPanel = new VerticalLayoutContainer();
		roomPanel.add((new FieldLabel(nameRoom, "Nazwa sali")));
		basic.add(roomPanel, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		SimpleContainer root = new SimpleContainer();
		root.add(basic);

		return root;
	}

	public void addValidator() {
		endDate.addValidator(new PastEndDateValidator(beginDate));
	}
}