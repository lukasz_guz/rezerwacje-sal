package pl.rs.client.reports.ui;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.reports.presenter.ReportSearchPresenter;
import pl.rs.client.reports.view.ReportSearchView;
import pl.rs.client.reports.view.ReportView;
import pl.rs.shared.dto.ChartData;
import pl.rs.shared.dto.ReportSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class ReportSearchPanel implements IsWidget, ReportSearchView {

	interface ReportSearchDriver extends SimpleBeanEditorDriver<ReportSearchDto, ReportSearchEditor> {
	}

	private final ReportSearchDriver reportSearchDriver = GWT.create(ReportSearchDriver.class);
	private ReportSearchEditor reportSearchEditor = new ReportSearchEditor();
	private ToolBar toolBar;
	private ReportSearchDto reportSearchDto = new ReportSearchDto();
	private ReportSearchPresenter presenter;
	private ReportView reportView;

	public ReportSearchPanel(ReportView reportView) {
		this.reportView = reportView;
		reportSearchEditor.addValidator();
		reportSearchDriver.initialize(reportSearchEditor);
		reportSearchDriver.edit(reportSearchDto);
	}

	@Override
	public Widget asWidget() {
		toolBar = createToolBar();
		VerticalLayoutContainer panels = new VerticalLayoutContainer();
		panels.add(toolBar, new VerticalLayoutData(1d, 28d));
		panels.add(reportSearchEditor, new VerticalLayoutData(1d, 100d));
		SimpleContainer sc = new SimpleContainer();
		sc.add((IsWidget) reportView);
		panels.add(sc, new VerticalLayoutData(1d, 1d));
		return panels;
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton searchBtn = new TextButton("Generuj");
		searchBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				reportSearchDto = reportSearchDriver.flush();
				if (reportSearchDriver.hasErrors()) {
					return;
				}
				presenter.getData(reportSearchDto);
			}
		});
		TextButton clearBtn = new TextButton("Wyczyść");
		clearBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				reportSearchDto = new ReportSearchDto();
				reportSearchDriver.edit(reportSearchDto);
			}
		});
		toolBar.add(searchBtn);
		toolBar.add(clearBtn);
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		reportView.updateView();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (ReportSearchPresenter) presenter;
	}

	@Override
	public ListStore<ChartData> getStore() {
		return reportView.getStore();
	}
}