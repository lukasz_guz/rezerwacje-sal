package pl.rs.client.reports.view;

import pl.rs.client.view.View;
import pl.rs.shared.dto.ChartData;

import com.sencha.gxt.data.shared.ListStore;

/**
 * Interfejs widoku raportów
 * 
 * @author lukasz.guz
 * 
 */
public interface ReportView extends View {

	ListStore<ChartData> getStore();
}