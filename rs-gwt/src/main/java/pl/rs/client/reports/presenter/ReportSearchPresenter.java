package pl.rs.client.reports.presenter;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.ReportSearchDto;

/**
 * Interfejs prezentera wyszukiwarki raportów
 * 
 * @author lukasz.guz
 * 
 */
public interface ReportSearchPresenter extends Presenter {
	void getData(ReportSearchDto reportSearchDto);
}