package pl.rs.client.reports.ui;

import java.math.BigDecimal;

import pl.rs.shared.dto.ChartData;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface ChartDataProperty extends PropertyAccess<ChartData> {
	ModelKeyProvider<ChartData> id();

	ValueProvider<ChartData, String> name();

	ValueProvider<ChartData, BigDecimal> data1();

	ValueProvider<ChartData, BigDecimal> data2();

}