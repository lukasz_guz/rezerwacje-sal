package pl.rs.client.reports.presenter;

import java.util.List;

import pl.rs.client.reports.view.ReportSearchView;
import pl.rs.client.service.ReportService;
import pl.rs.client.service.ReportServiceAsync;
import pl.rs.shared.dto.ChartData;
import pl.rs.shared.dto.ReportSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ReportSearchPresenterImpl implements ReportSearchPresenter {

	private final ReportServiceAsync reportServiceAsync = GWT.create(ReportService.class);
	private ReportSearchView view;

	public ReportSearchPresenterImpl(ReportSearchView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void getData(ReportSearchDto reportSearchDto) {
		reportServiceAsync.getData(reportSearchDto, new AsyncCallback<List<ChartData>>() {

			@Override
			public void onSuccess(List<ChartData> result) {
				view.getStore().clear();
				view.getStore().addAll(result);
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można utworzyć raportu");
			}
		});
	}
}