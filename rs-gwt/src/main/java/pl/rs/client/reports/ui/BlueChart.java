package pl.rs.client.reports.ui;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.reports.presenter.ReportPresenter;
import pl.rs.client.reports.view.ReportView;
import pl.rs.shared.dto.ChartData;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.chart.client.chart.Chart;
import com.sencha.gxt.chart.client.chart.Chart.Position;
import com.sencha.gxt.chart.client.chart.axis.CategoryAxis;
import com.sencha.gxt.chart.client.chart.axis.NumericAxis;
import com.sencha.gxt.chart.client.chart.series.BarSeries;
import com.sencha.gxt.chart.client.draw.Gradient;
import com.sencha.gxt.chart.client.draw.RGB;
import com.sencha.gxt.chart.client.draw.Stop;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite.TextAnchor;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.Resizable;
import com.sencha.gxt.widget.core.client.Resizable.Dir;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.info.Info;

public class BlueChart implements IsWidget, ReportView {

	private final ChartDataProperty chartDataProperty = GWT.create(ChartDataProperty.class);
	private final ListStore<ChartData> store = createStore();
	private final Chart<ChartData> chart = createChart(store);

	private ReportPresenter presenter;

	private ListStore<ChartData> createStore() {
		ListStore<ChartData> store = new ListStore<ChartData>(chartDataProperty.id());
		if (presenter != null) {
			presenter.getData();
		}
		return store;
	}

	private Chart<ChartData> createChart(ListStore<ChartData> store) {
		Chart<ChartData> chart = new Chart<ChartData>();
		chart.setStore(store);
		chart.setDefaultInsets(30);
		chart.setShadowChart(true);
		chart.addAxis(createNumericAxis());
		chart.addAxis(createCategoryAxis());
		chart.addGradient(createGradient());
		chart.addSeries(createCharBarSeries());
		return chart;
	}

	private NumericAxis<ChartData> createNumericAxis() {
		NumericAxis<ChartData> axis = new NumericAxis<ChartData>();
		axis.setPosition(Position.LEFT);
		axis.addField(chartDataProperty.data1());
		axis.addField(chartDataProperty.data2());
		axis.setDisplayGrid(true);
		axis.setMinimum(0);
		axis.setMaximum(3000);
		TextSprite text = createTextSprite();
		axis.setLabelConfig(text);
		return axis;
	}

	private CategoryAxis<ChartData, String> createCategoryAxis() {
		CategoryAxis<ChartData, String> catAxis = new CategoryAxis<ChartData, String>();
		catAxis.setPosition(Position.BOTTOM);
		catAxis.setField(chartDataProperty.name());
		TextSprite text = createTextSprite();
		text = text.copy();
		text.setFontSize(11);
		text.setTextAnchor(TextAnchor.MIDDLE);
		catAxis.setDisplayGrid(true);
		catAxis.setLabelConfig(text);
		catAxis.setLabelProvider(new LabelProvider<String>() {
			@Override
			public String getLabel(String item) {
				return item;
			}
		});
		return catAxis;
	}

	private TextSprite createTextSprite() {
		TextSprite text = new TextSprite();
		text.setFont("Arial");
		text.setFontSize(10);
		text.setTextBaseline(com.sencha.gxt.chart.client.draw.sprite.TextSprite.TextBaseline.MIDDLE);
		return text;
	}

	private Gradient createGradient() {
		Gradient gradient = new Gradient("bar-gradient", 90);
		gradient.addStop(new Stop(0, new RGB("#99BBE8")));
		gradient.addStop(new Stop(70, new RGB("#77AECE")));
		gradient.addStop(new Stop(100, new RGB("#77AECE")));
		return gradient;
	}

	private BarSeries<ChartData> createCharBarSeries() {
		final BarSeries<ChartData> bar = new BarSeries<ChartData>();
		bar.setYAxisPosition(Position.LEFT);
		bar.addYField(chartDataProperty.data1());
		bar.setColumn(true);
		bar.addColor(createGradient());
		return bar;
	}

	@Override
	public Widget asWidget() {
		chart.setLayoutData(new VerticalLayoutData(1, 1));
		SimpleContainer root = new SimpleContainer();
		root.add(chart);
		final Resizable resize = new Resizable(root, Dir.E, Dir.SE, Dir.S);
		resize.setMinHeight(400);
		resize.setMinWidth(400);
		return root;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		chart.redrawChart();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (ReportPresenter) presenter;
	}

	@Override
	public ListStore<ChartData> getStore() {
		return store;
	}
}