package pl.rs.client.dnd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pl.rs.client.RSEntryPoint;
import pl.rs.client.service.RoomSettingsService;
import pl.rs.client.service.RoomSettingsServiceAsync;
import pl.rs.shared.dto.ArragementDto;
import pl.rs.shared.dto.RoomSettingsDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.BigDecimalPropertyEditor;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class TableDnd implements IsWidget {

	public static final String PANEL_WIDTH = "900px", PANEL_HEIGHT = "650px";
	public static final int CELL_SIZE = 30;

	private final RoomSettingsServiceAsync roomSettingsServiceAsync = GWT.create(RoomSettingsService.class);
	private RoomSettingsDto roomSettingsDto;
	private TextButton closeButton = new TextButton("Zakończ");
	private boolean firstSave = false;

	public TableDnd(RoomSettingsDto roomSettingsDto) {
		this.roomSettingsDto = roomSettingsDto;
	}

	@Override
	public Widget asWidget() {

		AbsolutePanelDnd absolutePanel = new AbsolutePanelDnd();
		absolutePanel.setStyleName("GB2UA-DDAM");
		absolutePanel.setSize(PANEL_WIDTH, PANEL_HEIGHT);

		NumberFormat numberFormat = NumberFormat.getCurrencyFormat();
		NumberField<BigDecimal> price = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(numberFormat));
		price.setEditable(false);
		price.setValue(new BigDecimal(0));

		ToolBoxDnd toolBoxDnd = new ToolBoxDnd();
		DndContext context = new DndContext(toolBoxDnd, absolutePanel, price);
		toolBoxDnd.createRoomElementsDrag(context);
		absolutePanel.setContext(context);

		HorizontalLayoutContainer table = new HorizontalLayoutContainer();
		table.setBorders(true);

		table.add(toolBoxDnd, new HorizontalLayoutData(150, 650));
		table.add(absolutePanel, new HorizontalLayoutData(-1d, -1d));
		table.add(new FieldLabel(price, "Cena wszystkich wypożyczonych elementów"));

		initAbsolutePanelDnd(absolutePanel, context);

		ToolBar toolBar = createToolBar(context);
		VerticalLayoutContainer vlc = new VerticalLayoutContainer();
		vlc.add(toolBar);
		vlc.add(table);

		return vlc;
	}

	private void initAbsolutePanelDnd(AbsolutePanelDnd absolutePanelDnd, DndContext context) {
		for (ArragementDto arragementDto : roomSettingsDto.getArragements()) {
			RoomElementDrag roomElementDrag = new RoomElementDrag(arragementDto.getRoomElement(), CELL_SIZE);
			roomElementDrag.setToolTip(roomElementDrag.getRoomElementDto().getPrice() + " zł");
			ReDraggable d = new ReDraggable(roomElementDrag, context);
			d.setContainer(absolutePanelDnd);
			absolutePanelDnd.add(roomElementDrag, arragementDto.getX(), arragementDto.getY());
		}
	}

	private ToolBar createToolBar(final DndContext context) {
		ToolBar toolBar = new ToolBar();
		TextButton saveBtn = new TextButton("Zapisz");
		TextButton cancelBtn = new TextButton("Anuluj");
		saveBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Iterator<Widget> iterator = context.absolutePanelDnd.iterator();
				List<ArragementDto> arragementDtos = new ArrayList<ArragementDto>();
				while (iterator.hasNext()) {
					RoomElementDrag red = (RoomElementDrag) iterator.next();
					int x = context.absolutePanelDnd.getWidgetLeft(red);
					int y = context.absolutePanelDnd.getWidgetTop(red);
					ArragementDto arragementDto = new ArragementDto();
					arragementDto.setRoomElement(red.getRoomElementDto());
					arragementDto.setX(x);
					arragementDto.setY(y);
					arragementDtos.add(arragementDto);
				}
				roomSettingsDto.setArragements(arragementDtos);
				roomSettingsServiceAsync.save(roomSettingsDto, new AsyncCallback<RoomSettingsDto>() {
					@Override
					public void onSuccess(RoomSettingsDto result) {
						roomSettingsDto = result;
						firstSave = true;
						Info.display("", "Zapisano rozmieszczenie elementów sali");
					}

					@Override
					public void onFailure(Throwable caught) {
						Info.display("Błąd", "Zapis ustawienia elementów wyposażenie nie powiódł się");
					}
				});
			}
		});

		cancelBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.add(new SimpleContainer());
			}
		});

		toolBar.add(saveBtn);
		toolBar.add(cancelBtn);
		toolBar.add(closeButton);
		return toolBar;
	}

	public RoomSettingsDto getRoomSettingsDto() {
		return roomSettingsDto;
	}

	public void setRoomSettingsDto(RoomSettingsDto roomSettingsDto) {
		this.roomSettingsDto = roomSettingsDto;
	}

	public TextButton getCloseButton() {
		return closeButton;
	}

	public boolean isFirstSave() {
		return firstSave;
	}
}