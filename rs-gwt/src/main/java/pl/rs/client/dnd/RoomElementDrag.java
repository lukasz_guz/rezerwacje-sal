package pl.rs.client.dnd;

import pl.rs.client.utils.ImageHelperServlet;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;

/**
 * Kontener przechowujący elemety pokoju wykorzystywany do drag and drop na panel cel
 * 
 * @author lukasz.guz
 * 
 */
public class RoomElementDrag extends SimpleContainer {

	private Image image;
	private RoomElementDto roomElementDto;
	private int size;

	public RoomElementDrag(RoomElementDto roomElementDto, int size) {
		this.roomElementDto = roomElementDto;
		this.image = new Image(ImageHelperServlet.createPhotoPath(roomElementDto.getIcon()));
		this.setBorders(true);
		this.size = size;
		add(image);
		setWidth(size * roomElementDto.getColSpan());
		setHeight(size * roomElementDto.getRowSpan());
	}

	/*
	 * Getters and setters
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public RoomElementDto getRoomElementDto() {
		return roomElementDto;
	}

	public void setRoomElementDto(RoomElementDto roomElementDto) {
		this.roomElementDto = roomElementDto;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}