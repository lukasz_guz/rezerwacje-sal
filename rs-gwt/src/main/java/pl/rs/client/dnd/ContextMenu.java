package pl.rs.client.dnd;

import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.menu.Menu;

public class ContextMenu extends Menu {

	private Widget widgetContainsMenu;

	public ContextMenu(Widget widgetContainsMenu) {
		this.widgetContainsMenu = widgetContainsMenu;
	}

	public Widget getWidgetContainsMenu() {
		return widgetContainsMenu;
	}

	public void setWidgetContainsMenu(Widget widgetContainsMenu) {
		this.widgetContainsMenu = widgetContainsMenu;
	}
}