package pl.rs.client.dnd;

import java.math.BigDecimal;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.MenuItem;

public class AbsolutePanelDnd extends AbsolutePanel {

	private DndContext context;

	@Override
	public void add(Widget w, int left, int top) {
		super.add(w, left, top);
		RoomElementDrag red = (RoomElementDrag) w;
		red.setContextMenu(createContextMenu(red));
		BigDecimal addPrice = red.getRoomElementDto().getPrice();
		if (context != null) {
			BigDecimal newAllPrice = context.price.getValue().add(addPrice);
			context.price.setValue(newAllPrice);
		}
	}

	private ContextMenu createContextMenu(Widget widget) {
		ContextMenu contextMenu = new ContextMenu(widget);
		MenuItem remove = new MenuItem();
		remove.setText("Usuń element");
		remove.addSelectionHandler(new SelectionHandler<Item>() {
			@Override
			public void onSelection(SelectionEvent<Item> event) {
				ContextMenu contextMenu = (ContextMenu) event.getSelectedItem().getParent();
				context.getAbsolutePanelDnd().remove(contextMenu.getWidgetContainsMenu());
			}
		});
		contextMenu.add(remove);
		return contextMenu;
	}

	@Override
	public boolean remove(Widget w) {
		RoomElementDrag red = (RoomElementDrag) w;
		BigDecimal minusPrice = red.getRoomElementDto().getPrice();
		BigDecimal newAllPrice = context.price.getValue().subtract(minusPrice);
		context.price.setValue(newAllPrice);
		return super.remove(w);

	}

	public DndContext getContext() {
		return context;
	}

	public void setContext(DndContext context) {
		this.context = context;
	}
}