package pl.rs.client.dnd;

import java.util.logging.Logger;

import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.fx.client.DragEndEvent;
import com.sencha.gxt.fx.client.DragEndEvent.DragEndHandler;
import com.sencha.gxt.fx.client.Draggable;

/**
 * Obsługa przenoszenia elementów na panel cel.
 * 
 * @author lukasz.guz
 * 
 */
public class ReDraggable extends Draggable {

	public static final String GROUP_NAME = "roomElementTargetGroup";
	private static final Logger log = Logger.getLogger("RoomElementDropTarget");
	private DndContext context;

	public ReDraggable(Widget dragComponent, DndContext context) {
		super(dragComponent);
		this.context = context;

		addDragEndHandler(new DragEndHandler() {
			@Override
			public void onDragEnd(DragEndEvent event) {
				// TODO można usunąć jak się okaże że nie będzie już potrzebne
				int x = event.getX();
				int y = event.getY();
				int top = ReDraggable.this.context.absolutePanelDnd.getAbsoluteTop();
				int offHeight = top + ReDraggable.this.context.absolutePanelDnd.getOffsetHeight();
				int left = ReDraggable.this.context.absolutePanelDnd.getAbsoluteLeft();
				int offWidth = ReDraggable.this.context.absolutePanelDnd.getOffsetWidth();
				log.fine("tableDnd x: " + x + "\ty: " + y + "\ttop: " + top + "\toffHeight: " + offHeight + "\tleft: " + left + "\toffWidth" + offWidth);

				Widget widget = event.getSource().getDragWidget();
				ReDraggable.this.context.absolutePanelDnd.add(widget, x - left, y - top);
			}
		});
	}

	@Override
	protected void onMouseMove(Event event) {
		super.onMouseMove(event);
	}
}