package pl.rs.client.dnd;

import java.math.BigDecimal;

import com.sencha.gxt.widget.core.client.form.NumberField;

public class DndContext {

	protected ToolBoxDnd toolBoxDnd;
	protected AbsolutePanelDnd absolutePanelDnd;
	protected NumberField<BigDecimal> price;

	public DndContext() {
	}

	public DndContext(ToolBoxDnd toolBoxDnd, AbsolutePanelDnd absolutePanelDnd, NumberField<BigDecimal> price) {
		this.toolBoxDnd = toolBoxDnd;
		this.absolutePanelDnd = absolutePanelDnd;
		this.price = price;
	}

	public ToolBoxDnd getToolBoxDnd() {
		return toolBoxDnd;
	}

	public void setToolBoxDnd(ToolBoxDnd toolBoxDnd) {
		this.toolBoxDnd = toolBoxDnd;
	}

	public AbsolutePanelDnd getAbsolutePanelDnd() {
		return absolutePanelDnd;
	}

	public void setAbsolutePanelDnd(AbsolutePanelDnd absolutePanelDnd) {
		this.absolutePanelDnd = absolutePanelDnd;
	}

	public NumberField<BigDecimal> getPrice() {
		return price;
	}

	public void setPrice(NumberField<BigDecimal> price) {
		this.price = price;
	}
}