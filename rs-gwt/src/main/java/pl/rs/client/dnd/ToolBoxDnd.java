package pl.rs.client.dnd;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import pl.rs.client.service.RoomElementService;
import pl.rs.client.service.RoomElementServiceAsync;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.BeforeRemoveEvent;
import com.sencha.gxt.widget.core.client.event.BeforeRemoveEvent.BeforeRemoveHandler;
import com.sencha.gxt.widget.core.client.info.Info;

public class ToolBoxDnd implements IsWidget {

	private static final Logger log = Logger.getLogger("ToolBoxDnd");
	private DndContext context;
	private HorizontalLayoutContainer container;
	private final RoomElementServiceAsync roomElementServiceAsync = GWT.create(RoomElementService.class);
	private List<RoomElementDto> roomElementDtos = new ArrayList<RoomElementDto>();
	int cellSize = 30;

	public void createRoomElementsDrag(DndContext context) {
		this.context = context;
		container = new HorizontalLayoutContainer();
		container.addBeforeRemoveHandler(new BeforeRemoveHandler() {
			@Override
			public void onBeforeRemove(BeforeRemoveEvent event) {
				RoomElementDrag red = (RoomElementDrag) event.getWidget();
				int index = container.getWidgetIndex(red);
				RoomElementDrag newRoomElementDrag = createRoomElementDrag(red.getRoomElementDto());
				container.insert(newRoomElementDrag, index);
			}
		});

		roomElementServiceAsync.findAll(new AsyncCallback<List<RoomElementDto>>() {
			@Override
			public void onSuccess(List<RoomElementDto> result) {
				log.fine("Znaleziono: " + result.size() + " roomElements");
				roomElementDtos = result;
				container.setBorders(true);
				for (RoomElementDto roomElementDto : roomElementDtos) {
					RoomElementDrag roomElementDrag = createRoomElementDrag(roomElementDto);
					container.add(roomElementDrag);
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Info.display("Błąd", "Nie udało się pobrać elementów sali");
			}
		});
	}

	private RoomElementDrag createRoomElementDrag(RoomElementDto roomElementDto) {
		RoomElementDrag roomElementDrag = new RoomElementDrag(roomElementDto, cellSize);
		roomElementDrag.setToolTip(roomElementDrag.getRoomElementDto().getPrice() + " zł");
		ReDraggable d = new ReDraggable(roomElementDrag, ToolBoxDnd.this.context);
		d.setContainer(ToolBoxDnd.this.context.getAbsolutePanelDnd());
		return roomElementDrag;
	}

	@Override
	public Widget asWidget() {
		return container;
	}
}