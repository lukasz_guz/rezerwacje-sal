package pl.rs.client;

import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;

public class RSUncaughtExceptionHandler implements UncaughtExceptionHandler {

	@Override
	public void onUncaughtException(Throwable e) {
		e.printStackTrace();
	}
}