package pl.rs.client.validator;

import java.util.Date;
import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.error.DefaultEditorError;
import com.sencha.gxt.widget.core.client.form.validator.AbstractValidator;

public class PastEndDateValidator extends AbstractValidator<Date> {

	private DateField beginDateField;

	public PastEndDateValidator(DateField beginDateField) {
		this.beginDateField = beginDateField;
	}

	@Override
	public List<EditorError> validate(Editor<Date> editor, Date endDate) {
		Date startDate = beginDateField.getValue();
		if (startDate == null || endDate == null) {
			return null;
		}

		if (endDate.before(startDate)) {
			return createError(new DefaultEditorError(editor, "Data końca rezerwacji nie może być wcześniejsza od jej początku!", endDate));
		}
		return null;
	}
}