package pl.rs.client.validator;

import java.util.Date;
import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.sencha.gxt.widget.core.client.form.error.DefaultEditorError;
import com.sencha.gxt.widget.core.client.form.validator.AbstractValidator;

public class PastDateValidator extends AbstractValidator<Date> {

	@SuppressWarnings("deprecation")
	@Override
	public List<EditorError> validate(Editor<Date> editor, Date date) {
		Date now = new Date();
		now.setSeconds(0);
		now.setMinutes(now.getMinutes() - 1);
		if (date == null) {
			return null;
		}

		if (date.before(now)) {
			return createError(new DefaultEditorError(editor, "Data musi być teraźniejsza lub przyszła!", date));
		}
		return null;
	}
}