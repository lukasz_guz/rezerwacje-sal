package pl.rs.client.validator;

import java.util.List;

import pl.rs.shared.dto.UserDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.sencha.gxt.widget.core.client.form.error.DefaultEditorError;
import com.sencha.gxt.widget.core.client.form.validator.AbstractValidator;

public class ChangePasswordValidator extends AbstractValidator<String> {

	private UserDto userDto;

	public ChangePasswordValidator(UserDto userDto) {
		this.userDto = userDto;
	}

	@Override
	public List<EditorError> validate(Editor<String> editor, String newPassword) {
		String oldPassword = userDto.getPassword();
		if (oldPassword == null && newPassword == null) {
			return null;
		}
		if (oldPassword != null && newPassword != null && oldPassword.equals(newPassword)) {
			return null;
		}
		return createError(new DefaultEditorError(editor, "Hasło jest nieprawidłowe!", newPassword));
	}
}