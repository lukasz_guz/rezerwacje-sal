package pl.rs.client.validator;

import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.error.DefaultEditorError;
import com.sencha.gxt.widget.core.client.form.validator.AbstractValidator;

public class ConfirmPasswordValidator extends AbstractValidator<String> {

	private PasswordField password;

	public ConfirmPasswordValidator(PasswordField password) {
		this.password = password;
	}

	@Override
	public List<EditorError> validate(Editor<String> editor, String confirmPassword) {
		String pass = password.getValue();
		if (pass == null && confirmPassword == null) {
			return null;
		}
		if (pass != null && confirmPassword != null && pass.equals(confirmPassword)) {
			return null;
		}
		return createError(new DefaultEditorError(editor, "Podane hasła nie są identyczne!", confirmPassword));
	}
}