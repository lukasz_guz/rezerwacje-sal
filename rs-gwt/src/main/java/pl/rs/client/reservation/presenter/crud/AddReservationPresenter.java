package pl.rs.client.reservation.presenter.crud;

import pl.rs.shared.dto.ReservationDto;

/**
 * Interfejs prezentera dodawania nowej rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface AddReservationPresenter extends ReservationPresenter {

	void accept(ReservationDto reservationDto);
}