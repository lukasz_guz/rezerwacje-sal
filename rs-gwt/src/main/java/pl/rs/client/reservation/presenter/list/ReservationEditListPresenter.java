package pl.rs.client.reservation.presenter.list;

import pl.rs.shared.dto.ReservationDto;

/**
 * Interfejs prezentera edytowanej listy rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface ReservationEditListPresenter extends ReservationListPresenter {

	void delete(ReservationDto reservationDto);
}