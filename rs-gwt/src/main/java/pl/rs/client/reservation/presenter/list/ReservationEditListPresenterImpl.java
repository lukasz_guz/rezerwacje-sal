package pl.rs.client.reservation.presenter.list;

import pl.rs.client.reservation.view.list.ReservationEditListView;
import pl.rs.client.service.ReservationService;
import pl.rs.client.service.ReservationServiceAsync;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class ReservationEditListPresenterImpl implements ReservationEditListPresenter {

	private final ReservationServiceAsync reservationServiceAsync = GWT.create(ReservationService.class);

	private ReservationEditListView view;

	public ReservationEditListPresenterImpl(ReservationEditListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback) {
		reservationServiceAsync.findAllPagination(loadConfig, callback);
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void delete(ReservationDto reservationDto) {
		reservationServiceAsync.delete(reservationDto, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można usunąć rezerwacji. Skontaktuj się z administratorem");
			}

			@Override
			public void onSuccess(Void result) {
				view.addMessage("", "Usunięto rezerwacje");
				view.updateView();
			}
		});
	}
}