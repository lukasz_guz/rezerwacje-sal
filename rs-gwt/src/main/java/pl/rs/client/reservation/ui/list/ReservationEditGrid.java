package pl.rs.client.reservation.ui.list;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.reservation.presenter.crud.ReservationPresenterImpl;
import pl.rs.client.reservation.presenter.list.ReservationEditListPresenter;
import pl.rs.client.reservation.ui.crud.ReservationViewWindow;
import pl.rs.client.reservation.view.list.ReservationEditListView;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent.RowDoubleClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class ReservationEditGrid extends AbstractGrid<ReservationDto> implements ReservationEditListView {

	private ReservationProperties props = GWT.create(ReservationProperties.class);
	private ReservationEditListPresenter presenter;

	public ReservationEditGrid() {
		initGrid();
		toolBar = createToolBar();
		grid.addRowDoubleClickHandler(new RowDoubleClickHandler() {
			@Override
			public void onRowDoubleClick(RowDoubleClickEvent event) {
				ReservationDto reservationDto = grid.getSelectionModel().getSelectedItem();
				ReservationViewWindow reservationViewWindow = new ReservationViewWindow();
				new ReservationPresenterImpl(reservationDto.getId(), reservationViewWindow);
				reservationViewWindow.show();
			}
		});
	}

	protected RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>> initStandardProxy() {
		RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback) {
				presenter.findAllPagination(loadConfig, callback);
			}
		};
		return proxy;
	}

	protected ColumnModel<ReservationDto> createColumnModel() {
		ColumnConfig<ReservationDto, Date> beginDateCol = new ColumnConfig<ReservationDto, Date>(props.beginReservationDate(), 100, "Rozpoczęcie rezerwacji");
		beginDateCol.setCell(new DateCell(DateTimeFormat.getFormat("HH:mm dd-MM-yyyy")));
		ColumnConfig<ReservationDto, Date> endDateCol = new ColumnConfig<ReservationDto, Date>(props.endReservationDate(), 100, "Zakończenie rezerwacji");
		endDateCol.setCell(new DateCell(DateTimeFormat.getFormat("HH:mm dd-MM-yyyy")));
		ColumnConfig<ReservationDto, String> nameCol = new ColumnConfig<ReservationDto, String>(props.name(), 150, "Nazwa");
		ColumnConfig<ReservationDto, BigDecimal> priceCol = new ColumnConfig<ReservationDto, BigDecimal>(props.price(), 150, "Cena");
		ColumnConfig<ReservationDto, Integer> areaCol = new ColumnConfig<ReservationDto, Integer>(new ValueProvider<ReservationDto, Integer>() {
			@Override
			public Integer getValue(ReservationDto object) {
				return object.getRoom().getArea();
			}

			@Override
			public void setValue(ReservationDto object, Integer value) {
				object.getRoom().setArea(value);
			}

			@Override
			public String getPath() {
				return "room.area";
			}
		}, 50, "Powierzchnia");

		List<ColumnConfig<ReservationDto, ?>> columns = new ArrayList<ColumnConfig<ReservationDto, ?>>();
		columns.add(beginDateCol);
		columns.add(endDateCol);
		columns.add(nameCol);
		columns.add(priceCol);
		columns.add(areaCol);
		return new ColumnModel<ReservationDto>(columns);
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();

		TextButton deleteRoomBtn = new TextButton("Usuń");
		deleteRoomBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				ReservationDto reservationDto = grid.getSelectionModel().getSelectedItem();
				if (reservationDto == null) {
					return;
				}
				presenter.delete(reservationDto);
			}
		});

		toolBar.add(deleteRoomBtn);
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		grid.getLoader().load();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (ReservationEditListPresenter) presenter;
	}
}