package pl.rs.client.reservation.presenter.list;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * Iterfejs prezentera listy rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface ReservationListPresenter extends Presenter {

	void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback);
}