package pl.rs.client.reservation.presenter.crud;

import pl.rs.client.reservation.view.crud.AddReservationView;
import pl.rs.client.service.ReservationService;
import pl.rs.client.service.ReservationServiceAsync;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class AddReservationPresenterImpl implements AddReservationPresenter {

	private final ReservationServiceAsync reservationServiceAsync = GWT.create(ReservationService.class);
	private AddReservationView view;
	private ReservationDto reservationDto;

	public AddReservationPresenterImpl(AddReservationView view) {
		this.view = view;
		bind();
	}

	public AddReservationPresenterImpl(ReservationDto reservationDto, AddReservationView view) {
		this.view = view;
		this.reservationDto = reservationDto;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
		updateView();
	}

	private void updateView() {
		if (reservationDto != null) {
			view.setReservationDto(reservationDto);
			view.setRoomDto(reservationDto.getRoom());
			view.setAddressDto(reservationDto.getRoom().getAddress());
			view.updateView();
		}
	}

	@Override
	public void accept(ReservationDto reservationDto) {
		reservationServiceAsync.save(reservationDto, new AsyncCallback<ReservationDto>() {

			@Override
			public void onSuccess(ReservationDto result) {
				view.addMessage("", "Zapisano rezerwacje");
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można zapisać rezerwacji");
			}
		});
	}
}