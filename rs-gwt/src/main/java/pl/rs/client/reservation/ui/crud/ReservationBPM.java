package pl.rs.client.reservation.ui.crud;

import java.math.BigDecimal;

import pl.rs.client.RSEntryPoint;
import pl.rs.client.dnd.TableDnd;
import pl.rs.client.reservation.presenter.crud.AddReservationPresenterImpl;
import pl.rs.client.room.presenter.list.RoomListPresenterImpl;
import pl.rs.client.room.presenter.search.RoomSearchUserPresenterImpl;
import pl.rs.client.room.ui.list.RoomGrid;
import pl.rs.client.room.ui.search.RoomSearchPanel;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.ArragementDto;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;
import pl.rs.shared.dto.RoomSettingsDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class ReservationBPM {

	private RoomSearchDto roomSearchDto;
	private ReservationDto reservationDto;
	private RoomDto roomDto;
	private RoomSettingsDto roomSettingsDto;

	private RoomSearchPanel roomSearchPanel;
	private TableDnd tableDnd;
	private AddReservationPanel addReservationPanel;

	RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);

	public void start() {
		reservationDto = new ReservationDto();

		RoomGrid roomGrid = new RoomGrid();
		new RoomListPresenterImpl(roomGrid);

		roomSearchPanel = new RoomSearchPanel(roomGrid);
		new RoomSearchUserPresenterImpl(roomSearchPanel);

		prepareGridToolBar(roomSearchPanel);
		RSEntryPoint.centerPanel.clear();
		RSEntryPoint.centerPanel.add(roomSearchPanel);
		RSEntryPoint.centerPanel.forceLayout();
	}

	private void prepareGridToolBar(final RoomSearchPanel roomSearchPanel) {
		ToolBar gridToolBar = roomSearchPanel.getRoomGrid().getToolBar();
		gridToolBar.clear();

		TextButton selectBtn = new TextButton("Wybierz");
		selectBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				roomSearchDto = ReservationBPM.this.roomSearchPanel.getRoomSearchDto();
				if (roomSearchDto.getBeginReservationDate() == null || roomSearchDto.getEndReservationDate() == null) {
					Info.display("Błąd", "Należy uzupełnić datę rozpoczęcia i zakończenia rezerwacji. Następnie wyszukać sali dla podanych terminów");
					return;
				}
				roomDto = roomSearchPanel.getRoomGrid().getGrid().getSelectionModel().getSelectedItem();
				roomServiceAsync.findOne(roomDto.getId(), new AsyncCallback<RoomDto>() {
					@Override
					public void onSuccess(RoomDto result) {
						roomDto = result;
						startRoomSettings();
					}

					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});
		gridToolBar.add(selectBtn);
	}

	private void startRoomSettings() {
		roomSettingsDto = new RoomSettingsDto();
		tableDnd = new TableDnd(roomSettingsDto);
		tableDnd.getCloseButton().addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (!tableDnd.isFirstSave()) {
					Info.display("Błąd", "Zapisz ustawienie sali");
					return;
				}
				roomSettingsDto = tableDnd.getRoomSettingsDto();
				reservationDto.setBeginReservationDate(roomSearchDto.getBeginReservationDate());
				reservationDto.setEndReservationDate(roomSearchDto.getEndReservationDate());
				reservationDto.setRoom(roomDto);
				startAcceptReservation();
			}
		});
		RSEntryPoint.centerPanel.add(tableDnd);
	}

	private void startAcceptReservation() {
		reservationDto.setRoomSettings(roomSettingsDto);
		reservationDto.setPrice(sumPrice(roomDto, roomSettingsDto));

		addReservationPanel = new AddReservationPanel();
		new AddReservationPresenterImpl(reservationDto, addReservationPanel);
		RSEntryPoint.centerPanel.add(addReservationPanel);
	}

	private BigDecimal sumPrice(RoomDto roomDto, RoomSettingsDto roomSettingsDto) {
		BigDecimal reservationPrice = new BigDecimal(roomDto.getPrice().toString());
		for (ArragementDto arragement : roomSettingsDto.getArragements()) {
			reservationPrice = reservationPrice.add(arragement.getRoomElement().getPrice());
		}
		return reservationPrice;
	}

	public void startRoomSettings(ReservationDto reservationDto) {
		this.reservationDto = reservationDto;
		this.roomSettingsDto = reservationDto.getRoomSettings();
		tableDnd = new TableDnd(roomSettingsDto);
		tableDnd.getCloseButton().addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (!tableDnd.isFirstSave()) {
					Info.display("Błąd", "Zapisz ustawienie sali");
					return;
				}
				roomSettingsDto = tableDnd.getRoomSettingsDto();
				startAcceptReservation();
			}
		});
		RSEntryPoint.centerPanel.add(tableDnd);
	}
}