package pl.rs.client.reservation.view.search;

import pl.rs.client.view.View;

/**
 * Interfejs widoku wyszukiwarki rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface ReservationSearchView extends View {

}