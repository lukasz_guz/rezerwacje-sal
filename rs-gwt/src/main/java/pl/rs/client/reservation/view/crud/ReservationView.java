package pl.rs.client.reservation.view.crud;

import pl.rs.client.view.View;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.RoomDto;

public interface ReservationView extends View {

	ReservationDto getReservationDto();

	void setReservationDto(ReservationDto reservationDto);

	RoomDto getRoomDto();

	void setRoomDto(RoomDto roomDto);

	AddressDto getAddressDto();

	void setAddressDto(AddressDto addressDto);
}