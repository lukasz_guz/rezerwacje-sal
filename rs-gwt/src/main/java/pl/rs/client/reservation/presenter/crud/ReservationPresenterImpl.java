package pl.rs.client.reservation.presenter.crud;

import pl.rs.client.reservation.view.crud.ReservationView;
import pl.rs.client.service.ReservationService;
import pl.rs.client.service.ReservationServiceAsync;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ReservationPresenterImpl implements ReservationPresenter {

	private final ReservationServiceAsync reservationServiceAsync = GWT.create(ReservationService.class);
	private ReservationView view;
	private Long reservationId;

	public ReservationPresenterImpl(Long reservationId, ReservationView view) {
		this.reservationId = reservationId;
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
		reservationServiceAsync.findOne(reservationId, new AsyncCallback<ReservationDto>() {

			@Override
			public void onSuccess(ReservationDto result) {
				view.setReservationDto(result);
				view.setRoomDto(result.getRoom());
				view.setAddressDto(result.getRoom().getAddress());
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można wczytać rezerwacji");
			}
		});
	}
}