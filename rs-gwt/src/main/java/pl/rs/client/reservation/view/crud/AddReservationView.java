package pl.rs.client.reservation.view.crud;

/**
 * Interfejs widoku dodawania nowej rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface AddReservationView extends ReservationView {

}