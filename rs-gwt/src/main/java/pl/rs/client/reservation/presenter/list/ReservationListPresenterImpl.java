package pl.rs.client.reservation.presenter.list;

import pl.rs.client.reservation.view.list.ReservationListView;
import pl.rs.client.service.ReservationService;
import pl.rs.client.service.ReservationServiceAsync;
import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class ReservationListPresenterImpl implements ReservationListPresenter {

	private final ReservationServiceAsync reservationServiceAsync = GWT.create(ReservationService.class);

	private ReservationListView view;

	public ReservationListPresenterImpl(ReservationListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback) {
		reservationServiceAsync.findAllPagination(loadConfig, callback);
	}
}