package pl.rs.client.reservation.ui.crud;

import java.math.BigDecimal;

import pl.rs.shared.dto.ReservationDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.i18n.client.NumberFormat;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.BigDecimalPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;

public class ReservationEditor extends Composite implements Editor<ReservationDto> {

	TextField name = new TextField();
	DateField beginReservationDate = new DateField();
	DateField endReservationDate = new DateField();
	NumberField<BigDecimal> price = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(NumberFormat.getCurrencyFormat()));

	public ReservationEditor() {
		initWidget(preparePanel());
	}

	private SimpleContainer preparePanel() {
		VerticalLayoutContainer basicData = new VerticalLayoutContainer();
		basicData.add(new FieldLabel(name, "Nazwa rezerwacj"));
		basicData.add(new FieldLabel(beginReservationDate, "Początek rezerwacji"));
		basicData.add(new FieldLabel(endReservationDate, "Koniec rezerwacji"));
		basicData.add(new FieldLabel(price, "Koszt rezerwacji"));
		SimpleContainer root = new SimpleContainer();
		root.add(basicData);
		return root;
	}

	public void setEnabled(boolean enabled) {
		name.setEnabled(enabled);
		beginReservationDate.setEnabled(enabled);
		endReservationDate.setEnabled(enabled);
		price.setEnabled(enabled);
	}

	public void setReadOnly(boolean readOnly) {
		name.setReadOnly(readOnly);
		beginReservationDate.setReadOnly(readOnly);
		endReservationDate.setReadOnly(readOnly);
		price.setReadOnly(readOnly);
	}
}