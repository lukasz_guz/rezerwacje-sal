package pl.rs.client.reservation.view.list;

import pl.rs.client.view.View;

/**
 * Interfejs widoku listy rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface ReservationListView extends View {

}