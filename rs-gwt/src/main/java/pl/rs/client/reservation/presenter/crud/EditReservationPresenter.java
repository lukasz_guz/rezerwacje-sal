package pl.rs.client.reservation.presenter.crud;

import pl.rs.client.room.presenter.crud.RoomPresenter;

/**
 * Interfejs prezentera edycji rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface EditReservationPresenter extends RoomPresenter {

}