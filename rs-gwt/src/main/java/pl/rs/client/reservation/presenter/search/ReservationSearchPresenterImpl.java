package pl.rs.client.reservation.presenter.search;

import pl.rs.client.reservation.view.search.ReservationSearchView;
import pl.rs.client.service.ReservationService;
import pl.rs.client.service.ReservationServiceAsync;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class ReservationSearchPresenterImpl implements ReservationSearchPresenter {

	private final ReservationServiceAsync reservationServiceAsync = GWT.create(ReservationService.class);

	private ReservationSearchView view;

	public ReservationSearchPresenterImpl(ReservationSearchView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void findAllPaginationSearch(ReservationSearchDto reservationSearchDto, PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback) {
		reservationServiceAsync.findAllPaginationSearch(reservationSearchDto, loadConfig, callback);
	}
}