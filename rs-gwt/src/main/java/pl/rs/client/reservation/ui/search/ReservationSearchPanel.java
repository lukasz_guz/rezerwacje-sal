package pl.rs.client.reservation.ui.search;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.reservation.presenter.search.ReservationSearchPresenter;
import pl.rs.client.reservation.view.search.ReservationSearchView;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class ReservationSearchPanel implements IsWidget, ReservationSearchView {

	interface ReservationSearchDriver extends SimpleBeanEditorDriver<ReservationSearchDto, ReservationSearchEditor> {
	}

	private final ReservationSearchDriver reservationSearchDriver = GWT.create(ReservationSearchDriver.class);

	private ReservationSearchPresenter presenter;
	private ReservationSearchEditor reservationSearchEditor = new ReservationSearchEditor();
	private AbstractGrid<ReservationDto> reservationGrid;
	private ReservationSearchDto reservationSearchDto = new ReservationSearchDto();

	public ReservationSearchPanel(AbstractGrid<ReservationDto> reservationGrid) {
		this.reservationGrid = reservationGrid;
		reservationSearchDriver.initialize(reservationSearchEditor);
		reservationSearchDriver.edit(reservationSearchDto);
		reservationSearchEditor.addValidator();
	}

	@Override
	public Widget asWidget() {
		ToolBar toolBar = createToolBar();
		VerticalLayoutContainer panels = new VerticalLayoutContainer();
		panels.add(toolBar, new VerticalLayoutData(1d, 28d));
		panels.add(reservationSearchEditor, new VerticalLayoutData(1d, 200d));
		SimpleContainer sc = new SimpleContainer();
		sc.add(reservationGrid);
		panels.add(sc, new VerticalLayoutData(1d, 1d));
		return panels;
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton searchBtn = new TextButton("Wyszukaj");
		searchBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				reservationSearchDto = reservationSearchDriver.flush();
				if (reservationSearchDriver.hasErrors()) {
					return;
				}
				changeProxy(reservationSearchDto);
			}
		});
		TextButton clearBtn = new TextButton("Wyczyść");
		clearBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				reservationSearchDto = new ReservationSearchDto();
				reservationSearchDriver.edit(reservationSearchDto);
			}
		});

		toolBar.add(searchBtn);
		toolBar.add(clearBtn);
		return toolBar;
	}

	protected void changeProxy(ReservationSearchDto reservationSearchDto) {
		RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>> proxy = createdProxy(reservationSearchDto);
		PagingLoader<PagingLoadConfig, PagingLoadResult<ReservationDto>> loader = reservationGrid.createPaginLoader(proxy);
		reservationGrid.updateLoader(loader);
	}

	private RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>> createdProxy(final ReservationSearchDto reservationSearchDto) {
		RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<ReservationDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback) {
				presenter.findAllPaginationSearch(reservationSearchDto, loadConfig, callback);
			}
		};
		return proxy;
	}

	public AbstractGrid<ReservationDto> getReservationGrid() {
		return reservationGrid;
	}

	public ReservationSearchDto getReservationSearchDto() {
		return reservationSearchDto;
	}

	public void setReservationSearchDto(ReservationSearchDto reservationSearchDto) {
		this.reservationSearchDto = reservationSearchDto;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (ReservationSearchPresenter) presenter;
	}
}