package pl.rs.client.reservation.presenter.search;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * Interfejs prezentera wyszukiwarki rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public interface ReservationSearchPresenter extends Presenter {

	void findAllPaginationSearch(ReservationSearchDto reservationSearchDto, PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<ReservationDto>> callback);
}