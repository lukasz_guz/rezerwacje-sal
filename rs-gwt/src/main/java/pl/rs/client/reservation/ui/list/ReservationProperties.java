package pl.rs.client.reservation.ui.list;

import java.math.BigDecimal;
import java.util.Date;

import pl.rs.shared.dto.ReservationDto;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface ReservationProperties extends PropertyAccess<ReservationDto> {

	ModelKeyProvider<ReservationDto> id();

	ValueProvider<ReservationDto, Date> beginReservationDate();

	ValueProvider<ReservationDto, Date> endReservationDate();

	ValueProvider<ReservationDto, String> name();

	ValueProvider<ReservationDto, BigDecimal> price();
}