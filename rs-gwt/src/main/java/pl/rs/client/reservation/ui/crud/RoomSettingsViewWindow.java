package pl.rs.client.reservation.ui.crud;

import pl.rs.client.dnd.AbsolutePanelDnd;
import pl.rs.client.dnd.RoomElementDrag;
import pl.rs.client.dnd.TableDnd;
import pl.rs.client.service.RoomSettingsService;
import pl.rs.client.service.RoomSettingsServiceAsync;
import pl.rs.shared.dto.ArragementDto;
import pl.rs.shared.dto.RoomSettingsDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;

public class RoomSettingsViewWindow extends Window {

	private final static String WIDTH = "900 px";
	private final static String HEIGHT = "900 px";

	private final RoomSettingsServiceAsync roomSettingsServiceAsync = GWT.create(RoomSettingsService.class);

	private AbsolutePanelDnd absolutePanelDnd;

	public RoomSettingsViewWindow(Long roomSettingsId) {
		setWidth(WIDTH);
		setHeight(HEIGHT);
		setHeadingText("Podgląd ustawienia");
		setModal(true);
		absolutePanelDnd = createAbsolutePanelDnd(roomSettingsId);
		SimpleContainer root = new SimpleContainer();
		root.add(absolutePanelDnd);
		add(root);
	}

	private AbsolutePanelDnd createAbsolutePanelDnd(Long roomSettingsId) {
		final AbsolutePanelDnd absolutePanelDnd = new AbsolutePanelDnd();
		absolutePanelDnd.setStyleName("GB2UA-DDAM");
		absolutePanelDnd.setStyleName("absolutePanelDnd", true);
		absolutePanelDnd.setSize(TableDnd.PANEL_WIDTH, TableDnd.PANEL_HEIGHT);
		roomSettingsServiceAsync.findOne(roomSettingsId, new AsyncCallback<RoomSettingsDto>() {
			@Override
			public void onSuccess(RoomSettingsDto result) {
				initAbsolutePanelDnd(absolutePanelDnd, result);
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
		return absolutePanelDnd;
	}

	private void initAbsolutePanelDnd(AbsolutePanelDnd absolutePanelDnd, RoomSettingsDto roomSettingsDto) {
		for (ArragementDto arragementDto : roomSettingsDto.getArragements()) {
			RoomElementDrag roomElementDrag = new RoomElementDrag(arragementDto.getRoomElement(), TableDnd.CELL_SIZE);
			roomElementDrag.setToolTip(roomElementDrag.getRoomElementDto().getPrice() + " zł");
			absolutePanelDnd.add(roomElementDrag, arragementDto.getX(), arragementDto.getY());
			roomElementDrag.setContextMenu(null);
		}
	}
}