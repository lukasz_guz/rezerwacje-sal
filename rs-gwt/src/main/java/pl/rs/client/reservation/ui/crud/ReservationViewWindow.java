package pl.rs.client.reservation.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.reservation.presenter.crud.ReservationPresenter;
import pl.rs.client.reservation.view.crud.ReservationView;
import pl.rs.client.room.ui.crud.RoomEditor;
import pl.rs.client.user.ui.crud.AddressEditor;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

/**
 * Główna klasa widoku okienka rezerwacji
 * 
 * @author lukasz.guz
 * 
 */
public class ReservationViewWindow extends Window implements ReservationView {

	interface ReservationDriver extends SimpleBeanEditorDriver<ReservationDto, ReservationEditor> {
	}

	interface RoomDriver extends SimpleBeanEditorDriver<RoomDto, RoomEditor> {
	}

	interface AddressDriver extends SimpleBeanEditorDriver<AddressDto, AddressEditor> {
	}

	protected final static String WIDTH = "850px";
	protected final static String HEIGHT = "500px";

	protected final ReservationDriver reservationDriver = GWT.create(ReservationDriver.class);
	protected final RoomDriver roomDriver = GWT.create(RoomDriver.class);
	protected final AddressDriver roomAddressDriver = GWT.create(AddressDriver.class);

	protected ReservationEditor reservationEditor = new ReservationEditor();
	protected RoomEditor roomEditor = new RoomEditor();
	protected AddressEditor roomAddressEditor = new AddressEditor();

	protected ReservationDto reservationDto = new ReservationDto();
	protected RoomDto roomDto = new RoomDto();
	protected AddressDto addressDto = new AddressDto();

	protected ToolBar toolBar = createToolBar();

	@SuppressWarnings("unused")
	private ReservationPresenter presenter;

	public ReservationViewWindow() {
		setHeadingText("Podgląd rezerwacji");
		setReadOnly(true);
		init();
	}

	protected void init() {
		setWidth(WIDTH);
		setHeight(HEIGHT);
		setModal(true);

		reservationDriver.initialize(reservationEditor);
		reservationDriver.edit(reservationDto);

		roomDriver.initialize(roomEditor);
		roomDriver.edit(roomDto);

		roomAddressDriver.initialize(roomAddressEditor);
		roomAddressDriver.edit(addressDto);

		preparePanel();
	}

	private void setReadOnly(boolean readOnly) {
		reservationEditor.setReadOnly(readOnly);
		roomEditor.setReadOnly(readOnly);
		roomAddressEditor.setReadOnly(readOnly);
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton viewRoomSettings = new TextButton("Podgląd ustawienia sali");

		viewRoomSettings.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				new RoomSettingsViewWindow(reservationDto.getRoomSettings().getId()).show();
			}
		});

		toolBar.add(viewRoomSettings);
		return toolBar;
	}

	private void preparePanel() {
		VerticalLayoutContainer vContainer = new VerticalLayoutContainer();
		vContainer.add(toolBar);

		HorizontalLayoutContainer hcontainer = new HorizontalLayoutContainer();
		hcontainer.add(reservationEditor, new HorizontalLayoutData(-1d, -1d, new Margins(10)));
		hcontainer.add(roomEditor, new HorizontalLayoutData(-1d, -1d, new Margins(10)));
		hcontainer.add(roomAddressEditor, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		vContainer.add(hcontainer, new VerticalLayoutData(-1d, -1d, new Margins(10)));
		this.add(vContainer);
	}

	public ToolBar getToolBar() {
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		reservationDriver.edit(reservationDto);
		roomDriver.edit(roomDto);
		roomAddressDriver.edit(addressDto);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (ReservationPresenter) presenter;
	}

	@Override
	public ReservationDto getReservationDto() {
		return reservationDto;
	}

	@Override
	public void setReservationDto(ReservationDto reservationDto) {
		this.reservationDto = reservationDto;
	}

	@Override
	public RoomDto getRoomDto() {
		return roomDto;
	}

	@Override
	public void setRoomDto(RoomDto roomDto) {
		this.roomDto = roomDto;
	}

	@Override
	public AddressDto getAddressDto() {
		return addressDto;
	}

	@Override
	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}
}