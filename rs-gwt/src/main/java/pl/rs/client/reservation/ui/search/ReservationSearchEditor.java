package pl.rs.client.reservation.ui.search;

import java.math.BigDecimal;

import pl.rs.client.validator.PastEndDateValidator;
import pl.rs.shared.dto.ReservationSearchDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer.HorizontalLayoutData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.BigDecimalPropertyEditor;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.IntegerPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;

public class ReservationSearchEditor extends Composite implements Editor<ReservationSearchDto> {

	// Reservation
	TextField nameReservation = new TextField();
	DateField beginReservationDate = new DateField(new DateTimePropertyEditor(DateTimeFormat.getFormat("HH:mm dd-MM-yyyy")));
	DateField endReservationDate = new DateField(new DateTimePropertyEditor(DateTimeFormat.getFormat("HH:mm dd-MM-yyyy")));
	NumberField<BigDecimal> priceReservation = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(NumberFormat.getCurrencyFormat()));

	// Room
	TextField name = new TextField();
	TextField number = new TextField();
	NumberField<Integer> floor = new NumberField<Integer>(new IntegerPropertyEditor());
	NumberField<BigDecimal> price = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(NumberFormat.getCurrencyFormat()));
	NumberField<Integer> area = new NumberField<Integer>(new IntegerPropertyEditor());

	// Room address
	TextField city = new TextField();
	TextField apartamentNumber = new TextField();
	TextField houseNumber = new TextField();
	TextField street = new TextField();
	TextField postCode = new TextField();

	public ReservationSearchEditor() {
		preparePanel();
		initWidget(preparePanel());
	}

	private SimpleContainer preparePanel() {
		HorizontalLayoutContainer basic = new HorizontalLayoutContainer();

		VerticalLayoutContainer reservationPanel = new VerticalLayoutContainer();
		reservationPanel.add((new FieldLabel(nameReservation, "Nazwa rezerwacji")));
		reservationPanel.add((new FieldLabel(priceReservation, "Cena rezerwacji")));
		reservationPanel.add((new FieldLabel(beginReservationDate, "Od kiedy")));
		reservationPanel.add((new FieldLabel(endReservationDate, "Do kiedy")));
		basic.add(reservationPanel, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		VerticalLayoutContainer roomPanel = new VerticalLayoutContainer();
		roomPanel.add((new FieldLabel(name, "Nazwa sali")));
		roomPanel.add((new FieldLabel(number, "Numer sali")));
		roomPanel.add((new FieldLabel(floor, "Piętro")));
		roomPanel.add((new FieldLabel(price, "Cena")));
		roomPanel.add((new FieldLabel(area, "Powierzchnia")));
		basic.add(roomPanel, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		VerticalLayoutContainer addressPanel = new VerticalLayoutContainer();
		addressPanel.add((new FieldLabel(city, "Miasto")));
		addressPanel.add((new FieldLabel(apartamentNumber, "Numer lokalu")));
		addressPanel.add((new FieldLabel(houseNumber, "Numer domu")));
		addressPanel.add((new FieldLabel(street, "Ulica")));
		addressPanel.add((new FieldLabel(postCode, "Kod pocztowy")));
		basic.add(addressPanel, new HorizontalLayoutData(-1d, -1d, new Margins(10)));

		SimpleContainer root = new SimpleContainer();
		root.add(basic);

		return root;
	}

	public void addValidator() {
		endReservationDate.addValidator(new PastEndDateValidator(beginReservationDate));
	}
}