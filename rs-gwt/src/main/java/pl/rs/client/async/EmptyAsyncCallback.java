package pl.rs.client.async;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Klasa jest zaślepką dla wszystkich wywołań asynchronicznych, w których nie potrzeba żadnej obsługi
 * 
 * @author lukasz.guz
 * 
 * @param <T>
 */
public class EmptyAsyncCallback<T> implements AsyncCallback<T> {

	@Override
	public void onFailure(Throwable caught) {
	}

	@Override
	public void onSuccess(T result) {
	}
}