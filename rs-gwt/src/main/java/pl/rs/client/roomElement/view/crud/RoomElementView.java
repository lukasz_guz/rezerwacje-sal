package pl.rs.client.roomElement.view.crud;

import pl.rs.client.view.View;
import pl.rs.shared.dto.RoomElementDto;

/**
 * Interfejs widoku edycji, podglądu RoomElement
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomElementView extends View {

	RoomElementDto getRoomElementDto();

	void setRoomElementDto(RoomElementDto roomElementDto);
}