package pl.rs.client.roomElement.presenter.list;

import pl.rs.shared.dto.RoomElementDto;

/**
 * Interfejs prezentera dla listy elementów wyposażenia, którą można edytować
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomElementEditListPresenter extends RoomElementListPresenter {

	void delete(RoomElementDto roomElementDto);
}