package pl.rs.client.roomElement.view.crud;

/**
 * Interfejs widoku edycji roomElement
 * 
 * @author lukasz.guz
 * 
 */
public interface EditRoomElementView extends RoomElementView {
}