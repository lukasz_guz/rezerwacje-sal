package pl.rs.client.roomElement.presenter.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.RoomElementDto;

/**
 * Prezenter dodawania nowego roomElement
 * 
 * @author lukasz.guz
 * 
 */
public interface AddRoomElementPresenter extends Presenter {

	void saveOrUpdate(RoomElementDto roomElementDto);
}