package pl.rs.client.roomElement.view.list;

/**
 * Interfejs widoku listy elementów wyposażenia, które można edytować
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomElementEditListView extends RoomElementListView {
}