package pl.rs.client.roomElement.presenter.crud;

import java.io.Serializable;

import pl.rs.client.roomElement.view.crud.EditRoomElementView;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.user.client.rpc.AsyncCallback;

@SuppressWarnings("serial")
public class EditRoomElementPresenterImpl extends AddRoomElementPresenterImpl implements Serializable {

	private Long roomElementId;
	protected EditRoomElementView view;

	public EditRoomElementPresenterImpl(Long roomElementId, EditRoomElementView view) {
		super.view = view;
		this.view = view;
		this.roomElementId = roomElementId;
		bind();
	}

	@Override
	public void bind() {
		super.bind();
		roomElementServiceAsync.findOne(roomElementId, new AsyncCallback<RoomElementDto>() {
			@Override
			public void onSuccess(RoomElementDto result) {
				view.setRoomElementDto(result);
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można wczytać wskazanego elementu");
			}
		});
	}
}