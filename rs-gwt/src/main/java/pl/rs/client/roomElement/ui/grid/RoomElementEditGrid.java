package pl.rs.client.roomElement.ui.grid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.roomElement.presenter.crud.AddRoomElementPresenterImpl;
import pl.rs.client.roomElement.presenter.crud.EditRoomElementPresenterImpl;
import pl.rs.client.roomElement.presenter.list.RoomElementEditListPresenter;
import pl.rs.client.roomElement.ui.crud.AddRoomElementWindow;
import pl.rs.client.roomElement.view.list.RoomElementEditListView;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent.RowDoubleClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class RoomElementEditGrid extends AbstractGrid<RoomElementDto> implements RoomElementEditListView {

	private RoomElementProperties props = GWT.create(RoomElementProperties.class);
	private RoomElementEditListPresenter presenter;

	public RoomElementEditGrid() {
		initGrid();
		toolBar = createToolBar();
		grid.addRowDoubleClickHandler(new RowDoubleClickHandler() {
			@Override
			public void onRowDoubleClick(RowDoubleClickEvent event) {
				RoomElementDto roomElementDto = grid.getSelectionModel().getSelectedItem();
				AddRoomElementWindow addRoomElementWindow = new AddRoomElementWindow();
				new EditRoomElementPresenterImpl(roomElementDto.getId(), addRoomElementWindow);
				addRoomElementWindow.show();
			}
		});
	}

	@Override
	protected RpcProxy<PagingLoadConfig, PagingLoadResult<RoomElementDto>> initStandardProxy() {
		RpcProxy<PagingLoadConfig, PagingLoadResult<RoomElementDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<RoomElementDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomElementDto>> callback) {
				presenter.findAllPagination(loadConfig, callback);
			}
		};
		return proxy;
	}

	@Override
	protected ColumnModel<RoomElementDto> createColumnModel() {
		ColumnConfig<RoomElementDto, String> nameCol = new ColumnConfig<RoomElementDto, String>(props.name(), 150, "Nazwa");
		ColumnConfig<RoomElementDto, BigDecimal> price = new ColumnConfig<RoomElementDto, BigDecimal>(props.price(), 100, "Cena");

		List<ColumnConfig<RoomElementDto, ?>> columns = new ArrayList<ColumnConfig<RoomElementDto, ?>>();
		columns.add(nameCol);
		columns.add(price);
		return new ColumnModel<RoomElementDto>(columns);
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton addUserBtn = new TextButton("Dodaj element wyposażenia");
		addUserBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				AddRoomElementWindow addRoomElementWindow = new AddRoomElementWindow();
				new AddRoomElementPresenterImpl(addRoomElementWindow).bind();
				addRoomElementWindow.show();
			}
		});

		TextButton deleteRoomBtn = new TextButton("Usuń");
		deleteRoomBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				RoomElementDto roomElementDto = grid.getSelectionModel().getSelectedItem();
				if (roomElementDto == null) {
					return;
				}
				presenter.delete(roomElementDto);
			}
		});

		toolBar.add(addUserBtn);
		toolBar.add(deleteRoomBtn);
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		grid.getLoader().load();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (RoomElementEditListPresenter) presenter;
	}
}