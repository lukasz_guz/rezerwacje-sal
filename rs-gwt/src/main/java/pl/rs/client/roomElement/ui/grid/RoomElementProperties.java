package pl.rs.client.roomElement.ui.grid;

import java.math.BigDecimal;

import pl.rs.shared.dto.RoomElementDto;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface RoomElementProperties extends PropertyAccess<RoomElementDto> {

	ModelKeyProvider<RoomElementDto> id();

	ValueProvider<RoomElementDto, String> name();

	ValueProvider<RoomElementDto, BigDecimal> price();
}