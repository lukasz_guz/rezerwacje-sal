package pl.rs.client.roomElement.presenter.list;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * Interfejs prezentera obsługującego listę elementów wyposażenia
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomElementListPresenter extends Presenter {
	void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomElementDto>> callback);
}