package pl.rs.client.roomElement.presenter.crud;

/**
 * Interfejs prezentera, który edytuje sale
 * 
 * @author lukasz.guz
 * 
 */
public interface EditRoomElementPresenter extends AddRoomElementPresenter {

}