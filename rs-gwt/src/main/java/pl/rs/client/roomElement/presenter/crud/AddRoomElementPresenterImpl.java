package pl.rs.client.roomElement.presenter.crud;

import java.io.Serializable;

import pl.rs.client.roomElement.view.crud.RoomElementView;
import pl.rs.client.service.RoomElementService;
import pl.rs.client.service.RoomElementServiceAsync;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

@SuppressWarnings("serial")
public class AddRoomElementPresenterImpl implements AddRoomElementPresenter, Serializable {

	protected final RoomElementServiceAsync roomElementServiceAsync = GWT.create(RoomElementService.class);
	protected RoomElementView view;

	public AddRoomElementPresenterImpl(RoomElementView view) {
		this.view = view;
		bind();
	}

	protected AddRoomElementPresenterImpl() {
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void saveOrUpdate(RoomElementDto roomElementDto) {
		roomElementServiceAsync.save(roomElementDto, new AsyncCallback<RoomElementDto>() {
			@Override
			public void onSuccess(RoomElementDto result) {
				view.setRoomElementDto(result);
				view.updateView();
				view.addMessage("", "Zapisano element pomieszczenia");
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Zapis się nie powiódł");
			}
		});
	}
}