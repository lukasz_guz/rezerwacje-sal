package pl.rs.client.roomElement.presenter.list;

import pl.rs.client.roomElement.view.list.RoomElementEditListView;
import pl.rs.client.service.RoomElementService;
import pl.rs.client.service.RoomElementServiceAsync;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class RoomElementEditListPresenterImpl implements RoomElementEditListPresenter {

	private final RoomElementServiceAsync roomElementServiceAsync = GWT.create(RoomElementService.class);
	private RoomElementEditListView view;

	public RoomElementEditListPresenterImpl(RoomElementEditListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void delete(RoomElementDto roomElementDto) {
		roomElementServiceAsync.delete(roomElementDto, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				view.addMessage("", "Element wyposażenia został usunięty");
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Element wyposażenia został nie może zostać usunięty");
			}
		});
	}

	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomElementDto>> callback) {
		roomElementServiceAsync.findAllPagination(loadConfig, callback);
	}
}