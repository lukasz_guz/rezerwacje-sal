package pl.rs.client.roomElement.ui.crud;

import java.math.BigDecimal;

import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.i18n.client.NumberFormat;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.BigDecimalPropertyEditor;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.IntegerPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxNumberValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinNumberValidator;

public class RoomElementEditor extends Composite implements Editor<RoomElementDto> {

	private NumberFormat numberFormat = NumberFormat.getCurrencyFormat();

	TextField name = new TextField();
	NumberField<BigDecimal> price = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(numberFormat));
	NumberField<Integer> rowSpan = new NumberField<Integer>(new IntegerPropertyEditor());
	NumberField<Integer> colSpan = new NumberField<Integer>(new IntegerPropertyEditor());

	private SimpleContainer root = new SimpleContainer();

	public RoomElementEditor() {
		preparePanel();
		initWidget(root);
	}

	private void preparePanel() {
		VerticalLayoutContainer basicData = new VerticalLayoutContainer();
		basicData.add(new FieldLabel(name, "Nazwa"));
		basicData.add(new FieldLabel(price, "Cena"));
		basicData.add(new FieldLabel(colSpan, "Szerokość elementu w jednostkach"));
		basicData.add(new FieldLabel(rowSpan, "Wysokość elementu w jednostkach"));
		root.add(basicData);
	}

	public void addValidators() {
		name.addValidator(new EmptyValidator<String>());
		rowSpan.addValidator(new MinNumberValidator<Integer>(1));
		rowSpan.addValidator(new MaxNumberValidator<Integer>(5));
		colSpan.addValidator(new MinNumberValidator<Integer>(1));
		colSpan.addValidator(new MaxNumberValidator<Integer>(5));
		price.addValidator(new MinNumberValidator<BigDecimal>(new BigDecimal(0)));
	}
}