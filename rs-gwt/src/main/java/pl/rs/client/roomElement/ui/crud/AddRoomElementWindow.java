package pl.rs.client.roomElement.ui.crud;

import pl.rs.client.event.ChangeIconEvent;
import pl.rs.client.event.ChangeIconEvent.ChangeIconHandler;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.roomElement.presenter.crud.AddRoomElementPresenter;
import pl.rs.client.roomElement.view.crud.EditRoomElementView;
import pl.rs.client.roomElement.view.crud.RoomElementView;
import pl.rs.client.upload.PhotoUpload;
import pl.rs.client.utils.ImageHelperServlet;
import pl.rs.shared.dto.PhotoDto;
import pl.rs.shared.dto.RoomElementDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.ui.Image;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class AddRoomElementWindow extends Window implements RoomElementView, EditRoomElementView {

	interface RoomElementDriver extends SimpleBeanEditorDriver<RoomElementDto, RoomElementEditor> {
	}

	protected final static String WIDTH = "500 px";
	protected final static String HEIGHT = "500 px";
	protected final static String ICON_SIZE = "30px";

	protected final RoomElementDriver roomElementDriver = GWT.create(RoomElementDriver.class);

	protected RoomElementEditor roomElementEditor = new RoomElementEditor();
	protected RoomElementDto roomElementDto = new RoomElementDto();
	protected AddRoomElementPresenter presenter;

	protected SimpleContainer root = new SimpleContainer();
	protected PhotoUpload photoUpload = new PhotoUpload();
	protected Image icon = new Image();
	protected ChangeIconHandler changeIconHandler = new ChangeIconHandler() {
		@Override
		public void changeIcon(ChangeIconEvent event) {
			if (event.getIcon() != null) {
				roomElementDto.setIcon(event.getIcon());
				PhotoDto photo = event.getIcon();
				icon.setUrl(ImageHelperServlet.createPhotoPath(photo.getName()));
			} else if (event.getPhoto() != null) {
				roomElementDto.getPhotos().add(event.getPhoto());
			}
		}
	};

	public AddRoomElementWindow() {
		init();
	}

	protected void init() {
		setSize(WIDTH, HEIGHT);
		setHeadingText("Dodanie nowego elementu wyposażenia");
		setModal(true);
		setResizable(false);
		icon.setSize(ICON_SIZE, ICON_SIZE);

		roomElementEditor.addValidators();

		initializeDriver();
		preparePanel();

		setIcon();
		photoUpload.addChangeIconHandler(changeIconHandler);

		add(root, new MarginData(10));
	}

	protected void initializeDriver() {
		roomElementDriver.initialize(roomElementEditor);
		roomElementDriver.edit(roomElementDto);
	}

	private void preparePanel() {
		VerticalLayoutContainer registrationRoomElementPanel = new VerticalLayoutContainer();
		registrationRoomElementPanel.add(createToolBar());
		registrationRoomElementPanel.add(new FieldLabel(icon, "Ikona"));

		ContentPanel roomElementPanel = new ContentPanel();
		roomElementPanel.setHeadingText("Element wyposażenia");
		roomElementPanel.add(roomElementEditor);
		registrationRoomElementPanel.add(roomElementPanel);
		registrationRoomElementPanel.add(photoUpload);

		root.add(registrationRoomElementPanel);
	}

	protected ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton saveButton = new TextButton("Zapisz");
		saveButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				roomElementDto = roomElementDriver.flush();
				presenter.saveOrUpdate(roomElementDto);
			}
		});
		TextButton cancelButton = new TextButton("Anuluj");
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				hide();
			}
		});

		toolBar.add(saveButton);
		toolBar.add(cancelButton);
		return toolBar;
	}

	protected void setIcon() {
		if (roomElementDto.getIcon() != null) {
			icon.setUrl(ImageHelperServlet.createPhotoPath(roomElementDto.getIcon().getName()));
		}
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (AddRoomElementPresenter) presenter;
	}

	@Override
	public void updateView() {
		roomElementDriver.edit(roomElementDto);
		setIcon();
	}

	@Override
	public RoomElementDto getRoomElementDto() {
		return roomElementDto;
	}

	@Override
	public void setRoomElementDto(RoomElementDto roomElementDto) {
		this.roomElementDto = roomElementDto;
	}
}