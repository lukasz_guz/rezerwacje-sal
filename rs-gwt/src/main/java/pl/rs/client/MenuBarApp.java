package pl.rs.client;

import pl.rs.client.reports.presenter.ReportSearchPresenterImpl;
import pl.rs.client.reports.ui.BlueChart;
import pl.rs.client.reports.ui.ReportSearchPanel;
import pl.rs.client.reservation.presenter.list.ReservationEditListPresenterImpl;
import pl.rs.client.reservation.presenter.list.ReservationListPresenterImpl;
import pl.rs.client.reservation.presenter.search.ReservationSearchPresenterImpl;
import pl.rs.client.reservation.ui.crud.ReservationBPM;
import pl.rs.client.reservation.ui.list.ReservationEditGrid;
import pl.rs.client.reservation.ui.list.ReservationGrid;
import pl.rs.client.reservation.ui.search.ReservationSearchPanel;
import pl.rs.client.reservation.ui.search.ReservationSearchPanelAdmin;
import pl.rs.client.room.presenter.list.RoomEditListPresenterImpl;
import pl.rs.client.room.presenter.search.RoomSearchAdminPresenterImpl;
import pl.rs.client.room.ui.list.RoomEditGrid;
import pl.rs.client.room.ui.search.RoomSearchPanel;
import pl.rs.client.roomElement.presenter.list.RoomElementEditListPresenterImpl;
import pl.rs.client.roomElement.ui.grid.RoomElementEditGrid;
import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;
import pl.rs.client.user.presenter.crud.EditAdminUserPresenterImpl;
import pl.rs.client.user.presenter.list.UserEditListPresenterImpl;
import pl.rs.client.user.ui.crud.EditUserWindow;
import pl.rs.client.user.ui.list.UserEditGrid;
import pl.rs.shared.dto.UserDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class MenuBarApp implements IsWidget {

	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);

	private ToolBar toolBar = new ToolBar();

	public MenuBarApp() {
		toolBar = new ToolBar();

		final TextButton createReservationRoomBtn = new TextButton("Zarezerwuj salę");
		createReservationRoomBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				new ReservationBPM().start();
			}
		});

		final TextButton reservationRoomBtn = new TextButton("Rezerwacje");
		reservationRoomBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.clear();
				userServiceAsync.isAdmin(new AsyncCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean isAdmin) {
						if (isAdmin) {
							ReservationEditGrid reservationEditGrid = new ReservationEditGrid();
							new ReservationEditListPresenterImpl(reservationEditGrid);

							ReservationSearchPanelAdmin reservationSearchPanel = new ReservationSearchPanelAdmin(reservationEditGrid);
							new ReservationSearchPresenterImpl(reservationSearchPanel);

							RSEntryPoint.centerPanel.add(reservationSearchPanel);
						} else {
							ReservationGrid reservationGrid = new ReservationGrid();
							new ReservationListPresenterImpl(reservationGrid);

							ReservationSearchPanel reservationSearchPanel = new ReservationSearchPanel(reservationGrid);
							new ReservationSearchPresenterImpl(reservationSearchPanel);

							RSEntryPoint.centerPanel.add(reservationSearchPanel);
						}
						RSEntryPoint.centerPanel.forceLayout();
					}

					@Override
					public void onFailure(Throwable caught) {
					}
				});
			}
		});

		final TextButton roomButton = new TextButton("Sale");
		roomButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.clear();

				RoomEditGrid roomEditGrid = new RoomEditGrid();
				new RoomEditListPresenterImpl(roomEditGrid);

				RoomSearchPanel roomSearchPanel = new RoomSearchPanel(roomEditGrid);
				new RoomSearchAdminPresenterImpl(roomSearchPanel);

				RSEntryPoint.centerPanel.add(roomSearchPanel);
				RSEntryPoint.centerPanel.forceLayout();
			}
		});

		final TextButton roomElementButton = new TextButton("Elementy wyposażenia");
		roomElementButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.clear();
				RoomElementEditGrid roomElementEditGrid = new RoomElementEditGrid();
				new RoomElementEditListPresenterImpl(roomElementEditGrid);
				RSEntryPoint.centerPanel.add(roomElementEditGrid);
				RSEntryPoint.centerPanel.forceLayout();
			}
		});

		final TextButton usersButton = new TextButton("Użytkownicy");
		usersButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.clear();
				UserEditGrid userEditGrid = new UserEditGrid();
				new UserEditListPresenterImpl(userEditGrid);
				RSEntryPoint.centerPanel.add(userEditGrid);
				RSEntryPoint.centerPanel.forceLayout();
			}
		});

		final TextButton reportsButton = new TextButton("Raporty");
		reportsButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				RSEntryPoint.centerPanel.clear();
				ReportSearchPanel reportSearchPanel = new ReportSearchPanel(new BlueChart());
				new ReportSearchPresenterImpl(reportSearchPanel);
				RSEntryPoint.centerPanel.add(reportSearchPanel);
				RSEntryPoint.centerPanel.forceLayout();
			}
		});

		final TextButton logoutButton = new TextButton("Wyloguj");
		logoutButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "logout");
			}
		});

		toolBar.add(createReservationRoomBtn);
		toolBar.add(reservationRoomBtn);
		toolBar.add(roomButton);
		toolBar.add(roomElementButton);
		toolBar.add(usersButton);
		toolBar.add(reportsButton);
		userServiceAsync.getLoggedUser(new AsyncCallback<UserDto>() {
			@Override
			public void onSuccess(final UserDto result) {
				TextButton user = new TextButton(result.getLogin());
				user.addSelectHandler(new SelectHandler() {
					@Override
					public void onSelect(SelectEvent event) {
						EditUserWindow editUserWindow = new EditUserWindow();
						new EditAdminUserPresenterImpl(result.getId(), editUserWindow);
						editUserWindow.show();
					}
				});
				toolBar.add(user);
				toolBar.add(logoutButton);
				toolBar.forceLayout();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
		userServiceAsync.isAdmin(new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean isAdmin) {
				if (!isAdmin) {
					toolBar.remove(roomButton);
					toolBar.remove(usersButton);
					toolBar.remove(roomElementButton);
					toolBar.remove(reportsButton);
				}
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	@Override
	public Widget asWidget() {
		return toolBar;
	}
}