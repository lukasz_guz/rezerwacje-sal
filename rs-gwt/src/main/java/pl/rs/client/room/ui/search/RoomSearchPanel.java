package pl.rs.client.room.ui.search;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.room.presenter.search.RoomSearchPresenter;
import pl.rs.client.room.view.search.RoomSearchView;
import pl.rs.client.validator.PastDateValidator;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class RoomSearchPanel implements IsWidget, RoomSearchView {

	interface RoomSearchDriver extends SimpleBeanEditorDriver<RoomSearchDto, RoomSearchEditor> {
	}

	private final RoomSearchDriver roomSearchDriver = GWT.create(RoomSearchDriver.class);

	private AbstractGrid<RoomDto> roomGrid;
	private RoomSearchPresenter presenter;
	private RoomSearchDto roomSearchDto = new RoomSearchDto();
	private RoomSearchEditor roomSearchEditor = new RoomSearchEditor();

	public RoomSearchPanel(AbstractGrid<RoomDto> roomGrid) {
		this.roomGrid = roomGrid;
		roomSearchDriver.initialize(roomSearchEditor);
		roomSearchDriver.edit(roomSearchDto);
		addValidators();
	}

	private void addValidators() {
		roomSearchEditor.addValidator();
		roomSearchEditor.beginReservationDate.addValidator(new PastDateValidator());
		roomSearchEditor.endReservationDate.addValidator(new PastDateValidator());
	}

	@Override
	public Widget asWidget() {
		ToolBar toolBar = createToolBar();
		VerticalLayoutContainer panels = new VerticalLayoutContainer();
		panels.add(toolBar, new VerticalLayoutData(1d, 28d));
		panels.add(roomSearchEditor, new VerticalLayoutData(1d, 200d));
		SimpleContainer sc = new SimpleContainer();
		sc.add(roomGrid);
		panels.add(sc, new VerticalLayoutData(1d, 1d));
		return panels;
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton searchBtn = new TextButton("Wyszukaj");
		searchBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				roomSearchDto = roomSearchDriver.flush();
				if (roomSearchDriver.hasErrors()) {
					return;
				}
				changeProxy(roomSearchDto);
			}
		});
		TextButton clearBtn = new TextButton("Wyczyść");
		clearBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				roomSearchDto = new RoomSearchDto();
				roomSearchDriver.edit(roomSearchDto);
			}
		});

		toolBar.add(searchBtn);
		toolBar.add(clearBtn);
		return toolBar;
	}

	protected void changeProxy(RoomSearchDto roomSearchDto) {
		RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> proxy = createdProxy(roomSearchDto);
		PagingLoader<PagingLoadConfig, PagingLoadResult<RoomDto>> loader = roomGrid.createPaginLoader(proxy);
		roomGrid.updateLoader(loader);
	}

	private RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> createdProxy(final RoomSearchDto roomSearchDto) {
		RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
				presenter.findAllPaginSearch(roomSearchDto, loadConfig, callback);
			}
		};
		return proxy;
	}

	public RoomSearchDto getRoomSearchDto() {
		return roomSearchDto;
	}

	public void setRoomSearchDto(RoomSearchDto roomSearchDto) {
		this.roomSearchDto = roomSearchDto;
	}

	public AbstractGrid<RoomDto> getRoomGrid() {
		return roomGrid;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (RoomSearchPresenter) presenter;
	}
}