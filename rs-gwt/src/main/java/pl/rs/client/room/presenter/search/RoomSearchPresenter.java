package pl.rs.client.room.presenter.search;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * Interfejs prezenetera wyszukiwania sal
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomSearchPresenter extends Presenter {

	void findAllPaginSearch(RoomSearchDto roomSearchDto, PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback);
}