package pl.rs.client.room.presenter.list;

import pl.rs.client.presenter.Presenter;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface RoomListPresenter extends Presenter {
	void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback);
}