package pl.rs.client.room.presenter.crud;

import pl.rs.shared.dto.RoomDto;

/**
 * Interfejs prezentera dodającego nową salę
 * 
 * @author lukasz.guz
 * 
 */
public interface AddRoomPresenter extends RoomPresenter {
	void saveOrUpdate(RoomDto roomDto);
}