package pl.rs.client.room.presenter.list;

import pl.rs.shared.dto.RoomDto;

/**
 * Interfejs prezentera edytowanej listy sal
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomEditListPresenter extends RoomListPresenter {
	void delete(RoomDto roomDto);
}