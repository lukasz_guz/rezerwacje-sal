package pl.rs.client.room.presenter.crud;

import pl.rs.client.room.view.crud.AddRoomView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class AddRoomPresenterImpl implements AddRoomPresenter {

	protected final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);

	protected AddRoomView view;

	public AddRoomPresenterImpl(AddRoomView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void saveOrUpdate(RoomDto roomDto) {
		roomServiceAsync.save(roomDto, new AsyncCallback<RoomDto>() {
			@Override
			public void onSuccess(RoomDto result) {
				view.setRoomDto(result);
				view.setAddressDto(result.getAddress());
				view.updateView();
				view.addMessage("", "Zapisano sale");
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można zapisać sali");
			}
		});
	}
}