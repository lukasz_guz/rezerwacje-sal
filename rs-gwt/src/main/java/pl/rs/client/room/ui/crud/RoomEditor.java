package pl.rs.client.room.ui.crud;

import java.math.BigDecimal;

import pl.rs.shared.dto.RoomDto;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.i18n.client.NumberFormat;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.BigDecimalPropertyEditor;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.FloatPropertyEditor;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.IntegerPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinNumberValidator;

public class RoomEditor extends Composite implements Editor<RoomDto> {

	TextField name = new TextField();
	TextField number = new TextField();
	NumberField<Integer> floor = new NumberField<Integer>(new IntegerPropertyEditor());
	NumberField<BigDecimal> price = new NumberField<BigDecimal>(new BigDecimalPropertyEditor(NumberFormat.getCurrencyFormat()));
	NumberField<Float> width = new NumberField<Float>(new FloatPropertyEditor(NumberFormat.getDecimalFormat()));
	NumberField<Float> length = new NumberField<Float>(new FloatPropertyEditor(NumberFormat.getDecimalFormat()));
	NumberField<Integer> area = new NumberField<Integer>(new IntegerPropertyEditor());

	private SimpleContainer root = new SimpleContainer();

	public RoomEditor() {
		preparePanel();
		initWidget(root);
	}

	private void preparePanel() {
		VerticalLayoutContainer basicData = new VerticalLayoutContainer();
		basicData.add(new FieldLabel(name, "Nazwa"));
		basicData.add(new FieldLabel(number, "Numer"));
		basicData.add(new FieldLabel(floor, "Piętro"));
		basicData.add(new FieldLabel(width, "Szerokość"));
		basicData.add(new FieldLabel(length, "Długość"));
		basicData.add(new FieldLabel(area, "Powierzchnia"));
		area.setReadOnly(true);
		basicData.add(new FieldLabel(price, "Cena wynajęcia sali"));
		root.add(basicData);
	}

	public void addValidators() {
		name.addValidator(new EmptyValidator<String>());
		width.addValidator(new MinNumberValidator<Float>(1f));
		length.addValidator(new MinNumberValidator<Float>(1f));
		price.addValidator(new MinNumberValidator<BigDecimal>(new BigDecimal(1)));
	}

	public void setEnabled(boolean editable) {
		name.setEnabled(editable);
		number.setEnabled(editable);
		floor.setEnabled(editable);
		price.setEnabled(editable);
		width.setEnabled(editable);
		length.setEnabled(editable);
		area.setEnabled(editable);
	}

	public void setReadOnly(boolean readOnly) {
		name.setReadOnly(readOnly);
		number.setReadOnly(readOnly);
		floor.setReadOnly(readOnly);
		price.setReadOnly(readOnly);
		width.setReadOnly(readOnly);
		length.setReadOnly(readOnly);
	}
}