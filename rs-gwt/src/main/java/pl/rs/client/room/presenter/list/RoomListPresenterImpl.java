package pl.rs.client.room.presenter.list;

import pl.rs.client.room.view.list.RoomListView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class RoomListPresenterImpl implements RoomListPresenter {

	private final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);
	private RoomListView view;

	public RoomListPresenterImpl(RoomListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
		roomServiceAsync.findAllPagin(loadConfig, callback);
	}
}