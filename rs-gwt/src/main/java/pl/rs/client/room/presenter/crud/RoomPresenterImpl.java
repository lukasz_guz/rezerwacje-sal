package pl.rs.client.room.presenter.crud;

import pl.rs.client.room.view.crud.RoomView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RoomPresenterImpl implements RoomPresenter {

	protected final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);
	protected RoomView view;
	private Long roomId;

	public RoomPresenterImpl(Long roomId, RoomView view) {
		this.roomId = roomId;
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
		roomServiceAsync.findOne(roomId, new AsyncCallback<RoomDto>() {
			@Override
			public void onSuccess(RoomDto result) {
				updateDtos(result);
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można wczytać wskazanego elementu");
			}
		});
	}

	private void updateDtos(RoomDto roomDto) {
		view.setRoomDto(roomDto);
		view.setAddressDto(roomDto.getAddress());
		view.updateView();
	}
}