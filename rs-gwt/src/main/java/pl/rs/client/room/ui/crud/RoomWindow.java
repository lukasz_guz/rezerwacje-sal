package pl.rs.client.room.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.room.presenter.crud.RoomPresenterImpl;
import pl.rs.client.room.view.crud.RoomView;
import pl.rs.client.user.ui.crud.AddressEditor;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class RoomWindow extends Window implements RoomView {

	interface RoomDriver extends SimpleBeanEditorDriver<RoomDto, RoomEditor> {
	}

	interface AddressDriver extends SimpleBeanEditorDriver<AddressDto, AddressEditor> {
	}

	protected final static String WIDTH = "500px";
	protected final static String HEIGHT = "500px";

	protected final RoomDriver roomDriver = GWT.create(RoomDriver.class);
	protected final AddressDriver addressDriver = GWT.create(AddressDriver.class);

	protected RoomEditor roomEditor = new RoomEditor();
	protected AddressEditor addressEditor = new AddressEditor();

	protected RoomDto roomDto = new RoomDto();
	protected AddressDto addressDto = new AddressDto();

	protected ToolBar toolBar = new ToolBar();

	@SuppressWarnings("unused")
	private RoomPresenterImpl presenter;

	public RoomWindow() {
		setHeadingText("Podgląd sali");
		setReadOnly(true);
		init();
	}

	protected void setReadOnly(boolean readOnly) {
		toolBar.setVisible(!readOnly);
		roomEditor.setReadOnly(readOnly);
		addressEditor.setReadOnly(readOnly);
	}

	protected void init() {
		setSize(WIDTH, HEIGHT);
		setModal(true);
		setResizable(false);

		roomEditor.addValidators();
		addressEditor.addValidators();

		roomDriver.initialize(roomEditor);
		roomDriver.edit(roomDto);

		addressDriver.initialize(addressEditor);
		addressDriver.edit(addressDto);

		preparePanel();

		SimpleContainer root = preparePanel();
		add(root, new MarginData(10));
	}

	private SimpleContainer preparePanel() {
		SimpleContainer root = new SimpleContainer();
		VerticalLayoutContainer registrationRoomPanel = new VerticalLayoutContainer();
		registrationRoomPanel.add(toolBar);

		ContentPanel roomDataPanel = new ContentPanel();
		roomDataPanel.setHeadingText("Dane sali");
		roomDataPanel.add(roomEditor);
		registrationRoomPanel.add(roomDataPanel);

		ContentPanel addressPanel = new ContentPanel();
		addressPanel.setHeadingText("Adres sali");
		addressPanel.setResize(true);
		addressPanel.add(addressEditor);
		addressEditor.addValidators();
		registrationRoomPanel.add(addressPanel);

		root.add(registrationRoomPanel);
		return root;
	}

	@Override
	public RoomDto getRoomDto() {
		return roomDto;
	}

	@Override
	public void setRoomDto(RoomDto roomDto) {
		this.roomDto = roomDto;
	}

	@Override
	public AddressDto getAddressDto() {
		return addressDto;
	}

	@Override
	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		roomDriver.edit(roomDto);
		addressDriver.edit(addressDto);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (RoomPresenterImpl) presenter;
	}
}