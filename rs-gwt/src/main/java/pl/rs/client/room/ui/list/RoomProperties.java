package pl.rs.client.room.ui.list;

import java.math.BigDecimal;

import pl.rs.shared.dto.RoomDto;

import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface RoomProperties extends PropertyAccess<RoomDto> {

	ModelKeyProvider<RoomDto> id();

	ValueProvider<RoomDto, String> name();

	ValueProvider<RoomDto, String> number();

	ValueProvider<RoomDto, Integer> floor();

	ValueProvider<RoomDto, BigDecimal> price();

	ValueProvider<RoomDto, Integer> area();
}