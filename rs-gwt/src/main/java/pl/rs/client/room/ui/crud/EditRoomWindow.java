package pl.rs.client.room.ui.crud;

import pl.rs.client.presenter.Presenter;
import pl.rs.client.room.presenter.crud.EditRoomPresenter;
import pl.rs.client.room.view.crud.EditRoomView;

import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class EditRoomWindow extends RoomWindow implements EditRoomView {

	protected EditRoomPresenter presenter;

	public EditRoomWindow() {
		setHeadingText("Edytowanie sali");
		setReadOnly(false);
		toolBar = createToolBar();
		init();
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton saveButton = new TextButton("Zapisz");
		saveButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				roomDto = roomDriver.flush();
				addressDto = addressDriver.flush();
				if (roomDriver.hasErrors() || addressDriver.hasErrors()) {
					return;
				}
				roomDto.setAddress(addressDto);
				presenter.saveOrUpdate(roomDto);
			}
		});
		TextButton cancelButton = new TextButton("Anuluj");
		cancelButton.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				EditRoomWindow.this.hide();
			}
		});
		toolBar.add(saveButton);
		toolBar.add(cancelButton);
		return toolBar;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (EditRoomPresenter) presenter;
	}
}