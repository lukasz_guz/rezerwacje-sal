package pl.rs.client.room.view.crud;

import pl.rs.client.view.View;
import pl.rs.shared.dto.AddressDto;
import pl.rs.shared.dto.RoomDto;

/**
 * Interfejs widoku podglądu sali
 * 
 * @author lukasz.guz
 * 
 */
public interface RoomView extends View {
	RoomDto getRoomDto();

	void setRoomDto(RoomDto roomDto);

	AddressDto getAddressDto();

	void setAddressDto(AddressDto addressDto);
}