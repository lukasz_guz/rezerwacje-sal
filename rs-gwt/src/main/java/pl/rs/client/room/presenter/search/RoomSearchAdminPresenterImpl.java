package pl.rs.client.room.presenter.search;

import pl.rs.client.room.view.search.RoomSearchView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class RoomSearchAdminPresenterImpl implements RoomSearchPresenter {

	private final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);
	private RoomSearchView view;

	public RoomSearchAdminPresenterImpl(RoomSearchView view) {
		this.view = view;
		bind();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void findAllPaginSearch(RoomSearchDto roomSearchDto, PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
		roomServiceAsync.findAllPaginSearchAdmin(roomSearchDto, loadConfig, callback);
	}
}