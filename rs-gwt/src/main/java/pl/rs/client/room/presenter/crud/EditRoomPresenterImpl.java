package pl.rs.client.room.presenter.crud;

import pl.rs.client.room.view.crud.EditRoomView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class EditRoomPresenterImpl implements EditRoomPresenter {

	protected final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);
	protected EditRoomView view;
	private Long roomId;

	public EditRoomPresenterImpl(Long roomId, EditRoomView view) {
		this.roomId = roomId;
		this.view = view;
		bind();
	}

	@Override
	public void saveOrUpdate(RoomDto roomDto) {
		view.setPresenter(this);
		roomServiceAsync.save(roomDto, new AsyncCallback<RoomDto>() {
			@Override
			public void onSuccess(RoomDto result) {
				updateDtos(result);
				view.addMessage("", "Zapisano sale");
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można zapisać sali");
			}
		});
	}

	private void updateDtos(RoomDto roomDto) {
		view.setRoomDto(roomDto);
		view.setAddressDto(roomDto.getAddress());
		view.updateView();
	}

	@Override
	public void bind() {
		view.setPresenter(this);
		roomServiceAsync.findOne(roomId, new AsyncCallback<RoomDto>() {
			@Override
			public void onSuccess(RoomDto result) {
				updateDtos(result);
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Nie można wczytać wskazanego elementu");
			}
		});
	}
}