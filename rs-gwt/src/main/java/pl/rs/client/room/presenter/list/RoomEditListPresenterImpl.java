package pl.rs.client.room.presenter.list;

import pl.rs.client.room.view.list.RoomEditListView;
import pl.rs.client.service.RoomService;
import pl.rs.client.service.RoomServiceAsync;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public class RoomEditListPresenterImpl implements RoomEditListPresenter {

	private final RoomServiceAsync roomServiceAsync = GWT.create(RoomService.class);
	private RoomEditListView view;

	public RoomEditListPresenterImpl(RoomEditListView view) {
		this.view = view;
		bind();
	}

	@Override
	public void findAllPagination(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
		roomServiceAsync.findAllPagin(loadConfig, callback);
	}

	@Override
	public void bind() {
		view.setPresenter(this);
	}

	@Override
	public void delete(RoomDto roomDto) {
		roomServiceAsync.delete(roomDto, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				view.addMessage("", "Sala została usunięta");
				view.updateView();
			}

			@Override
			public void onFailure(Throwable caught) {
				view.addMessage("Błąd", "Sala nie może zostać usunięta");
			}
		});
	}
}