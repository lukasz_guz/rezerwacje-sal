package pl.rs.client.room.ui.list;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.room.presenter.crud.RoomPresenterImpl;
import pl.rs.client.room.presenter.list.RoomListPresenter;
import pl.rs.client.room.ui.crud.RoomWindow;
import pl.rs.client.room.view.list.RoomListView;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent.RowDoubleClickHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.info.Info;

public class RoomGrid extends AbstractGrid<RoomDto> implements RoomListView {

	private RoomProperties props = GWT.create(RoomProperties.class);
	private RoomListPresenter presenter;

	public RoomGrid() {
		initGrid();
		grid.addRowDoubleClickHandler(new RowDoubleClickHandler() {
			@Override
			public void onRowDoubleClick(RowDoubleClickEvent event) {
				RoomDto roomDto = grid.getSelectionModel().getSelectedItem();
				RoomWindow roomWindow = new RoomWindow();
				new RoomPresenterImpl(roomDto.getId(), roomWindow);
				roomWindow.show();
			}
		});
	}

	@Override
	protected RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> initStandardProxy() {
		RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
				presenter.findAllPagination(loadConfig, callback);
			}
		};
		return proxy;
	}

	@Override
	protected ColumnModel<RoomDto> createColumnModel() {
		ColumnConfig<RoomDto, String> nameCol = new ColumnConfig<RoomDto, String>(props.name(), 150, "Nazwa");
		ColumnConfig<RoomDto, String> numberCol = new ColumnConfig<RoomDto, String>(props.number(), 50, "Numer");
		ColumnConfig<RoomDto, Integer> floorCol = new ColumnConfig<RoomDto, Integer>(props.floor(), 50, "Piętro");
		ColumnConfig<RoomDto, BigDecimal> price = new ColumnConfig<RoomDto, BigDecimal>(props.price(), 100, "Cena");
		ColumnConfig<RoomDto, Integer> area = new ColumnConfig<RoomDto, Integer>(props.area(), 100, "Powierzchnia");

		List<ColumnConfig<RoomDto, ?>> columns = new ArrayList<ColumnConfig<RoomDto, ?>>();
		columns.add(nameCol);
		columns.add(numberCol);
		columns.add(floorCol);
		columns.add(price);
		columns.add(area);
		return new ColumnModel<RoomDto>(columns);
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		grid.getLoader().load();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (RoomListPresenter) presenter;
	}
}