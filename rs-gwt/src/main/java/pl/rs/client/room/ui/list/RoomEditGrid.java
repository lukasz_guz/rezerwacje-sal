package pl.rs.client.room.ui.list;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pl.rs.client.grid.AbstractGrid;
import pl.rs.client.presenter.Presenter;
import pl.rs.client.room.presenter.crud.AddRoomPresenterImpl;
import pl.rs.client.room.presenter.crud.EditRoomPresenterImpl;
import pl.rs.client.room.presenter.list.RoomEditListPresenter;
import pl.rs.client.room.ui.crud.AddRoomWindow;
import pl.rs.client.room.ui.crud.EditRoomWindow;
import pl.rs.client.room.view.list.RoomEditListView;
import pl.rs.shared.dto.RoomDto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent.RowDoubleClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

public class RoomEditGrid extends AbstractGrid<RoomDto> implements RoomEditListView {

	private final RoomProperties props = GWT.create(RoomProperties.class);

	private RoomEditListPresenter presenter;

	public RoomEditGrid() {
		initGrid();
		toolBar = createToolBar();
		grid.addRowDoubleClickHandler(new RowDoubleClickHandler() {
			@Override
			public void onRowDoubleClick(RowDoubleClickEvent event) {
				RoomDto roomDto = grid.getSelectionModel().getSelectedItem();
				EditRoomWindow editRoomWindow = new EditRoomWindow();
				new EditRoomPresenterImpl(roomDto.getId(), editRoomWindow);
				editRoomWindow.show();
			}
		});
	}

	@Override
	protected RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> initStandardProxy() {
		RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<RoomDto>>() {
			@Override
			public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<RoomDto>> callback) {
				presenter.findAllPagination(loadConfig, callback);
			}
		};
		return proxy;
	}

	@Override
	protected ColumnModel<RoomDto> createColumnModel() {
		ColumnConfig<RoomDto, String> nameCol = new ColumnConfig<RoomDto, String>(props.name(), 150, "Nazwa");
		ColumnConfig<RoomDto, String> numberCol = new ColumnConfig<RoomDto, String>(props.number(), 50, "Numer");
		ColumnConfig<RoomDto, Integer> floorCol = new ColumnConfig<RoomDto, Integer>(props.floor(), 50, "Piętro");
		ColumnConfig<RoomDto, BigDecimal> price = new ColumnConfig<RoomDto, BigDecimal>(props.price(), 100, "Cena");
		ColumnConfig<RoomDto, Integer> area = new ColumnConfig<RoomDto, Integer>(props.area(), 100, "Powierzchnia");

		List<ColumnConfig<RoomDto, ?>> columns = new ArrayList<ColumnConfig<RoomDto, ?>>();
		columns.add(nameCol);
		columns.add(numberCol);
		columns.add(floorCol);
		columns.add(price);
		columns.add(area);
		return new ColumnModel<RoomDto>(columns);
	}

	private ToolBar createToolBar() {
		ToolBar toolBar = new ToolBar();
		TextButton addUserBtn = new TextButton("Dodaj sale");
		addUserBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				AddRoomWindow addRoomWindow = new AddRoomWindow();
				new AddRoomPresenterImpl(addRoomWindow);
				addRoomWindow.show();
			}
		});

		TextButton deleteRoomBtn = new TextButton("Usuń");
		deleteRoomBtn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				RoomDto roomDto = grid.getSelectionModel().getSelectedItem();
				if (roomDto == null) {
					return;
				}
				presenter.delete(roomDto);
			}
		});

		toolBar.add(addUserBtn);
		toolBar.add(deleteRoomBtn);
		return toolBar;
	}

	@Override
	public void addMessage(String title, String message) {
		Info.display(title, message);
	}

	@Override
	public void updateView() {
		grid.getLoader().load();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (RoomEditListPresenter) presenter;
	}
}