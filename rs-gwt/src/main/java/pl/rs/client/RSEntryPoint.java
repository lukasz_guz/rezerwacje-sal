package pl.rs.client;

import pl.rs.client.reservation.presenter.list.ReservationEditListPresenterImpl;
import pl.rs.client.reservation.presenter.list.ReservationListPresenterImpl;
import pl.rs.client.reservation.presenter.search.ReservationSearchPresenterImpl;
import pl.rs.client.reservation.ui.list.ReservationEditGrid;
import pl.rs.client.reservation.ui.list.ReservationGrid;
import pl.rs.client.reservation.ui.search.ReservationSearchPanel;
import pl.rs.client.reservation.ui.search.ReservationSearchPanelAdmin;
import pl.rs.client.service.UserService;
import pl.rs.client.service.UserServiceAsync;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.container.Viewport;

public class RSEntryPoint implements EntryPoint, IsWidget {

	public static BorderLayoutContainer borderLayoutContainer;
	public static SimpleContainer centerPanel = new SimpleContainer();

	private final UserServiceAsync userServiceAsync = GWT.create(UserService.class);

	@Override
	public Widget asWidget() {
		return createBasicContainer();
	}

	private BorderLayoutContainer createBasicContainer() {
		borderLayoutContainer = new BorderLayoutContainer();
		borderLayoutContainer.setBorders(true);

		ContentPanel head = new ContentPanel();
		head.setHeaderVisible(false);
		head.add(new Image("images/baner.png"));

		BorderLayoutData headData = new BorderLayoutData(100);
		headData.setCollapsible(false);
		headData.setSplit(false);
		headData.setCollapseHidden(true);

		ContentPanel center = new ContentPanel();
		center.setHeaderVisible(false);
		VerticalLayoutContainer centerVP = new VerticalLayoutContainer();
		centerVP.add(new MenuBarApp(), new VerticalLayoutData(1d, 28d));
		centerVP.add(centerPanel, new VerticalLayoutData(1d, 1d));
		center.add(centerVP, new VerticalLayoutData(1d, 1d));

		BorderLayoutData centerData = new BorderLayoutData();
		centerData.setCollapsible(false);
		centerData.setSplit(false);

		RSEntryPoint.centerPanel.clear();

		userServiceAsync.isAdmin(new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean isAdmin) {
				if (isAdmin) {
					ReservationEditGrid reservationEditGrid = new ReservationEditGrid();
					new ReservationEditListPresenterImpl(reservationEditGrid);

					ReservationSearchPanelAdmin reservationSearchPanel = new ReservationSearchPanelAdmin(reservationEditGrid);
					new ReservationSearchPresenterImpl(reservationSearchPanel);

					RSEntryPoint.centerPanel.add(reservationSearchPanel);
				} else {
					ReservationGrid reservationGrid = new ReservationGrid();
					new ReservationListPresenterImpl(reservationGrid);

					ReservationSearchPanel reservationSearchPanel = new ReservationSearchPanel(reservationGrid);
					new ReservationSearchPresenterImpl(reservationSearchPanel);

					RSEntryPoint.centerPanel.add(reservationSearchPanel);
				}
				RSEntryPoint.centerPanel.forceLayout();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
		borderLayoutContainer.setNorthWidget(head, headData);
		borderLayoutContainer.setCenterWidget(center, centerData);

		return borderLayoutContainer;
	}

	@Override
	public void onModuleLoad() {
		GWT.setUncaughtExceptionHandler(new RSUncaughtExceptionHandler());
		Widget homePanel = asWidget();
		Viewport viewport = new Viewport();
		viewport.add(homePanel);
		RootPanel.get().add(viewport);
	}
}