package pl.rs.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.client.service.RoomService;
import pl.rs.service.pagination.RoomAdminPaginationHelper;
import pl.rs.service.pagination.RoomPaginationHelper;
import pl.rs.servicesGxt.RoomServiceGxtImpl;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;

import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@SuppressWarnings("serial")
public class RoomServiceImpl extends SpringGwtServlet implements RoomService {

	private static final Logger log = LoggerFactory.getLogger(RoomServiceImpl.class);

	@Autowired
	private RoomPaginationHelper roomPaginationHelper;

	@Autowired
	private RoomAdminPaginationHelper roomAdminPaginationHelper;

	@Autowired
	private RoomServiceGxtImpl roomServiceGxt;

	@Override
	public RoomDto save(RoomDto roomDto) {
		log.debug("Zapis room");
		return roomServiceGxt.save(roomDto);
	}

	@Override
	public void delete(RoomDto roomDto) {
		log.debug("Usuwanie room o id: " + roomDto.getId());
		roomServiceGxt.delete(roomDto);
	}

	@Override
	public RoomDto findOne(Long roomId) {
		log.debug("Wyszukanie room o id: " + roomId);
		return roomServiceGxt.findOne(roomId);
	}

	@Override
	public List<RoomDto> findAll() {
		log.debug("Pobranie wszystkich room");
		return roomServiceGxt.findAll();
	}

	@Override
	@Transactional
	public PagingLoadResult<RoomDto> findAllPagin(PagingLoadConfig config) {
		return roomPaginationHelper.findAll(config);
	}

	@Override
	public PagingLoadResult<RoomDto> findAllPaginSearch(RoomSearchDto roomSearchDto, PagingLoadConfig config) {
		return roomPaginationHelper.findAll(roomSearchDto, config);
	}

	@Override
	public PagingLoadResult<RoomDto> findAllPaginSearchAdmin(RoomSearchDto roomSearchDto, PagingLoadConfig config) {
		return roomAdminPaginationHelper.findAll(roomSearchDto, config);
	}
}