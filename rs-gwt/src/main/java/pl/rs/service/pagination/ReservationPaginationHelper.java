package pl.rs.service.pagination;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.rs.model.Reservation;
import pl.rs.model.User;
import pl.rs.servicesGxt.ReservationServiceGxtImpl;
import pl.rs.servicesGxt.UserServiceGxtImpl;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;
import pl.rs.shared.dto.Searchable;

@Component
public class ReservationPaginationHelper extends AbstractPaginationHelper<Reservation, ReservationDto> {

	@Autowired
	private ReservationServiceGxtImpl reservationServiceGxt;

	@Autowired
	private UserServiceGxtImpl userServiceGxt;

	@SuppressWarnings("unused")
	@PostConstruct
	private void initAbstractVariable() {
		serviceGxt = reservationServiceGxt;
		headAlias = "reservation";
	}

	@Override
	protected StringBuilder createCoutnQueryBuilder() {
		return new StringBuilder("SELECT COUNT(reservation) FROM Reservation reservation WHERE reservation.deleted=false");
	}

	@Override
	protected StringBuilder createSelectQueryBuilder() {
		return new StringBuilder("SELECT reservation FROM Reservation AS reservation WHERE reservation.deleted=false");
	}

	@Override
	protected void addConditions(StringBuilder selectBuilder) {
		User loggedUser = userServiceGxt.getLoggedUser();
		addOnlyUserReservationCondition(loggedUser, selectBuilder);
	}

	private void addOnlyUserReservationCondition(User user, StringBuilder selectBuilder) {
		if (!UserServiceGxtImpl.ROLE_ADMIN.equals(user.getRole())) {
			selectBuilder.append(" AND reservation.user.id = ");
			selectBuilder.append(user.getId());
		}
	}

	protected void addConditions(StringBuilder selectBuilder, Searchable searchable) {
		ReservationSearchDto reservationSearchDto = (ReservationSearchDto) searchable;

		String reservationConditions = addSearchReservationConditions(reservationSearchDto);
		selectBuilder.append(reservationConditions);

		String roomConditions = addSearchRoomConditions(reservationSearchDto);
		selectBuilder.append(roomConditions);

		String roomAddressConditions = addSearchRoomAddressConditions(reservationSearchDto);
		selectBuilder.append(roomAddressConditions);
	}

	private String addSearchReservationConditions(ReservationSearchDto reservationSearchDto) {
		StringBuilder selectBuilder = new StringBuilder();
		if (!isEmptyOrNull(reservationSearchDto.getNameReservation())) {
			selectBuilder.append(" AND reservation.name LIKE :nameReservation");
		}

		if (reservationSearchDto.getPriceReservation() != null) {
			selectBuilder.append(" AND reservation.price = :priceReservation");
		}

		if (reservationSearchDto.getBeginReservationDate() != null && reservationSearchDto.getEndReservationDate() == null) {
			selectBuilder.append(" AND reservation.beginReservationDate >= :beginReservationDate");
		} else if (reservationSearchDto.getBeginReservationDate() == null && reservationSearchDto.getEndReservationDate() != null) {
			selectBuilder.append(" AND reservation.endReservationDate <= :endReservationDate");
		} else if (reservationSearchDto.getBeginReservationDate() != null && reservationSearchDto.getEndReservationDate() != null) {
			selectBuilder.append(" AND (reservation.beginReservationDate <= :beginReservationDate AND reservation.endReservationDate >= :endReservationDate) OR ");
			selectBuilder.append("(reservation.beginReservationDate > :beginReservationDate AND reservation.beginReservationDate <= :endReservationDate) OR ");
			selectBuilder.append("(reservation.endReservationDate >= :beginReservationDate AND reservation.endReservationDate < :endReservationDate)");
		}
		return selectBuilder.toString();
	}

	private String addSearchRoomConditions(ReservationSearchDto reservationSearchDto) {
		StringBuilder selectBuilder = new StringBuilder();
		if (!isEmptyOrNull(reservationSearchDto.getName())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.name LIKE :name");
		}
		if (!isEmptyOrNull(reservationSearchDto.getNumber())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.number LIKE :number");
		}
		if (reservationSearchDto.getFloor() != null) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.floor = :floor");
		}
		if (reservationSearchDto.getPrice() != null) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.price = :price");
		}
		if (reservationSearchDto.getArea() != null) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.area >= :area");
		}
		return selectBuilder.toString();
	}

	private String addSearchRoomAddressConditions(ReservationSearchDto reservationSearchDto) {
		StringBuilder selectBuilder = new StringBuilder();
		if (!isEmptyOrNull(reservationSearchDto.getCity())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.address.city LIKE :city");
		}
		if (!isEmptyOrNull(reservationSearchDto.getApartamentNumber())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.address.apartamentNumber LIKE :apartamentNumber");
		}
		if (!isEmptyOrNull(reservationSearchDto.getHouseNumber())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.address.houseNumber LIKE :houseNumber");
		}
		if (!isEmptyOrNull(reservationSearchDto.getStreet())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.address.street LIKE :street");
		}
		if (!isEmptyOrNull(reservationSearchDto.getPostCode())) {
			selectBuilder.append(" AND ");
			selectBuilder.append(headAlias);
			selectBuilder.append(".room.address.postCode LIKE :postCode");
		}
		return selectBuilder.toString();
	}
}