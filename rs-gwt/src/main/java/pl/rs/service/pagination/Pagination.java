package pl.rs.service.pagination;

import pl.rs.shared.dto.Searchable;

import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface Pagination<T> {

	public PagingLoadResult<T> findAll(PagingLoadConfig config);

	public PagingLoadResult<T> findAll(Searchable searchable, PagingLoadConfig config);
}