package pl.rs.service.pagination;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.rs.model.RoomElement;
import pl.rs.servicesGxt.RoomElementServiceGxtImpl;
import pl.rs.shared.dto.RoomElementDto;
import pl.rs.shared.dto.Searchable;

@Component
public class RoomElementPaginationHelper extends AbstractPaginationHelper<RoomElement, RoomElementDto> {

	@Autowired
	private RoomElementServiceGxtImpl roomElementServiceGxt;

	@SuppressWarnings("unused")
	@PostConstruct
	private void initAbstractVariable() {
		serviceGxt = roomElementServiceGxt;
		headAlias = "roomElement";
	}

	@Override
	protected StringBuilder createCoutnQueryBuilder() {
		return new StringBuilder("SELECT COUNT(roomElement) FROM RoomElement roomElement WHERE roomElement.deleted=false");
	}

	@Override
	protected StringBuilder createSelectQueryBuilder() {
		return new StringBuilder("SELECT roomElement FROM RoomElement AS roomElement WHERE roomElement.deleted=false");
	}

	@Override
	protected void addConditions(StringBuilder selectBuilder, Searchable searchable) {
		// TODO Jeżeli trzeba będzie dodać wyszukiwarkę to zaimplementować
	}

	@Override
	protected void addConditions(StringBuilder selectBuilder) {
	}
}