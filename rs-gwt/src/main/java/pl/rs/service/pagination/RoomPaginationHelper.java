package pl.rs.service.pagination;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.rs.model.Room;
import pl.rs.servicesGxt.RoomServiceGxtImpl;
import pl.rs.shared.dto.RoomDto;
import pl.rs.shared.dto.RoomSearchDto;
import pl.rs.shared.dto.Searchable;

@Component
public class RoomPaginationHelper extends AbstractPaginationHelper<Room, RoomDto> {

	@Autowired
	protected RoomServiceGxtImpl roomServiceGxt;

	@SuppressWarnings("unused")
	@PostConstruct
	private void initAbstractVariable() {
		serviceGxt = roomServiceGxt;
		headAlias = "room";
	}

	@Override
	protected StringBuilder createCoutnQueryBuilder() {
		return new StringBuilder("SELECT COUNT(room) FROM Room room WHERE room.deleted=false");
	}

	@Override
	protected StringBuilder createSelectQueryBuilder() {
		return new StringBuilder("SELECT room FROM Room AS room WHERE room.deleted=false");
	}

	@Override
	protected void addConditions(StringBuilder selectBuilder, Searchable searchable) {
		RoomSearchDto roomSearchDto = (RoomSearchDto) searchable;
		String roomConditions = addSearchRoomConditions(roomSearchDto);
		selectBuilder.append(roomConditions);

		String roomAddressConditions = addSearchRoomAddressConditions(roomSearchDto);
		selectBuilder.append(roomAddressConditions);

		String reservationConditions = null;
		reservationConditions = addSearchReservationConditions(roomSearchDto);
		if (reservationConditions != null) {
			selectBuilder.append(" AND room NOT IN (");
			selectBuilder.append(reservationConditions);
			selectBuilder.append(")");
		}
	}

	protected String addSearchRoomConditions(RoomSearchDto roomSearchDto) {
		StringBuilder selectBuilder = new StringBuilder();
		if (!isEmptyOrNull(roomSearchDto.getName())) {
			selectBuilder.append(" AND room.name LIKE :name");
		}
		if (!isEmptyOrNull(roomSearchDto.getNumber())) {
			selectBuilder.append(" AND room.number LIKE :number");
		}
		if (roomSearchDto.getFloor() != null) {
			selectBuilder.append(" AND room.floor = :floor");
		}
		if (roomSearchDto.getPrice() != null) {
			selectBuilder.append(" AND room.price = :price");
		}
		if (roomSearchDto.getArea() != null) {
			selectBuilder.append(" AND room.area >= :area");
		}
		return selectBuilder.toString();
	}

	protected String addSearchRoomAddressConditions(RoomSearchDto roomSearchDto) {
		StringBuilder selectBuilder = new StringBuilder();
		if (!isEmptyOrNull(roomSearchDto.getCity())) {
			selectBuilder.append(" AND room.address.city LIKE :city");
		}
		if (!isEmptyOrNull(roomSearchDto.getApartamentNumber())) {
			selectBuilder.append(" AND room.address.apartamentNumber LIKE :apartamentNumber");
		}
		if (!isEmptyOrNull(roomSearchDto.getHouseNumber())) {
			selectBuilder.append(" AND room.address.houseNumber LIKE :houseNumber");
		}
		if (!isEmptyOrNull(roomSearchDto.getStreet())) {
			selectBuilder.append(" AND room.address.street LIKE :street");
		}
		if (!isEmptyOrNull(roomSearchDto.getPostCode())) {
			selectBuilder.append(" AND room.address.postCode LIKE :postCode");
		}
		return selectBuilder.toString();
	}

	protected String addSearchReservationConditions(RoomSearchDto roomSearchDto) {
		StringBuilder selectBuilder = new StringBuilder("SELECT reservation.room FROM Reservation AS reservation");
		if (roomSearchDto.getBeginReservationDate() != null && roomSearchDto.getEndReservationDate() == null) {
			selectBuilder.append(" WHERE reservation.beginReservationDate >= :beginReservationDate");
		} else if (roomSearchDto.getBeginReservationDate() == null && roomSearchDto.getEndReservationDate() != null) {
			selectBuilder.append(" WHERE reservation.endReservationDate <= :endReservationDate");
		} else if (roomSearchDto.getBeginReservationDate() != null && roomSearchDto.getEndReservationDate() != null) {
			selectBuilder.append(" WHERE (reservation.beginReservationDate <= :beginReservationDate AND reservation.endReservationDate >= :endReservationDate) OR ");
			selectBuilder.append("(reservation.beginReservationDate > :beginReservationDate AND reservation.beginReservationDate <= :endReservationDate) OR ");
			selectBuilder.append("(reservation.endReservationDate >= :beginReservationDate AND reservation.endReservationDate < :endReservationDate)");
		} else {
			return null;
		}
		return selectBuilder.toString();
	}

	@Override
	protected void addConditions(StringBuilder selectBuilder) {
	}
}