package pl.rs.service.pagination;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import pl.rs.servicesGxt.ServiceGxt;
import pl.rs.shared.dto.Searchable;

import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;

public abstract class AbstractPaginationHelper<Entity, Dto> implements Pagination<Dto> {

	@PersistenceContext
	protected EntityManager em;

	protected String headAlias;

	protected ServiceGxt<Entity, Dto> serviceGxt;

	@Override
	@Transactional
	public PagingLoadResult<Dto> findAll(Searchable searchable, PagingLoadConfig config) {
		StringBuilder countQueryBuilder = createCoutnQueryBuilder();
		addConditions(countQueryBuilder);
		addConditions(countQueryBuilder, searchable);
		Query countQuery = em.createQuery(countQueryBuilder.toString());
		addParameteToQuery(searchable, searchable.getClass(), countQuery);
		Long totalLength = (Long) countQuery.getSingleResult();
		int totalAmount = totalLength.intValue();

		StringBuilder selectBuilder = createSelectQueryBuilder();
		addConditions(selectBuilder);
		addConditions(selectBuilder, searchable);

		List<? extends SortInfo> sortInfo = config.getSortInfo();
		addSortSql(selectBuilder, sortInfo);

		Query selectQuery = createQuery(selectBuilder.toString(), config);
		addParameteToQuery(searchable, searchable.getClass(), selectQuery);

		return executeQuery(selectQuery, totalAmount, config);
	}

	@Override
	@Transactional
	public PagingLoadResult<Dto> findAll(PagingLoadConfig config) {
		StringBuilder countQueryBuilder = createCoutnQueryBuilder();
		addConditions(countQueryBuilder);
		Query countQuery = em.createQuery(countQueryBuilder.toString());

		Long totalLength = (Long) countQuery.getSingleResult();
		int totalAmount = totalLength.intValue();

		StringBuilder selectBuilder = createSelectQueryBuilder();
		addConditions(selectBuilder);
		List<? extends SortInfo> sortInfo = config.getSortInfo();
		addSortSql(selectBuilder, sortInfo);
		Query selectQuery = createQuery(selectBuilder.toString(), config);

		return executeQuery(selectQuery, totalAmount, config);
	}

	protected abstract StringBuilder createCoutnQueryBuilder();

	protected abstract StringBuilder createSelectQueryBuilder();

	protected abstract void addConditions(StringBuilder selectBuilder, Searchable searchable);

	protected abstract void addConditions(StringBuilder selectBuilder);

	protected void addSortSql(StringBuilder selectBuilder, List<? extends SortInfo> sortInfo) {
		if (sortInfo != null && !sortInfo.isEmpty()) {
			boolean first = true;
			for (SortInfo si : sortInfo) {
				if (first) {
					selectBuilder.append(" ORDER BY ");
					selectBuilder.append(headAlias);
					selectBuilder.append(".");
					selectBuilder.append(si.getSortField());
					selectBuilder.append(" ");
					selectBuilder.append(si.getSortDir());
					first = false;
				} else {
					selectBuilder.append(", ");
					selectBuilder.append(headAlias);
					selectBuilder.append(".");
					selectBuilder.append(si.getSortField());
					selectBuilder.append(" ");
					selectBuilder.append(si.getSortDir());
				}
			}
		}
	}

	public static <Search> void addParameteToQuery(Object searchDto, Class<Search> clazz, Query query) {
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				Object value = field.get(searchDto);
				if (value != null) {
					query.setParameter(field.getName(), value);
					if (value instanceof String) {
						String param = (String) value;
						if (param.isEmpty()) {
							continue;
						}
						query.setParameter(field.getName(), "%" + param + "%");
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	protected Query createQuery(String query, PagingLoadConfig config) {
		Query selectQuery = em.createQuery(query);
		selectQuery.setFirstResult(config.getOffset());
		selectQuery.setMaxResults(config.getLimit());
		return selectQuery;
	}

	protected PagingLoadResult<Dto> executeQuery(Query selectQuery, int totalAmount, PagingLoadConfig config) {
		@SuppressWarnings("unchecked")
		List<Entity> entities = selectQuery.getResultList();
		final List<Dto> dtos = serviceGxt.mapEntitiesToDtoList(entities);
		PagingLoadResult<Dto> paginResult = new PagingLoadResultBean<>(dtos, totalAmount, config.getOffset());
		return paginResult;
	}

	protected boolean isEmptyOrNull(String string) {
		if (string == null || string.isEmpty()) {
			return true;
		}
		return false;
	}
}