package pl.rs.service.pagination;

import org.springframework.stereotype.Component;

import pl.rs.shared.dto.RoomSearchDto;
import pl.rs.shared.dto.Searchable;

@Component
public class RoomAdminPaginationHelper extends RoomPaginationHelper {

	@Override
	protected void addConditions(StringBuilder selectBuilder, Searchable searchable) {
		RoomSearchDto roomSearchDto = (RoomSearchDto) searchable;
		String roomConditions = addSearchRoomConditions(roomSearchDto);
		selectBuilder.append(roomConditions);

		String roomAddressConditions = addSearchRoomAddressConditions(roomSearchDto);
		selectBuilder.append(roomAddressConditions);

		String reservationConditions = null;
		reservationConditions = addSearchReservationConditions(roomSearchDto);
		if (reservationConditions != null) {
			selectBuilder.append(" AND room IN (");
			selectBuilder.append(reservationConditions);
			selectBuilder.append(")");
		}
	}
}