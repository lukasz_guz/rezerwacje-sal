package pl.rs.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import pl.rs.repository.UserRepository;

@Component
public class AuthServiceImpl implements UserDetailsService {

	public static final String LOGGED_ATTRIBUTE = "logged";

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		pl.rs.model.User user = userRepository.findOneByLogin(login);
		if (user == null) {
			throw new UsernameNotFoundException("Nie znaleziono użytkownika!");
		}
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority(user.getRole()));
		org.springframework.security.core.userdetails.User userDetail = new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);

		return userDetail;
	}
}