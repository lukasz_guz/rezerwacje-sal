package pl.rs.service.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pl.rs.client.service.ReportService;
import pl.rs.service.SpringGwtServlet;
import pl.rs.service.pagination.AbstractPaginationHelper;
import pl.rs.shared.dto.ChartData;
import pl.rs.shared.dto.ReportSearchDto;
import pl.rs.shared.dto.Searchable;

@SuppressWarnings("serial")
public class ReportServiceImpl extends SpringGwtServlet implements ReportService {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ChartData> getData() {
		StringBuilder selectBuilder = new StringBuilder(
				"SELECT room.name, sum(reservation.price) FROM Room room, Reservation reservation WHERE room.id = reservation.room.id GROUP BY reservation.room");
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = em.createQuery(selectBuilder.toString()).getResultList();
		return convertToChartData(resultList);
	}

	private List<ChartData> convertToChartData(List<Object[]> list) {
		List<ChartData> data = new ArrayList<>();
		long i = 0;
		for (Object[] columns : list) {
			ChartData cd = new ChartData();
			cd.setId(i++);
			cd.setName((String) columns[0]);
			cd.setData1((BigDecimal) columns[1]);
			cd.setData2((BigDecimal) columns[1]);
			data.add(cd);
		}
		return data;
	}

	@Override
	public List<ChartData> getData(Searchable searchable) {
		ReportSearchDto reportSearchDto = (ReportSearchDto) searchable;
		StringBuilder selectBuilder = new StringBuilder("SELECT room.name, sum(reservation.price) FROM Room room, Reservation reservation WHERE room.id = reservation.room.id");
		if (reportSearchDto.getBeginDate() != null) {
			selectBuilder.append(" AND beginReservationDate >= :beginDate");
		}
		if (reportSearchDto.getEndDate() != null) {
			if (reportSearchDto.getBeginDate() != null) {
				selectBuilder.append(" OR endReservationDate <= :endDate");
			} else {
				selectBuilder.append(" AND endReservationDate <= :endDate");
			}
		}
		if (reportSearchDto.getNameRoom() != null && !reportSearchDto.getNameRoom().isEmpty()) {
			selectBuilder.append(" AND room.name LIKE :nameRoom");
		}
		selectBuilder.append(" GROUP BY reservation.room");
		Query selectQuery = em.createQuery(selectBuilder.toString());
		AbstractPaginationHelper.addParameteToQuery(reportSearchDto, ReportSearchDto.class, selectQuery);
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = selectQuery.getResultList();
		return convertToChartData(resultList);
	}
}