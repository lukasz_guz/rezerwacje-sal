package pl.rs.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.rs.client.service.RoomElementService;
import pl.rs.service.pagination.RoomElementPaginationHelper;
import pl.rs.servicesGxt.RoomElementServiceGxt;
import pl.rs.shared.dto.RoomElementDto;

import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@SuppressWarnings("serial")
public class RoomElementServiceImpl extends SpringGwtServlet implements RoomElementService {

	private static final Logger log = LoggerFactory.getLogger(RoomElementServiceImpl.class);

	@Autowired
	private RoomElementServiceGxt roomElementServiceGxt;

	@Autowired
	private RoomElementPaginationHelper roomElementPaginationHelper;

	@Override
	public RoomElementDto save(RoomElementDto roomElementDto) {
		log.debug("Zapisywanie roomElement");
		roomElementDto = roomElementServiceGxt.save(roomElementDto);
		return roomElementDto;
	}

	@Override
	public RoomElementDto findOne(Long roomElementId) {
		log.debug("Pobranie roomElement o id: " + roomElementId);
		return roomElementServiceGxt.findOneFetch(roomElementId);
	}

	@Override
	public List<RoomElementDto> findAll() {
		log.debug("Pobieranie wszystkich nie usuniętych roomElement");
		return roomElementServiceGxt.findAll();
	}

	@Override
	public PagingLoadResult<RoomElementDto> findAllPagination(PagingLoadConfig config) {
		return roomElementPaginationHelper.findAll(config);
	}

	@Override
	public void delete(RoomElementDto roomElementDto) {
		roomElementServiceGxt.delete(roomElementDto);
	}
}