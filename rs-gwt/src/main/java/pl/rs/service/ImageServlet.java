package pl.rs.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Sevlet służący do pobierania zdjęć z lokalnych zasobów. GXT jest generowane do javascriptu, który nie ma dostępu do lokalnych zasobów serwera.
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class ImageServlet extends HttpServlet {

	private static final String CONTENT_TYPE = "image/jpeg";
	private static final int INPUT_BUFFER_SIZE = 1024;
	public static final String PHOTO_NAME_PARAMETER = "photoName";

	@Value("${photo.dir.path}")
	private String photoDirPath;

	@Override
	public void init() throws ServletException {
		super.init();
		final WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		if (ctx == null) {
			throw new IllegalStateException("No Spring web application context found");
		}
		ctx.getAutowireCapableBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String photoName = req.getParameter(PHOTO_NAME_PARAMETER);

		resp.setContentType(CONTENT_TYPE);

		StringBuilder photoPathBuilder = new StringBuilder(photoDirPath);
		photoPathBuilder.append(File.separator);
		photoPathBuilder.append(photoName);

		File photo = new File(photoPathBuilder.toString());
		resp.setContentLength((int) photo.length());

		FileInputStream in = new FileInputStream(photo);
		OutputStream out = resp.getOutputStream();

		// Copy the contents of the file to the output stream
		byte[] buf = new byte[INPUT_BUFFER_SIZE];
		int count = 0;
		while ((count = in.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
		in.close();
		out.close();
	}

	public String getPhotoDirPath() {
		return photoDirPath;
	}

	public void setPhotoDirPath(String photoDirPath) {
		this.photoDirPath = photoDirPath;
	}
}