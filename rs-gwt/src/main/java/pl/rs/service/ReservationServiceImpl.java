package pl.rs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.client.service.ReservationService;
import pl.rs.service.pagination.ReservationPaginationHelper;
import pl.rs.servicesGxt.ReservationServiceGxt;
import pl.rs.shared.dto.ReservationDto;
import pl.rs.shared.dto.ReservationSearchDto;

import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@SuppressWarnings("serial")
public class ReservationServiceImpl extends SpringGwtServlet implements ReservationService {

	private static final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

	@Autowired
	private ReservationServiceGxt reservationServiceGxt;

	@Autowired
	private ReservationPaginationHelper reservationPaginationHelper;

	@Override
	@Transactional
	public ReservationDto save(ReservationDto reservationDto) {
		String login = getLoginUser();
		return reservationServiceGxt.save(reservationDto, login);
	}

	@Override
	public void delete(ReservationDto reservationDto) {
		String login = getLoginUser();
		reservationServiceGxt.delete(reservationDto, login);
	}

	@Override
	public ReservationDto findOne(Long id) {
		log.debug("Pobieranie rezerwacji o id: " + id);
		return reservationServiceGxt.findOne(id);
	}

	private String getLoginUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.getName();
	}

	@Override
	public PagingLoadResult<ReservationDto> findAllPagination(PagingLoadConfig config) {
		return reservationPaginationHelper.findAll(config);
	}

	@Override
	public PagingLoadResult<ReservationDto> findAllPaginationSearch(ReservationSearchDto reservationSearchDto, PagingLoadConfig config) {
		return reservationPaginationHelper.findAll(reservationSearchDto, config);
	}
}