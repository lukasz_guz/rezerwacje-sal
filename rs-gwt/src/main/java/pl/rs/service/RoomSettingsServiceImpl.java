package pl.rs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.client.service.RoomSettingsService;
import pl.rs.servicesGxt.RoomSettingsServiceGxt;
import pl.rs.shared.dto.RoomSettingsDto;

@SuppressWarnings("serial")
public class RoomSettingsServiceImpl extends SpringGwtServlet implements RoomSettingsService {

	private static final Logger log = LoggerFactory.getLogger(RoomSettingsServiceImpl.class);

	@Autowired
	private RoomSettingsServiceGxt roomSettingsServiceGxt;

	@Override
	@Transactional
	public RoomSettingsDto save(RoomSettingsDto roomSettingsDto) {
		return roomSettingsServiceGxt.save(roomSettingsDto);
	}

	@Override
	@Transactional
	public RoomSettingsDto findOne(Long id) {
		log.debug("Wyszukiwanie roomSettings o id: " + id);
		return roomSettingsServiceGxt.findOne(id);
	}
}