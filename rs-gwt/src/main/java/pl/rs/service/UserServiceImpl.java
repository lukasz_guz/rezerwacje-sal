package pl.rs.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.client.service.UserService;
import pl.rs.model.User;
import pl.rs.servicesGxt.UserServiceGxt;
import pl.rs.shared.dto.UserDto;

import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;

@SuppressWarnings("serial")
public class UserServiceImpl extends SpringGwtServlet implements UserService {

	@Autowired
	private UserServiceGxt userServiceGxt;

	@PersistenceContext
	private EntityManager em;

	@Override
	public UserDto save(UserDto userDto, boolean isAdmin) {
		return userServiceGxt.save(userDto, isAdmin);
	}

	@Override
	public UserDto getLoggedUser() {
		return userServiceGxt.getLoggedUserDto();
	}

	@Override
	public UserDto findOne(Long id) {
		return userServiceGxt.findOne(id);
	}

	@Override
	public List<UserDto> findAll() {
		return userServiceGxt.findAll();
	}

	@Override
	public UserDto findOneByLogin(String login) {
		return userServiceGxt.findOneByLogin(login);
	}

	@Override
	@Transactional
	public PagingLoadResult<UserDto> findAllPagin(PagingLoadConfig config) {
		Long totalLength = (Long) em.createQuery("SELECT COUNT(*) FROM User").getSingleResult();

		StringBuilder selectBuilder = new StringBuilder("SELECT user FROM User AS user");
		List<? extends SortInfo> sortInfo = config.getSortInfo();

		if (sortInfo != null && !sortInfo.isEmpty()) {
			boolean first = true;
			for (SortInfo si : sortInfo) {
				if (first) {
					selectBuilder.append(" ORDER BY user.");
					selectBuilder.append(si.getSortField());
					selectBuilder.append(" ");
					selectBuilder.append(si.getSortDir());
					first = false;
				} else {
					selectBuilder.append(", user.");
					selectBuilder.append(si.getSortField());
					selectBuilder.append(" ");
					selectBuilder.append(si.getSortDir());
				}
			}
		}
		Query selectQuery = em.createQuery(selectBuilder.toString());
		selectQuery.setFirstResult(config.getOffset());
		selectQuery.setMaxResults(config.getLimit());
		@SuppressWarnings("unchecked")
		List<User> users = selectQuery.getResultList();
		final List<UserDto> usersDto = userServiceGxt.mapEntitiesToDtoList(users);
		PagingLoadResult<UserDto> paginResult = new PagingLoadResultBean<>(usersDto, totalLength.intValue(), config.getOffset());
		return paginResult;
	}

	@Override
	public void changePassword(Long userId, String password) {
		userServiceGxt.changePassword(userId, password);
	}

	@Override
	public boolean isAdmin() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		return userServiceGxt.isAdmin(login);
	}

	@Override
	public void logout() {
		getThreadLocalRequest().getSession().invalidate();
	}
}