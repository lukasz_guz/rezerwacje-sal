package pl.rs.shared.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Encja zawierająca kto jaką salę zarezerwował
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class ReservationDto implements Serializable, Idable {

	private Long id;

	private boolean deleted = false;

	private String name;

	private BigDecimal price;

	private UserDto user;

	private RoomDto room;

	private RoomSettingsDto roomSettings;

	private Date beginReservationDate;

	private Date endReservationDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public RoomDto getRoom() {
		return room;
	}

	public void setRoom(RoomDto room) {
		this.room = room;
	}

	public RoomSettingsDto getRoomSettings() {
		return roomSettings;
	}

	public void setRoomSettings(RoomSettingsDto roomSettings) {
		this.roomSettings = roomSettings;
	}

	public Date getBeginReservationDate() {
		return beginReservationDate;
	}

	public void setBeginReservationDate(Date beginReservationDate) {
		this.beginReservationDate = beginReservationDate;
	}

	public Date getEndReservationDate() {
		return endReservationDate;
	}

	public void setEndReservationDate(Date endReservationDate) {
		this.endReservationDate = endReservationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}