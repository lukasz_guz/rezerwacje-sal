package pl.rs.shared.dto;

/**
 * Interfejs ten powinny implementować wszystkie dto, które są używane przez wyszukiwarki.
 * 
 * @author lukasz.guz
 * 
 */
public interface Searchable {
}