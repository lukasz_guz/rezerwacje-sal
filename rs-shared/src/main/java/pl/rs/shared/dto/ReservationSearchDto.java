package pl.rs.shared.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Obiekt transportowy danych z wyszukiwarki sal
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class ReservationSearchDto implements Serializable, Searchable {
	// Reservation
	private String nameReservation;
	private BigDecimal priceReservation;
	private Date beginReservationDate;
	private Date endReservationDate;

	// Room
	private String name;
	private String number;
	private Integer floor;
	private BigDecimal price;
	private Integer area;

	// Room address
	private String city;
	private String apartamentNumber;
	private String houseNumber;
	private String street;
	private String postCode;

	public Date getBeginReservationDate() {
		return beginReservationDate;
	}

	public void setBeginReservationDate(Date beginReservationDate) {
		this.beginReservationDate = beginReservationDate;
	}

	public Date getEndReservationDate() {
		return endReservationDate;
	}

	public void setEndReservationDate(Date endReservationDate) {
		this.endReservationDate = endReservationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getApartamentNumber() {
		return apartamentNumber;
	}

	public void setApartamentNumber(String apartamentNumber) {
		this.apartamentNumber = apartamentNumber;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getNameReservation() {
		return nameReservation;
	}

	public void setNameReservation(String nameReservation) {
		this.nameReservation = nameReservation;
	}

	public BigDecimal getPriceReservation() {
		return priceReservation;
	}

	public void setPriceReservation(BigDecimal priceReservation) {
		this.priceReservation = priceReservation;
	}
}