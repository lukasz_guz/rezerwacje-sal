package pl.rs.shared.dto;

import java.io.Serializable;

/**
 * Encja reprezentująca zdjęcia w systemie
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class PhotoDto implements Serializable, Idable {

	private Long id;
	private boolean deleted = false;
	private String name;
	private String contentType;

	public PhotoDto() {
	}

	public PhotoDto(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}