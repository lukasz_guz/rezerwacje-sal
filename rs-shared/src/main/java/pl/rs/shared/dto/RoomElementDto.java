package pl.rs.shared.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Element sali np. krzesło, stół
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class RoomElementDto implements Serializable, Idable {

	private Long id;
	private boolean deleted = false;
	private String name;
	private int rowSpan = 1;
	private int colSpan = 1;
	private PhotoDto icon;
	private List<PhotoDto> photos;
	private BigDecimal price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public PhotoDto getIcon() {
		return icon;
	}

	public void setIcon(PhotoDto icon) {
		this.icon = icon;
	}

	public List<PhotoDto> getPhotos() {
		if (photos == null) {
			photos = new ArrayList<PhotoDto>();
		}
		return photos;
	}

	public void setPhotos(List<PhotoDto> photos) {
		this.photos = photos;
	}

	public int getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}

	public int getColSpan() {
		return colSpan;
	}

	public void setColSpan(int colSpan) {
		this.colSpan = colSpan;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}