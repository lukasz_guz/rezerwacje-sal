package pl.rs.shared.dto;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
public class ChartData implements Serializable {

	private Long id;
	private String name;
	private BigDecimal data1;
	private BigDecimal data2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getData1() {
		return data1;
	}

	public void setData1(BigDecimal data1) {
		this.data1 = data1;
	}

	public BigDecimal getData2() {
		return data2;
	}

	public void setData2(BigDecimal data2) {
		this.data2 = data2;
	}
}