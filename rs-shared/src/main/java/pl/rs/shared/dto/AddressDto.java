package pl.rs.shared.dto;

import java.io.Serializable;

/**
 * Klasa reprezentująca adres w aplikacji
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class AddressDto implements Serializable, Idable {

	private Long id;
	private boolean deleted = false;
	private String city;
	private String apartamentNumber;
	private String houseNumber;
	private String street;
	private String postCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getApartamentNumber() {
		return apartamentNumber;
	}

	public void setApartamentNumber(String apartamentNumber) {
		this.apartamentNumber = apartamentNumber;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}