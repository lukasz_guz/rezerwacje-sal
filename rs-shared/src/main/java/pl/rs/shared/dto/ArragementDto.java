package pl.rs.shared.dto;

import java.io.Serializable;

/**
 * Klasa przechowująca rozmieszczenie elementu dla danej sali
 * 
 * @author lukasz.guz
 * 
 */
@SuppressWarnings("serial")
public class ArragementDto implements Serializable, Idable {

	private Long id;
	private Integer x;
	private Integer y;
	private ReservationDto reservation;
	private RoomElementDto roomElement;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public ReservationDto getReservation() {
		return reservation;
	}

	public void setReservation(ReservationDto reservation) {
		this.reservation = reservation;
	}

	public RoomElementDto getRoomElement() {
		return roomElement;
	}

	public void setRoomElement(RoomElementDto roomElement) {
		this.roomElement = roomElement;
	}
}