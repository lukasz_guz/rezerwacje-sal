package pl.rs.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RoomSettingsDto implements Serializable {

	private Long id;
	private boolean deleted = false;

	private List<ArragementDto> arragements = new ArrayList<ArragementDto>();

	public List<ArragementDto> getArragements() {
		return arragements;
	}

	public void setArragements(List<ArragementDto> arragements) {
		this.arragements = arragements;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}