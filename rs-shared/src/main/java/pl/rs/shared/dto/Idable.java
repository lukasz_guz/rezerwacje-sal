package pl.rs.shared.dto;

/**
 * Interfejs dla dto, które zawierają id
 * 
 * @author lukasz.guz
 * 
 */
public interface Idable {

	Long getId();
}