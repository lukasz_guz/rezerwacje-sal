package pl.rs.shared.exception;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LoginException extends Exception implements Serializable {

	private String message;

	public LoginException() {
	}

	public LoginException(String message) {
		this.message = message;
		// super(message);
	}

	public String getMessage() {
		return message;
	}
}