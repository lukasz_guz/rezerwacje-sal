package test;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.rs.model.Address;
import pl.rs.model.User;
import pl.rs.repository.UserRepository;
import pl.rs.servicesGxt.RoomElementServiceGxtImpl;
import pl.rs.shared.dto.PhotoDto;
import pl.rs.shared.dto.RoomElementDto;
import pl.rs.shared.dto.UserDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext.xml" })
public class Testowy {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoomElementServiceGxtImpl roomElementServiceGxt;

	@Autowired
	private Mapper mapper;

	@Test
	public void dozerTest() {
		User user = userRepository.findOne(3L);
		UserDto userDto = mapper.map(user, UserDto.class);
		System.out.println(userDto);
	}

	@Test
	public void saveUserWithAddress() {
		System.out.println("ur = " + userRepository);
		User user = new User();
		user.setFirstName("Lukasz");
		user.setLastName("Guz");
		user.setLogin("luk");
		user.setPassword("paa");

		Address address = new Address();
		address.setCity("Lublin");

		user.setAddress(address);

		userRepository.save(user);
		System.out.println(userRepository.findAll());
	}

	@Test
	public void saveUser() {
		User user = new User();
		user.setFirstName("Lukasz");
		user.setLastName("Guz");
		user.setLogin("luk");
		user.setPassword("paa");
		userRepository.save(user);
	}

	@Test
	public void getUserWithAddress() {
		User user = userRepository.findOne(3L);
		System.out.println(user.getAddress());
	}

	@Test
	public void testSaveRoomElementWithIcon() {
		PhotoDto icon = new PhotoDto();
		icon.setName("stol.jpg");

		RoomElementDto re = new RoomElementDto();
		re.setIcon(icon);
		re.setName("stół");
		re.setPrice(new BigDecimal(12));
		re.setRowSpan(1);
		re.setColSpan(3);

		roomElementServiceGxt.save(re);
	}

	@PersistenceContext
	private EntityManager em;

	@Test
	public void test() {
		List<Object[]> resultList = em.createQuery(
				"SELECT room.name, sum(reservation.price) FROM Room room, Reservation reservation WHERE room.id = reservation.room.id GROUP BY reservation.room").getResultList();
		System.out.println(resultList);
	}
}