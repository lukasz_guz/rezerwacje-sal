package pl.rs.servicesGxt;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.model.User;
import pl.rs.repository.UserRepository;
import pl.rs.shared.dto.UserDto;

@Service
public class UserServiceGxtImpl implements UserServiceGxt {

	public static final String ROLE_USER = "ROLE_USER", ROLE_ADMIN = "ROLE_ADMIN";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private Mapper mapper;

	@Transactional
	@Override
	public UserDto findOne(Long id) {
		User user = userRepository.findOne(id);
		// Pobranie adresu z bazy póki jest otwarta sesja
		user.getAddress();
		return mapEntityToDto(user);
	}

	@Transactional
	@Override
	public UserDto save(UserDto userDto, boolean isAdmin) {
		User user = mapDtoToEntity(userDto);
		if (isAdmin) {
			user.setRole(ROLE_ADMIN);
		}
		if (user.getRole() == null || !isAdmin) {
			user.setRole(ROLE_USER);
		}
		user = userRepository.save(user);
		userDto = mapEntityToDto(user);
		return userDto;
	}

	@Transactional
	@Override
	public void changePassword(Long userId, String password) {
		User user = userRepository.findOne(userId);
		user.setPassword(password);
		userRepository.save(user);
	}

	@Transactional
	@Override
	public UserDto findOneByLogin(String login) {
		return mapEntityToDto(userRepository.findOneByLogin(login));
	}

	@Transactional
	@Override
	public User getLoggedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		return userRepository.findOneByLogin(login);
	}

	@Transactional
	@Override
	public UserDto getLoggedUserDto() {
		return mapEntityToDto(getLoggedUser());
	}

	@Transactional
	@Override
	public List<UserDto> findAll() {
		List<User> users = userRepository.findAll();
		List<UserDto> usersDto = mapEntitiesToDtoList(users);
		return usersDto;
	}

	@Override
	public boolean isAdmin(String login) {
		User user = userRepository.findOneByLogin(login);
		if (user == null) {
			return false;
		}
		if (ROLE_ADMIN.equals(user.getRole())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isAdminLoggedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = authentication.getName();
		return isAdmin(login);
	}

	@Override
	public List<User> mapDtosToEntitiesList(List<UserDto> usersDto) {
		List<User> users = new ArrayList<>();
		for (UserDto userDto : usersDto) {
			User user = mapDtoToEntity(userDto);
			users.add(user);
		}
		return users;
	}

	@Override
	public List<UserDto> mapEntitiesToDtoList(List<User> users) {
		List<UserDto> usersDto = new ArrayList<>();
		for (User user : users) {
			UserDto userDto = mapEntityToDto(user);
			usersDto.add(userDto);
		}
		return usersDto;
	}

	@Override
	public User mapDtoToEntity(UserDto userDto) {
		return mapper.map(userDto, User.class);
	}

	@Override
	public UserDto mapEntityToDto(User user) {
		return mapper.map(user, UserDto.class);
	}
}