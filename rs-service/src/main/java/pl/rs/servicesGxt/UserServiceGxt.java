package pl.rs.servicesGxt;

import java.util.List;

import pl.rs.model.User;
import pl.rs.shared.dto.UserDto;

public interface UserServiceGxt extends ServiceGxt<User, UserDto> {

	UserDto findOne(Long id);

	UserDto save(UserDto userDto, boolean isAdmin);

	void changePassword(Long userId, String password);

	UserDto findOneByLogin(String login);

	User getLoggedUser();

	UserDto getLoggedUserDto();

	List<UserDto> findAll();

	boolean isAdmin(String login);

	boolean isAdminLoggedUser();
}