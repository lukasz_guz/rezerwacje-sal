package pl.rs.servicesGxt;

import pl.rs.model.Reservation;
import pl.rs.shared.dto.ReservationDto;

public interface ReservationServiceGxt extends ServiceGxt<Reservation, ReservationDto> {

	ReservationDto save(ReservationDto reservationDto, String login);

	void delete(ReservationDto reservationDto, String login);

	ReservationDto findOne(Long id);
}