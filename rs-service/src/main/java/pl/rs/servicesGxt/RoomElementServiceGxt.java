package pl.rs.servicesGxt;

import java.util.List;

import pl.rs.model.RoomElement;
import pl.rs.shared.dto.RoomElementDto;

public interface RoomElementServiceGxt extends ServiceGxt<RoomElement, RoomElementDto> {

	RoomElementDto save(RoomElementDto roomElementDto);

	void delete(RoomElementDto roomElementDto);

	RoomElementDto findOneFetch(Long id);

	List<RoomElementDto> findAll();
}
