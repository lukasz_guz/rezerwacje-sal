package pl.rs.servicesGxt;

import java.util.List;

import pl.rs.model.Room;
import pl.rs.shared.dto.RoomDto;

public interface RoomServiceGxt extends ServiceGxt<Room, RoomDto> {
	RoomDto save(RoomDto roomDto);

	void delete(RoomDto roomDto);

	RoomDto findOne(Long id);

	List<RoomDto> findAll();
}