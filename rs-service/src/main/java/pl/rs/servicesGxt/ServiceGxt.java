package pl.rs.servicesGxt;

import java.util.List;

public interface ServiceGxt<Entity, Dto> {

	Dto mapEntityToDto(Entity entity);

	Entity mapDtoToEntity(Dto dto);

	List<Dto> mapEntitiesToDtoList(List<Entity> entities);

	List<Entity> mapDtosToEntitiesList(List<Dto> dtos);
}