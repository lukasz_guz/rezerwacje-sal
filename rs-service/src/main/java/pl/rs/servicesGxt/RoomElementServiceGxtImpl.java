package pl.rs.servicesGxt;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.model.RoomElement;
import pl.rs.repository.RoomElementRepository;
import pl.rs.shared.dto.RoomElementDto;

@Component
public class RoomElementServiceGxtImpl implements RoomElementServiceGxt {

	@Autowired
	private RoomElementRepository roomElementRepository;

	@Autowired
	private Mapper mapper;

	@Transactional
	public RoomElementDto save(RoomElementDto roomElementDto) {
		RoomElement roomElement = mapDtoToEntity(roomElementDto);

		roomElement = roomElementRepository.save(roomElement);
		return mapEntityToDto(roomElement);
	}

	public void delete(RoomElementDto roomElementDto) {
		roomElementDto.setDeleted(true);
		save(roomElementDto);
	}

	public RoomElementDto findOneFetch(Long id) {
		RoomElement roomElement = roomElementRepository.findOneFetch(id);
		RoomElementDto roomElementDto = mapEntityToDto(roomElement);
		return roomElementDto;
	}

	public List<RoomElementDto> findAll() {
		List<RoomElementDto> returnList = new ArrayList<>();
		List<RoomElement> listFromBase = roomElementRepository.findAllWithoutDeleted();
		for (RoomElement re : listFromBase) {
			RoomElementDto reDto = mapEntityToDto(re);
			returnList.add(reDto);
		}
		return returnList;
	}

	@Override
	public List<RoomElement> mapDtosToEntitiesList(List<RoomElementDto> roomElementDtos) {
		List<RoomElement> roomElements = new ArrayList<>();
		for (RoomElementDto roomElementDto : roomElementDtos) {
			RoomElement roomElement = mapDtoToEntity(roomElementDto);
			roomElements.add(roomElement);
		}
		return roomElements;
	}

	@Override
	public List<RoomElementDto> mapEntitiesToDtoList(List<RoomElement> roomElements) {
		List<RoomElementDto> roomElementDtos = new ArrayList<>();
		for (RoomElement roomElement : roomElements) {
			RoomElementDto roomElementDto = mapEntityToDto(roomElement);
			roomElementDtos.add(roomElementDto);
		}
		return roomElementDtos;
	}

	@Override
	public RoomElement mapDtoToEntity(RoomElementDto roomElementDto) {
		return mapper.map(roomElementDto, RoomElement.class);
	}

	@Override
	public RoomElementDto mapEntityToDto(RoomElement roomElement) {
		return mapper.map(roomElement, RoomElementDto.class);
	}
}