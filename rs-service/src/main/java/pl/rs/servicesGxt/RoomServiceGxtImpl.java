package pl.rs.servicesGxt;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.rs.model.Room;
import pl.rs.repository.RoomRepository;
import pl.rs.shared.dto.RoomDto;

@Service
public class RoomServiceGxtImpl implements RoomServiceGxt {

	@Autowired
	private Mapper mapper;

	@Autowired
	private RoomRepository roomRepository;

	public RoomDto save(RoomDto roomDto) {
		Float area = roomDto.getWidth() * roomDto.getLength();
		roomDto.setArea(area.intValue());

		Room room = mapDtoToEntity(roomDto);
		room = roomRepository.save(room);
		roomDto = mapEntityToDto(room);
		return roomDto;
	}

	public void delete(RoomDto roomDto) {
		Room room = mapDtoToEntity(roomDto);
		room.setDeleted(true);
		roomRepository.save(room);
	}

	public RoomDto findOne(Long id) {
		// TODO najprawdopodobniej trzeba będzie ściągać rezerwacje bo inaczej zostaną skasowane
		Room room = roomRepository.findOneFetch(id);
		RoomDto roomDto = mapEntityToDto(room);
		return roomDto;
	}

	public List<RoomDto> findAll() {
		List<Room> rooms = roomRepository.findAll();
		List<RoomDto> roomDtos = mapEntitiesToDtoList(rooms);
		return roomDtos;
	}

	@Override
	public List<Room> mapDtosToEntitiesList(List<RoomDto> roomDtos) {
		List<Room> rooms = new ArrayList<>();
		for (RoomDto roomDto : roomDtos) {
			Room room = mapDtoToEntity(roomDto);
			rooms.add(room);
		}
		return rooms;
	}

	@Override
	public List<RoomDto> mapEntitiesToDtoList(List<Room> rooms) {
		List<RoomDto> roomDtos = new ArrayList<>();
		for (Room room : rooms) {
			RoomDto roomDto = mapEntityToDto(room);
			roomDtos.add(roomDto);
		}
		return roomDtos;
	}

	@Override
	public Room mapDtoToEntity(RoomDto roomDto) {
		return mapper.map(roomDto, Room.class);
	}

	@Override
	public RoomDto mapEntityToDto(Room room) {
		return mapper.map(room, RoomDto.class);
	}
}