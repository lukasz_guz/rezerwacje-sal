package pl.rs.servicesGxt;

import pl.rs.model.RoomSettings;
import pl.rs.shared.dto.RoomSettingsDto;

public interface RoomSettingsServiceGxt extends ServiceGxt<RoomSettings, RoomSettingsDto> {

	RoomSettingsDto save(RoomSettingsDto roomSettingsDto);

	RoomSettingsDto findOne(Long id);

}