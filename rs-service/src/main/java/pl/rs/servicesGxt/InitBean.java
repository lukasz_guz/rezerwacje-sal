package pl.rs.servicesGxt;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.model.User;
import pl.rs.repository.UserRepository;

@Component
public class InitBean {

	private String login = "admin", password = "admin";

	@Autowired
	private UserRepository userRepository;

	@SuppressWarnings("unused")
	@PostConstruct
	@Transactional
	private void addAdminUser() {
		User user = userRepository.findOneByLogin(login);
		if (user == null) {
			user = new User();
			user.setLogin(login);
			user.setPassword(password);
			user.setRole(UserServiceGxtImpl.ROLE_ADMIN);
			user.setLastName(login);
			user.setFirstName(login);
			userRepository.save(user);
		}
	}
}