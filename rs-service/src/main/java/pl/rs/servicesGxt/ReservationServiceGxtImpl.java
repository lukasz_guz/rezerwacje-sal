package pl.rs.servicesGxt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.rs.model.Arragement;
import pl.rs.model.Reservation;
import pl.rs.model.Room;
import pl.rs.model.RoomSettings;
import pl.rs.model.User;
import pl.rs.repository.ReservationRepository;
import pl.rs.repository.RoomRepository;
import pl.rs.repository.RoomSettingsRepository;
import pl.rs.repository.UserRepository;
import pl.rs.shared.dto.ReservationDto;

@Service
public class ReservationServiceGxtImpl implements ReservationServiceGxt {

	private static final Logger log = LoggerFactory.getLogger(ReservationServiceGxtImpl.class);
	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private RoomSettingsRepository roomSettingsRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private Mapper mapper;

	@Transactional
	public ReservationDto save(ReservationDto reservationDto, String login) {
		log.debug("Zapisywanie reservation");
		Reservation reservation = mapDtoToEntity(reservationDto);
		Room room = roomRepository.findOne(reservation.getRoom().getId());
		RoomSettings roomSettings = roomSettingsRepository.findOne(reservation.getRoomSettings().getId());
		User user = userRepository.findOneByLogin(login);

		BigDecimal reservationPrice = new BigDecimal(room.getPrice().toString());
		for (Arragement arragement : reservation.getRoomSettings().getArragements()) {
			reservationPrice = reservationPrice.add(arragement.getRoomElement().getPrice());
		}

		reservation.setRoomSettings(null);
		reservation.setRoom(null);
		reservation.setUser(null);
		reservation = reservationRepository.save(reservation);

		reservation.setRoomSettings(roomSettings);
		reservation.setRoom(room);
		reservation.setUser(user);

		reservation.setPrice(reservationPrice);
		reservation = reservationRepository.save(reservation);
		reservationDto = mapEntityToDto(reservation);
		return reservationDto;
	}

	public void delete(ReservationDto reservationDto, String login) {
		reservationDto.setDeleted(true);
		save(reservationDto, login);
	}

	public ReservationDto findOne(Long id) {
		Reservation reservation = reservationRepository.findOneFetch(id);
		return mapEntityToDto(reservation);
	}

	@Override
	public List<Reservation> mapDtosToEntitiesList(List<ReservationDto> reservationDtos) {
		List<Reservation> reservations = new ArrayList<>();
		for (ReservationDto reservationDto : reservationDtos) {
			Reservation reservation = mapDtoToEntity(reservationDto);
			reservations.add(reservation);
		}
		return reservations;
	}

	@Override
	public List<ReservationDto> mapEntitiesToDtoList(List<Reservation> reservations) {
		List<ReservationDto> reservationDtos = new ArrayList<>();
		for (Reservation reservation : reservations) {
			ReservationDto reservationDto = mapEntityToDto(reservation);
			reservationDtos.add(reservationDto);
		}
		return reservationDtos;
	}

	@Override
	public Reservation mapDtoToEntity(ReservationDto reservationDto) {
		return mapper.map(reservationDto, Reservation.class);
	}

	@Override
	public ReservationDto mapEntityToDto(Reservation reservation) {
		return mapper.map(reservation, ReservationDto.class);
	}
}