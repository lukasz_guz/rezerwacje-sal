package pl.rs.servicesGxt;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.rs.model.RoomSettings;
import pl.rs.repository.RoomSettingsRepository;
import pl.rs.shared.dto.RoomSettingsDto;

@Service
public class RoomSettingsServiceGxtImpl implements RoomSettingsServiceGxt {

	private static final Logger log = LoggerFactory.getLogger(RoomSettingsServiceGxtImpl.class);

	@Autowired
	private Mapper mapper;

	@Autowired
	private RoomSettingsRepository roomSettingsRepository;

	@Override
	public RoomSettingsDto save(RoomSettingsDto roomSettingsDto) {
		log.debug("Zapisywanie roomSettings");
		RoomSettings roomSettings = mapDtoToEntity(roomSettingsDto);
		roomSettings = roomSettingsRepository.save(roomSettings);
		roomSettings.getArragements();
		roomSettingsDto = mapEntityToDto(roomSettings);
		return roomSettingsDto;
	}

	@Override
	public RoomSettingsDto findOne(Long id) {
		RoomSettings roomSettings = roomSettingsRepository.findOneFetch(id);
		RoomSettingsDto roomSettingsDto = mapEntityToDto(roomSettings);
		return roomSettingsDto;
	}

	@Override
	public List<RoomSettings> mapDtosToEntitiesList(List<RoomSettingsDto> roomSettingsDtos) {
		List<RoomSettings> roomSettingsList = new ArrayList<>();
		for (RoomSettingsDto roomSettingsDto : roomSettingsDtos) {
			RoomSettings roomSettings = mapDtoToEntity(roomSettingsDto);
			roomSettingsList.add(roomSettings);
		}
		return roomSettingsList;
	}

	@Override
	public List<RoomSettingsDto> mapEntitiesToDtoList(List<RoomSettings> roomSettingsList) {
		List<RoomSettingsDto> roomSettingsDtos = new ArrayList<>();
		for (RoomSettings roomSettings : roomSettingsList) {
			RoomSettingsDto roomSettingsDto = mapEntityToDto(roomSettings);
			roomSettingsDtos.add(roomSettingsDto);
		}
		return roomSettingsDtos;
	}

	@Override
	public RoomSettings mapDtoToEntity(RoomSettingsDto roomSettingsDto) {
		return mapper.map(roomSettingsDto, RoomSettings.class);
	}

	@Override
	public RoomSettingsDto mapEntityToDto(RoomSettings roomSettings) {
		return mapper.map(roomSettings, RoomSettingsDto.class);
	}
}