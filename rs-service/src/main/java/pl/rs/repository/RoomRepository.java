package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.rs.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

	@Query("SELECT r FROM Room r LEFT JOIN FETCH r.address AS address LEFT JOIN FETCH r.photos AS photos WHERE r.id=:id")
	Room findOneFetch(@Param("id") Long id);
}