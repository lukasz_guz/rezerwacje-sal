package pl.rs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.rs.model.RoomElement;

@Repository
public interface RoomElementRepository extends JpaRepository<RoomElement, Long> {

	@Query("SELECT re FROM RoomElement AS re LEFT JOIN FETCH re.icon AS photo LEFT JOIN FETCH re.photos AS photos WHERE re.id=:id AND re.deleted=false")
	RoomElement findOneFetch(@Param("id") Long id);

	@Query("SELECT re FROM RoomElement AS re LEFT JOIN re.icon AS photo WHERE re.deleted=false")
	List<RoomElement> findAllWithoutDeleted();
}