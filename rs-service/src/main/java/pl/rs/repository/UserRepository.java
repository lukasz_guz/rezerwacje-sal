package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.rs.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findOneByLogin(@Param("login") String login);
}