package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.rs.model.RoomSettings;

public interface RoomSettingsRepository extends JpaRepository<RoomSettings, Long> {

	@Query("SELECT rs FROM RoomSettings AS rs LEFT JOIN FETCH rs.arragements AS arr LEFT JOIN FETCH arr.roomElement AS re WHERE rs.id=:id")
	RoomSettings findOneFetch(@Param("id") Long id);
}