package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.rs.model.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
}