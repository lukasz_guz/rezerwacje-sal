package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.rs.model.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

	@Query("SELECT r FROM Reservation AS r LEFT JOIN FETCH r.room AS room WHERE r.id=:id")
	Reservation findOneFetch(@Param("id") Long id);
}