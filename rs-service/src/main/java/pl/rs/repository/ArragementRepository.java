package pl.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.rs.model.Arragement;

public interface ArragementRepository extends JpaRepository<Arragement, Long> {
}