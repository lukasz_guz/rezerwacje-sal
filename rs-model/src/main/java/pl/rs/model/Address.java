package pl.rs.model;

import javax.persistence.Entity;

/**
 * Klasa reprezentująca adres w aplikacji. Używana dla sal i użytkowników
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class Address extends BasicEntity {

	private String city;
	private String apartamentNumber;
	private String houseNumber;
	private String street;
	private String postCode;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getApartamentNumber() {
		return apartamentNumber;
	}

	public void setApartamentNumber(String apartamentNumber) {
		this.apartamentNumber = apartamentNumber;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
}