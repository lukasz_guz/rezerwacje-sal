package pl.rs.model;

import javax.persistence.Entity;

/**
 * Encja reprezentująca zdjęcia w systemie
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class Photo extends BasicEntity {

	private String name;
	private String contentType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}