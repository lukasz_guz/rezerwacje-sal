package pl.rs.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;

/**
 * Element sali np. krzesło, stół
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class RoomElement extends BasicEntity {

	@Min(value = 1)
	private int rowSpan = 1;

	@Min(value = 1)
	private int colSpan = 1;

	private String name;

	@ManyToOne(cascade = CascadeType.ALL)
	private Photo icon;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Photo> photos;
	private BigDecimal price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Photo getIcon() {
		return icon;
	}

	public void setIcon(Photo icon) {
		this.icon = icon;
	}

	public List<Photo> getPhotos() {
		if (photos == null) {
			photos = new ArrayList<>();
		}
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	public int getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}

	public int getColSpan() {
		return colSpan;
	}

	public void setColSpan(int colSpan) {
		this.colSpan = colSpan;
	}
}