package pl.rs.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Encja zawierająca kto jaką salę zarezerwował
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class Reservation extends BasicEntity {

	private String name;

	private BigDecimal price;

	@ManyToOne
	private User user;

	@ManyToOne
	private Room room;

	@ManyToOne
	private RoomSettings roomSettings;

	@Temporal(TemporalType.TIMESTAMP)
	private Date beginReservationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endReservationDate;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public RoomSettings getRoomSettings() {
		return roomSettings;
	}

	public void setRoomSettings(RoomSettings roomSettings) {
		this.roomSettings = roomSettings;
	}

	public Date getBeginReservationDate() {
		return beginReservationDate;
	}

	public void setBeginReservationDate(Date beginReservationDate) {
		this.beginReservationDate = beginReservationDate;
	}

	public Date getEndReservationDate() {
		return endReservationDate;
	}

	public void setEndReservationDate(Date endReservationDate) {
		this.endReservationDate = endReservationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}