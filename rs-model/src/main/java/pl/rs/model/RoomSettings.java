package pl.rs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * Ustawienia wyposażenia sali
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class RoomSettings extends BasicEntity {

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Arragement> arragements;

	public List<Arragement> getArragements() {
		return arragements;
	}

	public void setArragements(List<Arragement> arragements) {
		this.arragements = arragements;
	}
}