package pl.rs.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Klasa przechowująca rozmieszczenie elementu dla danej sali
 * 
 * @author lukasz.guz
 * 
 */
@Entity
public class Arragement extends BasicEntity {

	private Integer x;
	private Integer y;

	@ManyToOne
	private RoomElement roomElement;

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public RoomElement getRoomElement() {
		return roomElement;
	}

	public void setRoomElement(RoomElement roomElement) {
		this.roomElement = roomElement;
	}
}